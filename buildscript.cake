#tool nuget:?package=NUnit.ConsoleRunner&version=3.4.0
#tool "nuget:?package=OpenCover"
#tool "nuget:?package=ReportGenerator"
#addin "Cake.FileHelpers"

//////////////////////////////////////////////////////////////////////
// Common variables
//////////////////////////////////////////////////////////////////////

var buildDir = MakeAbsolute(Directory("./build/"));

var target = Argument("target", "Default");
var configuration = Argument("configuration", "Debug");
var built = Argument("built", false);
var pushNuget = Argument("pushNuget", false);
var buildOctoPackage = Argument("buildOctoPackage", false);
var localNuget = Argument("localNuget", false);

var solutionFile = solution + ".sln";

//////////////////////////////////////////////////////////////////////
// TASKS
//////////////////////////////////////////////////////////////////////

Task("Clean")
    .Does(() => 
{
	CleanDirectory(buildDir);
});

Func<string, IList<string>> recurseForProjFiles = (string basePath) => {
	var results = new List<string>();
	foreach (var dir in System.IO.Directory.GetDirectories(basePath)) {
		results.AddRange(recurseForProjFiles(dir));
	}
	foreach (var file in System.IO.Directory.GetFiles(basePath, "*.csproj")) {
		results.Add(file);
	}
	
	return results;
};

Task("CreateAssemblyInfos")
	.Does(() => 
{
	Information("Finding project files...");
		
	var basePath = Directory(".");
	var projectFiles = recurseForProjFiles(basePath);
		
	foreach (var projFile in projectFiles.Select(x => new FileInfo(x))) {
		var path = projFile.Directory.FullName;
		var versionFile = System.IO.Path.Combine(path, "VERSION");
		Information("Looking for version file " + versionFile);
			
		var infoFile = System.IO.Path.Combine(path, "Properties", "AssemblyInfo.cs");

		CreateAssemblyInfo(infoFile, new AssemblyInfoSettings {
			Version = version,
			FileVersion = version,
			Company = company,
			Copyright = copyright
		});
	}

	Information("Updated all project versions to " + version);
})
.ReportError(exception => {
	Error("Failed to create version info files: " + exception.Message);
});

Task("Restore-NuGet-Packages")
    .IsDependentOn("Clean")
    .Does(() =>
{
    NuGetRestore(solutionFile);
});

Task("Build")
    .IsDependentOn("Restore-NuGet-Packages")
	.IsDependentOn("CreateAssemblyInfos")
    .Does(() =>
{
    if(IsRunningOnWindows())
    {
      // Use MSBuild
      MSBuild(solutionFile, settings => {
        settings.SetConfiguration(configuration);
		settings.WithProperty("OutDir", buildDir.ToString());
	  });
    }
    else
    {
      // Use XBuild
      XBuild(solutionFile, settings =>
        settings.SetConfiguration(configuration));
    }
});

Task("Run-Unit-Tests")
    .Does(() =>
{
	var testFiles = GetFiles(buildDir.ToString() + "/*.Test*.dll");
	if (testFiles.Count == 0) {
		Information("No test files found.");
		return;
	}

	try {
		OpenCover(tool => {
			tool.NUnit3(buildDir.ToString() + "/*.Test*.dll", new NUnit3Settings {
				NoResults = false
				});
		},
		new FilePath(buildDir.ToString() + "/coverageresult.xml"),
		new OpenCoverSettings {
				ReturnTargetCodeOffset = 0
			}
			.WithFilter("+[" + solution + "]*")
			.WithFilter("-[" + solution + "]*.Annotations*")
			.WithFilter("-[" + solution + "]*.Properties*")
			);
	} catch (Exception e) {
		Error("Failed to execute unit tests: " + e.Message);
		throw;
	}

	try {
		ReportGenerator(buildDir.ToString() + "/coverageresult.xml", "./coverage/");
	} catch (Exception e) {
		Error("Failed to generate coverage report: " + e.Message);
		throw;
	}

});

Task("PackNuGet")
	.Does(() =>
{
	var nugetDir = new DirectoryInfo(string.Format("{0}/lastNuget", buildDir));
	nugetDir.Create();
	NuGetPack(string.Format("./{0}/{0}.csproj", solution), new NuGetPackSettings {
		OutputDirectory = string.Format("{0}/lastNuget", buildDir),
		Properties = new Dictionary<string,string>{{"OutDir", MakeAbsolute(buildDir).ToString()}},
		Version = string.Format("{0}{1}", version, isPreRelease ? "-beta" : ""),
		Description = description
	});

	if(pushNuget) {
		var localNugetPath = EnvironmentVariable("USERPROFILE") + "\\NuGet";

		var nuGetPath = localNuget ? localNugetPath : EnvironmentVariable("NUGET_URL");

		if (localNuget && !DirectoryExists(localNugetPath)) {
			CreateDirectory(localNugetPath);
		}
		
		NuGetPush(GetFiles(string.Format("{0}/lastNuget/*.nupkg", buildDir)), new NuGetPushSettings{
			Source = nuGetPath,
			ApiKey = EnvironmentVariable("NUGET_API_KEY")
		});
	}
});

Task("Octo-Push")
	.IsDependentOn("PackNuGet")
	.Does(() => 
{
	var files = GetFiles(string.Format("{0}/*.nupkg", buildDir));
	if(files.Count() == 0)
	throw new Exception("Nothing to push.");
	NuGetPush(files, new NuGetPushSettings{
		Source = EnvironmentVariable("OCTOPUS_URL"),
		ApiKey = EnvironmentVariable("OCTOPUS_KEY")
	});
});


//////////////////////////////////////////////////////////////////////
// TASK TARGETS
//////////////////////////////////////////////////////////////////////

Task("Default")
	.IsDependentOn("Build")
    .IsDependentOn("Run-Unit-Tests");

//////////////////////////////////////////////////////////////////////
// EXECUTION
//////////////////////////////////////////////////////////////////////

RunTarget(target);
