﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using PrepaidCommon;

namespace CNGenUtil
{
    /// <summary>
    /// Interaction logic for AutoPostCleanup.xaml
    /// </summary>
    public partial class AutoPostCleanupView : Window
    {
        public AutoPostCleanupView()
        {
            InitializeComponent();    
            AutoPostCleanupModel apModel = new AutoPostCleanupModel();
            DataContext = apModel;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void OnAutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            string displayName = AttributeHelper.GetDisplayName(e.PropertyDescriptor);
            if (string.IsNullOrEmpty(displayName))
                e.Cancel = true; // hide the column
            else
            {
                e.Column.Header = displayName;
                e.Column.CellStyle = AttributeHelper.GetCellStyle(e.PropertyDescriptor);
            }
        }
    }
}
