﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Input;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CNGenUtil
{
    public class ConfigureUploadSiteModel : INotifyPropertyChanged
    {
        public static string SftpFolderIncomm = "/incomm/Incomm/";
        public static string SftpFolderIncomm_VMS = "/incomm/Incomm_VMS CN Files/";

        private CNGenUtilViewModel _mainModel;
        private List<string> _workflowNames;
        private List<string> _uploadSites;

        public event PropertyChangedEventHandler PropertyChanged;
        public Action OKAction { get; set; }

        public string Jobcode { get => _mainModel.SelectedJobCode;  }
        public string Subjob {  get => _mainModel.SelectedSubjob; }
        public string PackingUPC { get => _mainModel.UploadRecord.PackagingUPC; }

        public ICommand OKCommand { get; set; }

        public List<string> WorkflowNames
        {
            get => _workflowNames;
            set
            {
                _workflowNames = value;
                OnPropertyChanged(nameof(WorkflowNames));
            }
        }

        public List<string> UploadSites
        {
            get => _uploadSites;
            set
            {
                _uploadSites = value;
                OnPropertyChanged(nameof(UploadSites));
            }
        }

        public string SelectedWorkflowName { get; set; }

        public string SelectedUploadSite { get; set; }

        public ConfigureUploadSiteModel(CNGenUtilViewModel model, Action closeOKaction)
        {
            _mainModel = model;
            OKAction = closeOKaction;
            OKCommand = new RelayCommand(AddSite);
        }

        public void Initialize()
        {
            UtilitySupportController usc = new UtilitySupportController();
            List<string> wfns = usc.GetWorkflowNames();
            WorkflowNames = wfns;
            SelectedWorkflowName = "Standard No UPC";

            List<string> sites = new List<string>();
            sites.Add(SftpFolderIncomm);
            sites.Add(SftpFolderIncomm_VMS);
            UploadSites = sites;
            SelectedUploadSite = SftpFolderIncomm;
        }

        private void AddSite(object obj)
        {
            UtilitySupportController usc = new UtilitySupportController();
            string errMsg = usc.AddWorkflow(PackingUPC, SelectedWorkflowName, SelectedUploadSite);
            if (errMsg.Length == 0) // success
                OKAction();
            else
                MessageBox.Show($"Unable to add new workflow: {errMsg}", "Error adding workflow", MessageBoxButton.OK, MessageBoxImage.Warning);
        }

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
