﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace CNGenUtil
{
    public class RF_AmexBase : RF_Base
    {
        protected string _amexCounter = string.Empty;

        protected virtual string CounterName { get => "IncommShipTrackCounter"; }

        public RF_AmexBase() : base()
        {
        }

        public override bool Initialize()
        {
            bool ret = base.Initialize();
            if (ret)
            {
                string errMsg = string.Empty;
                try
                {
                    CDMSDataController cdc = new CDMSDataController();
                    _amexCounter = cdc.GetCounterValue(CounterName).ToString();
                    if (string.IsNullOrWhiteSpace(_amexCounter))
                    {
                        errMsg = $"'{CounterName}' counter not found";
                    }
                }
                catch (Exception ex)
                {
                    errMsg = $"Exception getting AMEX counter: {ex.Message}";
                    ret = false;
                }

                if (!string.IsNullOrEmpty(errMsg))
                {
                    MessageBox.Show(errMsg, "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                    ret = false;
                }
            }
            return ret;
        }
    }
}
