﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PrepaidCommon;

namespace CNGenUtil
{
    public class RF_FakeCN : RF_CNBase
    {
        public override string DisplayName { get; set; } = "Fake CN";
        protected override string SprocName { get => "util_FakeCNGen"; }

        public RF_FakeCN(): base()
        {
        }

    }
}
