﻿using PrepaidCommon;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace CNGenUtil
{
	public class MPFileNameViewModel : FileNameViewModel
	{
		private bool _isTypeOne;

		public List<string> MerchantList { get; set; }
		public string Merchant { get; set; }


		public bool IsTypeOne
		{
			get => _isTypeOne;
			set
			{
				_isTypeOne = value; 
				OnPropertyChanged(nameof(IsTypeOne));
			}
		}

		public MPFileNameViewModel()
		{
			//Vin said there can be more in the list in the future. As of now, just INCOMM.
			List<string> merchantList = new List<string>
			{
				"INCOMM"
			};

			MerchantList = merchantList;
			Merchant = merchantList[0];
			IsTypeOne = false;
		}

		public MPFileNameViewModel(MPFileNameViewModel mpvm) : this()
		{
			Merchant = mpvm.Merchant;
			IsProd = mpvm.IsProd;
			IsTypeOne = mpvm.IsTypeOne;
		}
	}
}
