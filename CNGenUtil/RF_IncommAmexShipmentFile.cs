﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PrepaidCommon;
using System.IO;
using CsvHelper;
using System.Numerics;

namespace CNGenUtil
{
    public class RF_IncommAmexShipmentFile : RF_AmexBase
    {
        public override string DisplayName { get; set; } = "Incomm Amex Shipment File";
        protected override string SprocName { get => "Gen_IncAmexShipmentTracking"; }

        public RF_IncommAmexShipmentFile(): base()
        {

        }

        protected override string CreateFileName()
        {
            DateParts dp = new DateParts();

            return $"CPIMN_VMS_{dp.Day}{dp.Month}{dp.YearShort}_{_amexCounter}_SHIP.csv";
        }

        protected override LineWriter GetLineWriter()
        {
            return (w, rdr) => 
            {
                w.WriteLine("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19}",
                    rdr.GetString(0), rdr.GetString(1), rdr.GetString(2), rdr.GetString(3), rdr.GetString(4),
                    rdr.GetString(5), rdr.GetString(6), rdr.GetString(7), rdr.GetString(8), rdr.GetString(9), rdr.GetString(10),
                    rdr.GetString(11), rdr.GetString(12), rdr.GetString(13), rdr.GetString(14), rdr.GetString(15), rdr.GetString(16)
                    , rdr.GetString(17), rdr.GetString(18), rdr.GetString(19));
            };
        }

        public override int ValidateFile()
        {
            ErrorLogC errorLog = new ErrorLogC();
            int totalErrors = 0;
			int expectedLineLength = 0;
			int lineNum = 0;
			int actualRecords = 0;

			// check line length and line count
			using (StreamReader rdr = new StreamReader(ReturnFilePath))
			{
				while (rdr.Peek() > 0)
				{
					var lineLen = rdr.ReadLine().Length;
					lineNum++;
					if (lineNum == 2)
					{
						expectedLineLength = lineLen; // all lines should be same length as first data record (2nd line in file)
					}
					else if (lineNum > 2 && lineLen != expectedLineLength)
					{
						errorLog.Add(typeof(LineLengthError), lineNum, lineLen.ToString());
						totalErrors++;
					}
				}

				actualRecords = lineNum - 1;
				if (actualRecords != _expectedFileCountFromJob)
				{
					totalErrors++;
				}
			}

			// declare fields we'll track to make sure they're the same in all lines
			string expectedCustomer = string.Empty;
			string expectedParentOrderID = string.Empty;
			string expectedChildOrderID = string.Empty;
			string expectedFileDate = string.Empty;
			string expectedAmexPackageID = string.Empty;
			string expectedAmexCardType = string.Empty;

			// process file using CsvReader to extract fields.
			using (TextReader fileReader = File.OpenText(ReturnFilePath))
			{
				fileReader.ReadLine(); // header
				lineNum = 1;
				var csv = new CsvReader(fileReader);
				BigInteger lastControlVal = 0;
				BigInteger currentControlVal = 0;

				while (csv.Read())
				{
					lineNum++;
					if (lineNum == 2)
					{
						expectedCustomer = csv.GetField<string>(0);
						expectedParentOrderID = csv.GetField<string>(2);
						expectedChildOrderID = csv.GetField<string>(3);
						expectedFileDate = csv.GetField<string>(4);
						expectedAmexPackageID = csv.GetField<string>(7);
						expectedAmexCardType = csv.GetField<string>(8);
					}

					string customer = csv.GetField<string>(0);
					if (customer != expectedCustomer)
					{
						errorLog.Add(typeof(CustomerError), lineNum, customer);
						totalErrors++;
					}

					string parentOrderID = csv.GetField<string>(2);
					if (parentOrderID != expectedParentOrderID)
					{
						errorLog.Add(typeof(ParentOrderIDError), lineNum, parentOrderID);
						totalErrors++;
					}

					string childOrderID = csv.GetField<string>(3);
					if (childOrderID != expectedChildOrderID)
					{
						errorLog.Add(typeof(ChildOrderIDError), lineNum, childOrderID);
						totalErrors++;
					}

					string fileDate = csv.GetField<string>(4);
					if (fileDate != expectedFileDate)
					{
						errorLog.Add(typeof(FileDateError), lineNum, fileDate);
						totalErrors++;
					}

					string amexPackageID = csv.GetField<string>(7);
					if (amexPackageID != expectedAmexPackageID)
					{
						errorLog.Add(typeof(PackageIDError), lineNum, customer);
						totalErrors++;
					}

					string amexCardType = csv.GetField<string>(8);
					if (amexCardType != expectedAmexCardType)
					{
						errorLog.Add(typeof(AmexCardTypeError), lineNum, customer);
						totalErrors++;
					}

					string serialNum = csv.GetField<string>(5);
					BigInteger.TryParse(serialNum, out currentControlVal);
					if (lastControlVal != 0)
					{
						if (currentControlVal != (lastControlVal + 1))
						{
							errorLog.Add(typeof(ControlNumberError), lineNum, currentControlVal.ToString());
							totalErrors++;
						}
					}
					lastControlVal = currentControlVal;
				}
			}

			// add error log headers as needed
			Dictionary<Type, string> errorHeaderParams = new Dictionary<Type, string>();
			errorHeaderParams.Add(typeof(LineLengthError), expectedLineLength.ToString());
			errorHeaderParams.Add(typeof(CustomerError), expectedCustomer);
			errorHeaderParams.Add(typeof(ParentOrderIDError), expectedParentOrderID);
			errorHeaderParams.Add(typeof(ChildOrderIDError), expectedChildOrderID);
			errorHeaderParams.Add(typeof(FileDateError), expectedFileDate);
			errorHeaderParams.Add(typeof(PackageIDError), expectedAmexPackageID);
			errorHeaderParams.Add(typeof(AmexCardTypeError), expectedAmexCardType);

			// write the file
			WriteValidationFile(errorHeaderParams, actualRecords, totalErrors, errorLog);

            return totalErrors;
        }
    }
}
