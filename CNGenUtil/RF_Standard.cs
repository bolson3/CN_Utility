﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PrepaidCommon;
using System.Data.SqlClient;
using System.Data;
using System.Numerics;
using System.IO;
using CsvHelper;

namespace CNGenUtil
{
    public class RF_Standard : RF_CNBase
    {
        public override bool HasTestCardArgument { get => true; }

        public RF_Standard() : base()
        {

        }

        protected override void AddCommandParameters(SqlCommand cmd)
        {
            cmd.Parameters.AddWithValue("@TestOnly", SqlDbType.Int).Value = IsTest ? 1 : 0;
        }

        protected override int GetCardCount()
        {
            if (IsTest)
            {
                return GetCount(true) - GetCount(false);
            }
            else
            {
                return GetCount(false);
            }
        }    
    }
}
