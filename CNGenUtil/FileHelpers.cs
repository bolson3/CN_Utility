﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Navigation;
using FileHelpers;

namespace CNGenUtil
{
	[FixedLengthRecord()]
	class FileHeader
	{
		[FieldFixedLength(1)]
		public string someData = string.Empty;

		[FieldFixedLength(30)]
		[FieldTrim(TrimMode.Right)]
		public string OrderID = string.Empty;

		[FieldFixedLength(30)]
		[FieldTrim(TrimMode.Right)]
		public string IDS = string.Empty;

		[FieldFixedLength(47)]
		[FieldTrim(TrimMode.Right)]
		public string someMoreData = string.Empty;

		[FieldFixedLength(26)]
		[FieldTrim(TrimMode.Right)]
		public string scheduling = string.Empty;

		[FieldFixedLength(35)]
		[FieldTrim(TrimMode.Right)]
		public string addy1 = string.Empty;

		[FieldFixedLength(35)]
		[FieldTrim(TrimMode.Right)]
		public string addy2 = string.Empty;

		[FieldFixedLength(50)]
		[FieldTrim(TrimMode.Right)]
		public string cityStateZip = string.Empty;

		[FieldFixedLength(79)]
		public string someMoreMoreData = string.Empty;

		public override string ToString()
		{
			return OrderID + "," + IDS + "," + scheduling + "," +  addy1 + "," + addy2 + "," + cityStateZip;
		}
	}

	[FixedLengthRecord()]
	class FileBody
	{
		[FieldFixedLength(334)]
		public string gibberish = string.Empty;

		[FieldFixedLength(16)]
		public string controlNum = string.Empty;
		[FieldTrim(TrimMode.Right)]

		[FieldFixedLength(208)]
		public string moreGibberish = string.Empty;

		public override string ToString()
		{
			return controlNum;
		}
	}

	[FixedLengthRecord()]
	class FileFooter
	{
		[FieldFixedLength(13)]
		public string footer = string.Empty;

		public override string ToString()
		{
			return footer;
		}
	}
}
