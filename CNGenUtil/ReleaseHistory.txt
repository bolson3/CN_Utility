﻿v5.0.0.0 5/12/22
- Refactored UI to improve usability
- Refactored implementation to improve maintainability and reliability
- AMEX SHIP is now a return file option rather than separate "menu" item
- Destination folder is now determined automatically
- Post status in now shown and can be refreshed to "see" a file get uploaded

v4.6.4.0 4/27/22
- Fix issue that caused some incorrect logging of return files.
  In particular, RFiles and MPFiles where not getting recorded as having been created.

v4.6.3.0 4/14/22
- Fix for Fake CN on corp

v4.6.2.0 4/12/22
- Updated radio button for user to have to pick production/test 

v4.6.1.0 2/1/22
- Fixed data field value error handling for OrderID, FFID, and BHN IID

v4.6.0.0 10/1/21
- MP file generation now supports carrier other than just CardC

v4.5.0.0 10/1/21
- Added support for Fake CN generation (on onboarding)

v4.4.3.0 9/13/21
- Added secure file server check for SHIP files

v4.4.2.0 8/27/21
- Updated check for MPFile gen - to verify carrier is Card C

v4.4.1.0 7/26/21
- Updated check for saving to secure file server

v4.4.0.0 5/20/21
- Update PrepaidCommon library to support new SQL.

v4.3.1.0 4/6/2021
- Support subjob identifier starting with T

v4.3.0.0 10/30
- RFile bug fix in validation count
- Added authentication and authorization

v4.2.8.0 9/17/20
- Fix for CN validation with subjobs having split pallets (omit line length check)

v4.2.7.0 9/16/20
- Fix for denomination formatting issue for 5-card MP File 
- Fix for 5 card MP File report (stored procedure fix)

v4.2.6.0 8/24/20
- Remove '#' from Magic_Number header

v4.2.5 8/11/20
- Improved upload pending error message.

v4.2.4.0 8/6/20
- Changed handling of Child Header ID to leave package ID blank if header field is not of form INCOMM-MR-########-####

v4.2.3.0
- Fixed MPFile regression

v4.2.2.2
- Fixed bug in admin function to clear upload errors

v4.2.2.1
- Added warning about moving auto-upload files
- Added warning for multipack return file/job mismatches
- Fix for issue where subjob identifiers were not being found

v4.2.2.0
- Added warning if destination folder is secure-side file server
- Updated PrepaidCommon to 3.3.1

v4.2.1.0
- Bug fix for multipack CN gen (test cards)

v4.2.0.0
- Added ability for support admins to clear return file errors

v4.1.1.0
- Added login/logoff messages to log

v4.1.0.0
- Merge of Multipack CN and MPFile features

v4.0.0.4
- Multipack return file validation

v4.0.0.0
- Multipack return files (w/ and w/o UPC)

v3.5.0.2
-Fixed validation for MP files and refactored some code.

v3.5.0.1
-Added validation to MP files

v3.5.0.0
- Added ability to create MP files

v3.4.1.0
- RFile validation fix (count)

v3.4.0.0
- Added previous serial number to serial number error in RFile validation file; and expose line number (Vin requests)
- Added expire date to Standard With UPC return file (Incomm request)
- Security improvements

v3.3.0.0
- RFile validation
- Added STOREFINANCIAL to processors list

v3.2.0.0
- Added ability to autopost VRF files