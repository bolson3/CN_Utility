﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CNGenUtil
{
    /// <summary>
    /// Interaction logic for ConfigureUploadSite.xaml
    /// </summary>
    public partial class ConfigureUploadSite : Window
    {
        private readonly ConfigureUploadSiteModel _viewModel;

        public ConfigureUploadSite(CNGenUtilViewModel mainModel)
        {
            InitializeComponent();
            _viewModel = new ConfigureUploadSiteModel(mainModel, new Action(CloseOK));
            _viewModel.Initialize();
            DataContext = _viewModel;
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            Close(false);
        }

        private void CloseOK()
        {
            Close(true);
        }

        private void Close(bool result)
        {
            DialogResult = result;
            Close();
        }
    }
}
