﻿using System.Windows;
using System;

namespace CNGenUtil
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class ReturnFilesView : Window
    {

        private ReturnFileStatusViewModel _rfsvm;
        public ReturnFilesView()
        {
            InitializeComponent();

            _btnManageErrors.Visibility = UserIsAuthorized() ? Visibility.Visible : Visibility.Hidden;

            _rfsvm = new ReturnFileStatusViewModel();
            _rfsvm.Deferred = true;

            DataContext = _rfsvm;
        }

        private bool UserIsAuthorized()
        {
            bool auth = false;
            try
            {
                UtilitySupportController usdc = new UtilitySupportController();
                string authUsers = usdc.GetAppSetting("CDMS Support Utility", "AdminUsers", string.Empty).ToLowerInvariant();
                auth = authUsers.Contains(Environment.UserName.ToLowerInvariant());
            }
            catch { }
            return auth;
        }

        public void Initialize()
        {
            _rfsvm.Search();
        }
        
        private void CloseBtn_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
