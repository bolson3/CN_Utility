=========== Authentication ===========

Verifies the user is who they say they are.

==== APIs
Authenticate(bool forceAuth=false) -- user is Environment.UserName; opens Login dialog; uses AD if param is true or ADAuthentication is true
Authenticate(CredentialProvider getCredentials) -- user/pwd obtained via the callback; uses AD if ADAuthentication is true
Authenticate(Credentials userCreds) -- user/pwd provided in Credentials; uses AD if ADAuthentication is true

==== Config setting
Key: ADAuthentication
Description: Controls whether or not to authenticate the user via the Active Directory domain controller (for the current domain).
Values: true, false

=========== Authorization ===========

Determines whether or not the user is authorized to use the application.

==== APIs
Authorize() -- true if the user is authorized to run the app; if user not yet authenticatd, first invokes Authenticate() to authenticate user

==== Config settings
Key: Authorization
Description: Type of authorization method to use
Values:
- None - all users are authorized
- CDMS - only users belonging to specificed CDMS groups (CDMSGroupIDs) are authorized to use the application
- AD - only users belonging to specificed AD groups (ADGroups) are authorized to use the application
- ACL - only specific users (ACL) are authorized to use the application

Key: CDMSGroupIDs
Value: List of comma/space separated CDMS group IDs eg 5, 7

Key: ADGroups 
Value: List of AD group names, comma separated

Key: ACL (or ValidUsers)
Value: List of authorized usernames

=========== Roles ===========

Roles can be used to control which features within the application are available to a user.

There are 3 roles: 
- System admin - Users that can access all capabilities (ie developers)
- Application admin - Users that can access certain functions, such as administrative functions.
- User - Any user that is not in one of the admin roles

==== APIs
UserIsSystemAdmin() - true if user is system admin
UserIsApplicationAdmin() - true if user is application admin
UserIsAdmin() - true if user is either a system admin or application admin

==== Config settings
Key: SystemAdmins 
Values: List of usernames - space, comma, semicolon separated

Key: ApplicationAdmins
Values: List of usernames