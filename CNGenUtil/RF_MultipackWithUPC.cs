﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PrepaidCommon;

namespace CNGenUtil
{
    public class RF_MultipackWithUPC : RF_Multipack
    {
        public override string DisplayName { get => "Multipack With UPC"; }
        protected override string SprocName { get => "util_FullCnMPGenUPC"; }
        protected override bool HasUPC { get => true; }

        public override string Header
        {
            get => base.Header + ",Packaging_UPC,Expiry_Date";
        }

        public RF_MultipackWithUPC(): base()
        {

        }

    }
}
