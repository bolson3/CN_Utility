﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CsvHelper;
using System.IO;
using PrepaidCommon;
using System.Windows;
using System.Data.SqlClient;
using System.Data;

namespace CNGenUtil
{
    public class RF_Base
    {
        protected delegate void LineWriter(StreamWriter stream, SqlDataReader rdr);
        protected CNGenUtilViewModel _mainModel = CNGenUtilViewModel.Instance;
        protected int _expectedFileCountFromJob = 0;

        /// <summary>
        /// This name must match the entry in the UtilitySupport tables UPCWorkflowUpload and ReturnFileURLs
        /// </summary>
        public virtual string DisplayName { get; set; } = string.Empty;
        public virtual bool IsMultipack { get => false; }
        public virtual bool HasTestCardArgument { get => false; }
        public virtual bool ShowFileNamePreview { get => false; }
        public virtual string MessagePrefix { get => $"A return file for {SelectedJobCode}_{SelectedSubjob} of type '{DisplayName}'"; }
        protected virtual string SprocName { get => string.Empty; }
        public virtual bool PerformsValidation { get => true; }

        public CDMSJob SelectedJob { get => _mainModel.SelectedJob; }
        public string SelectedJobCode { get => SelectedJob.JobCode; }
        public string SelectedSubjob { get => _mainModel.SelectedSubjob; }

        /// <summary>
        /// Return file name, w/o path
        /// </summary>
        public string ReturnFileName { get; set; } = string.Empty;
        /// <summary>
        /// Return file folder
        /// </summary>
        public string ReturnFileFolder { get; set; } = string.Empty;
        /// <summary>
        /// Full return file path
        /// </summary>
        public string ReturnFilePath { get => Path.Combine(ReturnFileFolder, ReturnFileName); }

        /// <summary>
        /// Validation file name w/o path
        /// </summary>
        public string ValidationFileName { get => string.IsNullOrEmpty(ReturnFileName) ? string.Empty : $"{ReturnFileName}_validation.txt"; }
        /// <summary>
        /// Validation file is always saved to same folder as return file
        /// </summary>
        public string ValidationFilePath { get => Path.Combine(ReturnFileFolder, ValidationFileName); }

        public bool CanAutoUpload { get; set; } = false;
        public string UploadURL { get; set; } = string.Empty;
        public string UploadFolder { get; set; } = string.Empty;
        public string UploadServerName { get; set; } = string.Empty;

        public RF_Base()
        {
        }

        public void Reset()
        {
            ReturnFileName = string.Empty;
        }

        public void SetReturnFile(string existingRetFile)
        {
            ReturnFileFolder = Path.GetDirectoryName(existingRetFile);
            ReturnFileName = Path.GetFileName(existingRetFile);
        }
        
        /// <summary>
        /// Checks that prerequisites are satisfied for file generation. May display text boxes.
        /// </summary>
        public virtual bool Initialize()
        {
            bool ret = true;
            if (SelectedJob.IsMultipack && !IsMultipack)
            {
                if (MessageBox.Show($"This appears to be a multipack job, but '{DisplayName}' is not a multipack return file.\n\nAre you sure you want to continue?",
                    "Potential mis-match", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.No)
                    ret = false;
            }
            else if (!SelectedJob.IsMultipack && IsMultipack)
            {
                if (MessageBox.Show($"{DisplayName} is a multipack return file, but this does not appear to be a multipack job.\n\nAre you sure you want to continue?",
                    "Potential mis-match", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.No)
                    ret = false;
            }

            return ret;
        }

        /// <summary>
        /// Perform any extra checks needed on the return file records assuming the normal record was not found
        /// </summary>
        /// <returns>true if return file checks are successful</returns>
        public virtual bool CustomReturnFileRecordCheck()
        {
            return true;
        }

        /// <summary>
        /// Generates a return file.
        /// Throws exceptions on failure. No message boxes displayed.
        /// </summary>
        public void GenerateFile()
        {
            try
            {
                ReturnFileName = CreateFileName();
                CreateFile();
            }
            catch (Exception ex)
            {
                if (File.Exists(ReturnFilePath))
                    File.Delete(ReturnFilePath);                
                throw new Exception($"GenerateFile failed: {ex.Message}");
            }
        }

        /// <summary>
        /// Key used in the NewReturnFiles table
        /// </summary>
        /// <param name="isTest"></param>
        /// <returns></returns>
        public virtual string Key(bool isTest)
        {
            return DisplayName;
        }

        /// <summary>
        /// Checks that prerequisites are satisfied for file validation. May display text boxes.
        /// </summary>
        public virtual bool ValidateInitialize(out string errMsg)
        {
            bool ret = false;
            errMsg = string.Empty;
            try
            {
                _expectedFileCountFromJob = GetCardCount();
                ret = true;
            }
            catch(Exception ex)
            {
                errMsg = $"Exception getting card count: {ex.Message}";
            }

            if (_expectedFileCountFromJob == 0)
            {
                errMsg = $"Unable to obtain production card count for {SelectedJobCode} {SelectedSubjob}";
            }
            return ret;
        }

        /// <summary>
        /// Validates the return file in the context of the current job. 
        /// Throws exceptions on failure. No message boxes displayed.
        /// </summary>
        /// <returns>number of errors found</returns>
        public virtual int ValidateFile()
        {
            return 0;
        }

        protected virtual string CreateFileName()
        {
            return string.Empty;
        }

        protected virtual int GetCardCount()
        {
            return GetCount(false);
        }

        /// <summary>
        /// Write file. This works for most return file types that use a single Sproc invocation.
        /// </summary>
        protected virtual void CreateFile()
        {
            using (StreamWriter wtr = new StreamWriter(ReturnFilePath))
            {
                using (SqlConnection conn = new SqlConnection(AppHelper.ConnectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(SprocName, conn))
                    {
                        cmd.CommandTimeout = 600;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@JobCode", SqlDbType.VarChar).Value = SelectedJobCode;
                        cmd.Parameters.AddWithValue("@SubjobIdentifier", SqlDbType.VarChar).Value = SelectedSubjob;
                        AddCommandParameters(cmd);

                        using (SqlDataReader rdr = cmd.ExecuteReader())
                        {
                            if (!rdr.HasRows)
                                throw new ArgumentException($"No results returned for {SelectedJobCode} {SelectedSubjob}.");
                            WriteFile(wtr, rdr);
                        }
                    }
                }
            }      
        }

        protected virtual void AddCommandParameters(SqlCommand cmd)
        {
        }

        protected virtual void WriteFile(StreamWriter wtr, SqlDataReader rdr)
        {
            WriteFileBody(wtr, rdr);
        }

        protected virtual LineWriter GetLineWriter()
        {
            return (w, r) => { WriteLine(w, r); };
        }


        protected void WriteFileBody(StreamWriter stream, SqlDataReader rdr)
        {
            LineWriter lineWriter = GetLineWriter();
            while (rdr.Read())
            {
                lineWriter(stream, rdr);
            }
        }

        protected void WriteLine(StreamWriter writer, SqlDataReader rdr)
        {
            string line = rdr.FieldCount > 0 ? rdr.GetString(0) : string.Empty;
            for (int i = 1; i < rdr.FieldCount; i++)
                line += "," + rdr.GetString(i);
            writer.WriteLine(line);
        }

        protected int GetCount(bool all)
        {
            int count = 0;
            using (SqlConnection con = new SqlConnection(AppHelper.ConnectionString))
            {
                con.Open();

                SqlCommand cmd = new SqlCommand(all ? "Util_GetProductionCount_all" : "Util_GetProductionCount", con)
                {
                    CommandTimeout = 600,
                    CommandType = CommandType.StoredProcedure
                };
                cmd.Parameters.AddWithValue("@JobCode", SqlDbType.VarChar).Value = SelectedJobCode;
                cmd.Parameters.AddWithValue("@SubjobIdentifier", SqlDbType.VarChar).Value = SelectedSubjob;

                count = DBHelper.ExecuteScalarSafe(cmd, 0);

                con.Close();
            }

            return count;
        }

        protected void WriteValidationFile(Dictionary<Type, string> headerErrorParams, int actualRecordsCount, int totalErrors, ErrorLogC errorLog)
        {
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(ValidationFilePath))
            {
                file.WriteLine($"Checks performed at: {DateTime.Now}");
                file.WriteLine($"File checked was: {ReturnFilePath}");
                file.WriteLine($"Total records in file is: {actualRecordsCount}, records expected from database for production cards is {_expectedFileCountFromJob}");
                file.WriteLine($"Total errors found: {totalErrors}");
                file.WriteLine();
                foreach (Type eType in errorLog.Keys)
                {
                    ErrorWriter writer = GetWriter(eType, headerErrorParams);
                    file.WriteLine(writer.GetHeader());
                    foreach (ErrorRecord rec in errorLog[eType])
                        file.WriteLine(writer.GetEntry(rec));
                }
            }
        }

        private ErrorWriter GetWriter(Type eType, Dictionary<Type, string> pValues)
        {
            ErrorWriter writer = (ErrorWriter)Activator.CreateInstance(eType);
            if (pValues.ContainsKey(eType))
                writer.Param = pValues[eType];
            return writer;
        }

        /// <summary>
        /// Returns the field value at the specified index. Returns defaultVal if value not found.
        /// </summary>
        protected T GetField<T>(CsvReader csv, int index, T defaultVal)
        {
            if (!csv.TryGetField<T>(index, out T val))
                val = defaultVal;
            return val;
        }

        protected class DateParts
        {
            public string Year { get; }
            public string YearShort { get; }
            public string Month { get; }
            public string Day { get; }
            public string Hour { get; }
            public string Minute { get; }
            public string Second { get; }
            public DateParts()
            {
                var localDate = DateTime.Now.ToString("yyyy, MM, dd, hh, mm, ss");
                Year = localDate.Substring(0, 4);
                YearShort = localDate.Substring(2, 2);
                Month = localDate.Substring(6, 2);
                Day = localDate.Substring(10, 2);
                Hour = localDate.Substring(14, 2);
                Minute = localDate.Substring(18, 2);
                Second = localDate.Substring(22, 2);
            }
        }
    }

    public class CardSubjobComparer : IComparer<CDMSSubjob>
    {
        public int Compare(CDMSSubjob x, CDMSSubjob y)
        {
            if (x == null && y == null)
                return 0;
            if (x == null)
                return -1;
            if (y == null)
                return 1;
            return x.MinControlNumber.CompareTo(y.MinControlNumber);
        }
    }
}
