﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.IO;
using CsvHelper;
using System.Numerics;
using PrepaidCommon;
using System.Data.SqlClient;
using System.Data;

namespace CNGenUtil
{
    public class RF_IncommAmexVRFFile : RF_AmexBase
    {
        private string _fileDate = string.Empty;
        private int _incAmexFileCount = 0;

        private const int EXPECTED_HEADER_LENGTH = 40;
        private const int EXPECTED_HEADER_FIELDS = 9;
        private const int EXPECTED_FOOTER_FIELD_COUNT = 15;
        private const int EXPECTED_FOOTER_LINE_LENGTH = 34;
        private const int EXPECTED_BODY_FIELD_COUNT = 29;
        private const int EXPECTED_LOCAL_COUNTER_LENGTH = 9;
        private const int EXPECTED_GLOBAL_COUNTER_LENGTH = 7;
        private const string EXPECTED_FIRST_LOCAL_COUNTER = "000000001";
        private const string EXPECTED_LINE_TYPE_HEADER = "0001";
        private const string EXPECTED_LINE_TYPE_BODY = "0002";
        private const string EXPECTED_LINE_TYPE_FOOTER = "2000";
        private const int EXPECTED_FOOTER_LINE_TYPE_LENGTH = 4;

        public override string DisplayName { get; set; } = "Incomm Amex VRF File";
        protected override string SprocName { get => "util_IncAmexBundleVRF"; }
        protected override string CounterName { get => "AMEX_VRF_Number"; }


        public RF_IncommAmexVRFFile(): base()
        {

        }

        protected override string CreateFileName()
        {
            DateParts dp = new DateParts();
            _fileDate = $"{dp.Year}{dp.Month}{dp.Day}{dp.Hour}{dp.Minute}{dp.Second}";
            return $"CPI-{_fileDate}-{_amexCounter.PadLeft(7, '0')}.txt";
        }

        protected override int GetCardCount()
        {
            return GetCount(true);
        }

        protected override void WriteFile(StreamWriter wtr, SqlDataReader rdr)
        {
            _incAmexFileCount = 0;
            wtr.WriteLine($"0001,CPIMN,{_amexCounter.PadLeft(7, '0')},{_fileDate},02,,,,");
            WriteFileBody(wtr, rdr);
            wtr.WriteLine($"2000,{_amexCounter.PadLeft(7, '0')},{_incAmexFileCount.ToString().PadLeft(9, '0')},,,,,,,,,,,,");
        }

        protected override LineWriter GetLineWriter()
        {
            return (w, rdr) =>
            {
                w.WriteLine("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21},{22},{23},{24},{25},{26},{27},{28}",
                        rdr.GetString(0), rdr.GetString(1), rdr.GetString(2), rdr.GetString(3), rdr.GetString(4),
                        rdr.GetString(5), rdr.GetString(6), rdr.GetString(7), rdr.GetString(8), rdr.GetString(9), rdr.GetString(10), rdr.GetString(11), rdr.GetString(12), rdr.GetString(13), rdr.GetString(14),
                        rdr.GetString(15), rdr.GetString(16), rdr.GetString(17), rdr.GetString(18), rdr.GetString(19), rdr.GetString(20), rdr.GetString(21), rdr.GetString(22), rdr.GetString(23), rdr.GetString(24),
                        rdr.GetString(25), rdr.GetString(26), rdr.GetString(27), rdr.GetString(28));
                _incAmexFileCount++;
            };
        }

        public override int ValidateFile()
        {
            ErrorLogC errorLog = new ErrorLogC();
            int totalErrors = 0;
            int lineNum = 0;
            int incAmexBodyLength = 0;

            int actualRecords = File.ReadAllLines(ReturnFilePath).Count() - 2; // exclude header and footer
            if (actualRecords != _expectedFileCountFromJob)
            {
                totalErrors++;
            }

            // check line length and line count
            using (StreamReader rdr = new StreamReader(ReturnFilePath))
            {
                string currentLine = string.Empty;

                // header checks
                if (rdr.Peek() > 0)
                {
                    currentLine = rdr.ReadLine();
                    lineNum = 1;

                    int actualHeaderLength = currentLine.Length;
                    if (actualHeaderLength != EXPECTED_HEADER_LENGTH)
                    {
                        errorLog.Add(typeof(IncAmexHeaderError), lineNum, actualHeaderLength.ToString());
                        totalErrors++;
                    }

                    int actualHeaderFields = currentLine.Split(',').Length;
                    if (actualHeaderFields != EXPECTED_HEADER_FIELDS)
                    {
                        errorLog.Add(typeof(IncAmexHeaderFieldsError), lineNum, actualHeaderFields.ToString());
                        totalErrors++;
                    }
                }

                // body checks
                while (rdr.Peek() > 0 && lineNum < actualRecords + 1)
                {
                    currentLine = rdr.ReadLine();
                    lineNum++;

                    int lineLength = currentLine.Length;
                    if (lineNum == 2)
                    {
                        incAmexBodyLength = lineLength; // this is the first detail row - record length to use when validating subsequent rows
                    }
                    if (lineLength != incAmexBodyLength)
                    {
                        errorLog.Add(typeof(IncAmexBodyLineLengthError), lineNum, lineLength.ToString());
                        totalErrors++;
                    }

                    var actualBodyFieldCount = currentLine.Split(',').Length;
                    if (actualBodyFieldCount != EXPECTED_BODY_FIELD_COUNT)
                    {
                        errorLog.Add(typeof(IncAmexBodyFieldCountError), lineNum, actualBodyFieldCount.ToString()); // TODO: Should this be expected or actual?
                        totalErrors++;
                    }
                }

                // footer checks
                if (rdr.Peek() > 0)
                {
                    currentLine = rdr.ReadLine();
                    lineNum++;

                    var actualFooterLineLength = currentLine.Length;
                    if (actualFooterLineLength != EXPECTED_FOOTER_LINE_LENGTH)
                    {
                        errorLog.Add(typeof(IncAmexFooterLineLengthError), lineNum, actualFooterLineLength.ToString());
                        totalErrors++;
                    }

                    var actualFooterFieldCount = currentLine.Split(',').Length;
                    if (actualFooterFieldCount != EXPECTED_FOOTER_FIELD_COUNT)
                    {
                        errorLog.Add(typeof(IncAmexFooterFieldCountError), lineNum, actualFooterFieldCount.ToString());
                        totalErrors++;
                    }
                }
            }

            // declare fields we'll track across lines
            string globalCounterValue = string.Empty;
            int incAmexGlobalCounterLength = 0;
            int incAmexLocalCounterLength = 0;
            string expectedLocalCountValue = _expectedFileCountFromJob.ToString().PadLeft(9, '0');

            // process file using CsvReader to extract fields.
            using (TextReader fileReader = File.OpenText(ReturnFilePath))
            {
                CsvReader csv = new CsvReader(fileReader);
                lineNum = 0;
                while (csv.Read())
                {
                    lineNum++;
                    if (lineNum == 1) // header
                    {
                        var headerLineType = csv.GetField<string>(0);
                        if (headerLineType != EXPECTED_LINE_TYPE_HEADER)
                        {
                            errorLog.Add(typeof(IncAmexHeaderLineTypeError), lineNum, headerLineType);
                            totalErrors++;
                        }                        
                        
                        globalCounterValue = csv.GetField<string>(2);
                        incAmexGlobalCounterLength = globalCounterValue.Length;
                        if (incAmexGlobalCounterLength != EXPECTED_GLOBAL_COUNTER_LENGTH)
                        {
                            errorLog.Add(typeof(IncAmexGlobalCounterLengthError), lineNum, incAmexGlobalCounterLength.ToString());
                            totalErrors++;
                        }
                    }
                    else if (lineNum < actualRecords + 2) // body  
                    {
                        var actualLineType = csv.GetField<string>(0);
                        if (actualLineType != EXPECTED_LINE_TYPE_BODY)
                        {
                            errorLog.Add(typeof(IncAmexFirstDetailLineTypeError), lineNum, actualLineType);
                            totalErrors++;
                        }

                        var currentGlobalCounterValue = csv.GetField<string>(1);
                        if (currentGlobalCounterValue != globalCounterValue)
                        {
                            errorLog.Add(typeof(IncAmexGlobalCounterValueError), lineNum, currentGlobalCounterValue);
                            totalErrors++;
                        }

                        var actualFirstLocalCounter = csv.GetField<string>(2);
                        if ((actualFirstLocalCounter != EXPECTED_FIRST_LOCAL_COUNTER) && (lineNum == 2)) // only check value on first detail row
                        {
                            errorLog.Add(typeof(IncAmexFirstLocalCounterError), lineNum, actualFirstLocalCounter);
                            totalErrors++;
                        }

                        incAmexLocalCounterLength = actualFirstLocalCounter.Length;
                        if (incAmexLocalCounterLength != EXPECTED_LOCAL_COUNTER_LENGTH)
                        {
                            errorLog.Add(typeof(IncAmexLocalCounterLengthError), lineNum, incAmexLocalCounterLength.ToString());
                            totalErrors++;
                        }

                        incAmexGlobalCounterLength = currentGlobalCounterValue.Length;
                        if (incAmexGlobalCounterLength != EXPECTED_GLOBAL_COUNTER_LENGTH)
                        {
                            errorLog.Add(typeof(IncAmexGlobalCounterLengthError), lineNum, incAmexGlobalCounterLength.ToString());
                            totalErrors++;
                        }
                    }
                    else if (lineNum == actualRecords + 2) // footer
                    {
                        var actualFooterLineType = csv.GetField<string>(0);
                        if (actualFooterLineType != EXPECTED_LINE_TYPE_FOOTER)
                        {
                            errorLog.Add(typeof(IncAmexFooterLineTypeError), lineNum, actualFooterLineType);
                            totalErrors++;
                        }

                        var footerLineTypeLength = actualFooterLineType.Length;
                        if (footerLineTypeLength != EXPECTED_FOOTER_LINE_TYPE_LENGTH)
                        {
                            errorLog.Add(typeof(IncAmexFooterLineTypeLengthError), lineNum, footerLineTypeLength.ToString());
                            totalErrors++;
                        }

                        var currentGlobalCounterValue = csv.GetField<string>(1);
                        if (currentGlobalCounterValue != globalCounterValue)
                        {
                            errorLog.Add(typeof(IncAmexGlobalCounterValueError), lineNum, currentGlobalCounterValue);
                            totalErrors++;
                        }                        
                        
                        incAmexGlobalCounterLength = currentGlobalCounterValue.Length; // added this since it was not being updated and would mis-report below
                        if (incAmexGlobalCounterLength != EXPECTED_GLOBAL_COUNTER_LENGTH)
                        {
                            errorLog.Add(typeof(IncAmexGlobalCounterLengthError), lineNum, incAmexGlobalCounterLength.ToString());
                            totalErrors++;
                        }

                        var actualLocalCounterValue = csv.GetField<string>(2);
                        if (actualLocalCounterValue != expectedLocalCountValue)
                        {
                            errorLog.Add(typeof(IncAmexFooterLocalCounterValueError), lineNum, actualLocalCounterValue);
                            totalErrors++;
                        }

                        incAmexLocalCounterLength = actualLocalCounterValue.Length;
                        if (incAmexLocalCounterLength != EXPECTED_LOCAL_COUNTER_LENGTH)
                        {
                            errorLog.Add(typeof(IncAmexLocalCounterLengthError), lineNum, incAmexLocalCounterLength.ToString());
                            totalErrors++;
                        }
                    }
                }
            }

            // add error log headers as needed
            Dictionary<Type, string> errorHeaderParams = new Dictionary<Type, string>();
            errorHeaderParams.Add(typeof(IncAmexGlobalCounterValueError), globalCounterValue);
            errorHeaderParams.Add(typeof(IncAmexBodyLineLengthError), incAmexBodyLength.ToString());
            errorHeaderParams.Add(typeof(IncAmexFooterLocalCounterValueError), expectedLocalCountValue);

            // write the file
            WriteValidationFile(errorHeaderParams, actualRecords, totalErrors, errorLog);

            return totalErrors;
        }
    }
}
