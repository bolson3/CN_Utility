﻿using PrepaidCommon;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using OfficeOpenXml;
using System.Windows.Input;
using System.IO;

namespace CNGenUtil
{
    public class ReturnFileStatusViewModel : INotifyPropertyChanged
    {
        private List<ReturnFileRecord> _returnFiles;
        private bool _pending;
        private bool _posted;
        private bool _deferred;
        private bool _error;
        private bool _notAutoPost;
        private string _jobCodeToSearch = string.Empty;
        private List<DateFilterItem> _dateFilters;
        private DateFilterItem _selectedDateFilter;
        private List<TypeFilterItem> _typeFilters;
        private TypeFilterItem _selectedTypeFilter;

        public event PropertyChangedEventHandler PropertyChanged;

        public bool Pending
        {
            get => _pending;
            set
            {
                _pending = value;
                OnPropertyChanged(nameof(Pending));
            }
        }

        public bool Posted
        {
            get => _posted;
            set
            {
                _posted = value;
                OnPropertyChanged(nameof(Posted));
            }
        }

        public bool Deferred
        {
            get => _deferred;
            set
            {
                _deferred = value;
                OnPropertyChanged(nameof(Deferred));
            }
        }

        public bool Error
        {
            get => _error;
            set
            {
                _error = value;
                OnPropertyChanged(nameof(Error));
            }
        }

        public bool NotAutoPost
        {
            get => _notAutoPost;
            set
            {
                _notAutoPost = value;
                OnPropertyChanged(nameof(NotAutoPost));
            }
        }

        public string JobCodeToSearch
        {
            get => _jobCodeToSearch;
            set
            {
                _jobCodeToSearch = value;
                OnPropertyChanged(nameof(JobCodeToSearch));
            }
        }

        public DateFilterItem SelectedDateFilter
        {
            get => _selectedDateFilter;
            set
            {
                _selectedDateFilter = value;
                OnPropertyChanged(nameof(SelectedDateFilter));
            }
        }

        public List<DateFilterItem> DateFilters
        {
            get => _dateFilters;
            set
            {
                _dateFilters = value;
                OnPropertyChanged(nameof(DateFilters));
            }
        }

        public TypeFilterItem SelectedTypeFilter
        {
            get => _selectedTypeFilter;
            set
            {
                _selectedTypeFilter = value;
                OnPropertyChanged(nameof(SelectedTypeFilter));
            }
        }

        public List<TypeFilterItem> TypeFilters
        {
            get => _typeFilters;
            set
            {
                _typeFilters = value;
                OnPropertyChanged(nameof(TypeFilters));
            }
        }

        public List<ReturnFileRecord> ReturnFiles
        {
            get => _returnFiles;
            set
            {
                _returnFiles = value;
                OnPropertyChanged(nameof(ReturnFiles));
            }
        }

        public ICommand SearchCommand { get; set; }

        public ICommand GenerateExcelCommand { get; set; }

        public ICommand ResetCommand { get; set; }

        public RelayCommand ManageErrorsCommand { get; set; }

        public ReturnFileStatusViewModel()
        {
            SearchCommand = new RelayCommand(param => Search());
            GenerateExcelCommand = new RelayCommand(param => GenerateExcelSheet());
            ResetCommand = new RelayCommand(param => Reset());
            ManageErrorsCommand = new RelayCommand(ManageErrors);

            InitializeFilters();
            Reset();
        }

        private void ManageErrors(object obj)
        {
            AutoPostCleanupView apView = new AutoPostCleanupView();
            apView.ShowDialog();
            Search();
        }

        private void InitializeFilters()
        {
            List<DateFilterItem> dateFilters = new List<DateFilterItem>();

            dateFilters.Add(new DateFilterItem()
            {
                DisplayText = "Last 24 Hours",
                Item = 0
            });
            dateFilters.Add(new DateFilterItem()
            {
                DisplayText = "Last 7 Days",
                Item = 1
            });
            dateFilters.Add(new DateFilterItem()
            {
                DisplayText = "Last 30 days",
                Item = 2
            });
            dateFilters.Add(new DateFilterItem()
            {
                DisplayText = "All",
                Item = 3
            });

            DateFilters = dateFilters;

            List<TypeFilterItem> typeFilters = new List<TypeFilterItem>();
            typeFilters.Add(new TypeFilterItem("CN", ReturnFileCategory.Standard));
            typeFilters.Add(new TypeFilterItem("CN MP", ReturnFileCategory.Multipack));
            typeFilters.Add(new TypeFilterItem("VRF", ReturnFileCategory.VRF));
            typeFilters.Add(new TypeFilterItem("SHIP", ReturnFileCategory.SHIP));
            typeFilters.Add(new TypeFilterItem("All", ReturnFileCategory.All));
            TypeFilters = typeFilters;
        }

        private void Reset()
        {
            SelectedDateFilter = DateFilters[3];
            SelectedTypeFilter = TypeFilters[0];
            JobCodeToSearch = string.Empty;
            Pending = Posted = Error = NotAutoPost = false;
            Deferred = true;
        }

        private void GenerateExcelSheet()
        {
            
            string fileName = AppHelper.GetExcelOutputFile("Save return file status results as", string.Empty);

            if (fileName.Length > 0 && GenerateExcelFile(fileName))
            {
                if (MessageBox.Show(string.Format("Successfully generated '{0}'\n\nWould you like to open it now?", fileName),
                    "Success", MessageBoxButton.YesNo, MessageBoxImage.Information) == MessageBoxResult.Yes)
                {
                    string res = AppHelper.OpenFile(fileName);
                    if (res.Length > 0)
                        MessageBox.Show(res, "Unable to open file", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
        }

        private bool GenerateExcelFile(string fileName)
        {
            try
            {
                using (ExcelPackage excelPackage = new ExcelPackage())
                {
                    excelPackage.Workbook.Properties.Author = "DMT";
                    excelPackage.Workbook.Properties.Title = "Return Files Status";
                    excelPackage.Workbook.Properties.Created = DateTime.Now;

                    ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets.Add("Return Files Status");

                    List<string[]> headerRow = new List<string[]>()
                        {
                          new string[] { "Job Code", "SubJob", "Source file", "Status", "Filename", "Last Updated", "Upload site", "Error detail" }
                        };

                    // Determine the header range (e.g. A1:F1)
                    string headerRange = "A1:" + Char.ConvertFromUtf32(headerRow[0].Length + 64) + "1";

                    // Populate header row data
                    worksheet.Cells[headerRange].LoadFromArrays(headerRow);
                    
                    // Header format
                    worksheet.Cells[headerRange].Style.Font.Bold = true;
                    worksheet.Cells[headerRange].Style.Font.Size = 12;
                    worksheet.Cells[headerRange].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                    // Center some Columns
                    worksheet.Column(1).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    worksheet.Column(2).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                    worksheet.Column(headerRow[0].Length).Style.WrapText = true;

                    for (int i = 1; i <= headerRow[0].Length; i++)
                    {
                        worksheet.Column(i).Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                    }

                    // Add the data
                    int inc = 2;
                    string row = "";
                    foreach (ReturnFileRecord record in ReturnFiles)
                    {
                        row = inc++.ToString();
                        worksheet.Cells["A" + row].Value = record.Jobcode;
                        worksheet.Cells["B" + row].Value = record.Subjob;
                        worksheet.Cells["C" + row].Value = record.SourceFile;
                        worksheet.Cells["D" + row].Value = record.PostState;
                        worksheet.Cells["E" + row].Value = record.FileName;
                        worksheet.Cells["F" + row].Value = record.DateLastUpdated.ToString();
                        worksheet.Cells["G" + row].Value = record.UploadSite;
                        worksheet.Cells["H" + row].Value = record.PostError;
                    }

                    worksheet.Cells[worksheet.Dimension.Address].AutoFitColumns();
                    worksheet.Column(headerRow[0].Length).Width = 80;

                    FileInfo excelFile = new FileInfo(fileName);
                    excelPackage.SaveAs(excelFile);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(string.Format("Please make sure the file is not open in another window. [{0}]", e.Message),
                    "Error while saving file", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            };

            return true;
        }

        public void Search()
        {
            UtilitySupportController usc = new UtilitySupportController();

            bool[] postStateSelection = new[] { Pending, Posted, Deferred, Error, NotAutoPost };

            var stateSelection = new List<byte>();

            if (Pending)
            {
                stateSelection.Add((byte)PostState.Pending);
            }
            if (Posted)
            {
                stateSelection.Add((byte)PostState.Posted);
            }
            if (Deferred)
            {
                stateSelection.Add((byte)PostState.Deferred);
            }
            if (Error)
            {
                stateSelection.Add((byte)PostState.Error);
            }
            if (NotAutoPost)
            {
                stateSelection.Add((byte)PostState.NoAutoPost);
            }

            DateTime dateFilter = GetDateFromDateFilters(SelectedDateFilter);

            ReturnFiles = usc.GetReturnFiles(stateSelection, JobCodeToSearch, dateFilter, SelectedTypeFilter.Category);

        }

        private DateTime GetDateFromDateFilters(DateFilterItem selectedItem)
        {
            DateTime dateTime = DateTime.MinValue;

            if (selectedItem.Item == 0)  //If its Last 24 Hours
            {
                dateTime = DateTime.Now.AddHours(-24);
            }
            if (selectedItem.Item == 1)  //If its Last 7 Days
            {
                dateTime = DateTime.Now.AddDays(-7);
            }
            if (selectedItem.Item == 2)  //If its Last Month
            {
                dateTime = DateTime.Now.AddDays(-30);
            }

            return dateTime;

        }

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
    public class DateFilterItem
    {
        public string DisplayText { get; set; } = string.Empty;
        public int Item { get; set; }
    }

    public class TypeFilterItem
    {
        public string DisplayText { get; set; } = string.Empty;
        public ReturnFileCategory Category { get; set; }
        public TypeFilterItem(string text, ReturnFileCategory cat)
        {
            DisplayText = text;
            Category = cat;
        }
    }

    public enum ReturnFileCategory
    {
        All,
        Standard,
        SHIP,
        VRF,
        Multipack
    }
}
