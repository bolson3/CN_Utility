﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CNGenUtil
{
    public class ErrorLogC : Dictionary<Type, List<ErrorRecord>>
    {
        public void Add(Type eType, int lineNum, string val)
        {
            if (!this.ContainsKey(eType))
                this.Add(eType, new List<ErrorRecord>());
            this[eType].Add(new ErrorRecord(lineNum, val));
        }

        public ErrorLogC() : base()
        {

        }
    }

    public class ErrorRecord
    {
        public int LineNumber { get; set; }
        public string Value { get; set; }
        public ErrorRecord(int lineNum, string val)
        {
            LineNumber = lineNum;
            Value = val;
        }
    }

    #region ErrorWriter
    public class ErrorWriter
    {
        public string Param { get; set; } = string.Empty;
        public virtual string GetHeader()
        {
            return string.Empty;
        }
        public virtual string GetEntry(ErrorRecord record)
        {
            return $"Record Number: {record.LineNumber} is {record.Value}";
        }
    }

    public class CustomerError : ErrorWriter
    {
        public override string GetHeader()
        {
            return $"Customer Error: Expected Customer is {Param}";
        }
    }
    public class ParentOrderIDError : ErrorWriter
    {
        public override string GetHeader()
        {
            return $"Parent Order ID Error: Expected {Param}";
        }
    }
    public class ChildOrderIDError : ErrorWriter
    {
        public override string GetHeader()
        {
            return $"Child order ID Error: Expected Child Order ID is {Param}";
        }
    }
    public class FileDateError : ErrorWriter
    {
        public override string GetHeader()
        {
            return $"File Date Error: Expected File Date is {Param}";
        }
    }
    public class ClientOrderIDError : ErrorWriter
    {
        public override string GetHeader()
        {
            return $"Client ID Error: Expected Client ID is {Param}";
        }
    }
    public class PackageIDError : ErrorWriter
    {
        public override string GetHeader()
        {
            return $"Package ID Error: Expected Package ID is {Param}";
        }
    }
    public class AmexCardTypeError : ErrorWriter
    {
        public override string GetHeader()
        {
            return $"Card Type Error: Expected Card Type is {Param}";
        }
    }
    public class OrderIDError : ErrorWriter
    {
        public override string GetHeader()
        {
            return $"Order Id Error: Expected Order ID is {Param}";
        }
    }
    public class ProdIDError : ErrorWriter
    {
        public override string GetHeader()
        {
            return $"Prod ID Error: Expected Prod ID is {Param}";
        }
    }
    public class ProductUPCError : ErrorWriter
    {
        public override string GetHeader()
        {
            return $"Product UPC Error: Expected {Param}";
        }
    }
    public class SeqNumError : ErrorWriter
    {
        public override string GetHeader()
        {
            return "Sequence Number Error";
        }
        public override string GetEntry(ErrorRecord record)
        {
            return $"Record Number: {record.LineNumber} serial number: {record.Value}";
        }
    }
    public class LineLengthError : ErrorWriter
    {
        public override string GetHeader()
        {
            return $"File Line Length Error: Expected Line Length based on first line {Param}";
        }
        public override string GetEntry(ErrorRecord record)
        {
            return $"Length of line {record.LineNumber} : {record.Value}";
        }
    }
    public class ExpireDateError : ErrorWriter
    {
        public override string GetHeader()
        {
            return "Expiration date Error:";
        }
        public override string GetEntry(ErrorRecord record)
        {
            return $"Expiration date on line {record.LineNumber} {record.Value}";
        }
    }
    public class SenderIDError : ErrorWriter
    {
        public override string GetHeader()
        {
            return $"Sender Id Error: Expected Sender ID is {Param}";
        }
    }
    public class IIDError : ErrorWriter
    {
        public override string GetHeader()
        {
            return $"IID Error: Expected  {Param}";
        }
    }
    public class ExpiryError : ErrorWriter
    {
        public override string GetHeader()
        {
            return $"Expiry Error: Expected Expiry is {Param}";
        }
    }
    public class ProxyError : ErrorWriter
    {
        public override string GetHeader()
        {
            return $"Proxy length Error: Expected proxy length is {Param}";
        }
    }
    public class CartonError : ErrorWriter
    {
        public override string GetHeader()
        {
            return $"Carton Error: Expected {Param}";
        }
    }
    public class ControlNumberError : ErrorWriter
    {
        public override string GetHeader()
        {
            return "Control Number Error:";
        }
        public override string GetEntry(ErrorRecord record)
        {
            return $"Control Number on line {record.LineNumber} is not sequential, instead it is: {record.Value}";
        }
    }
    /// <summary>
    /// Base class for IncAmex errors
    /// </summary>
    public class IncAmexError : ErrorWriter
    {
        public override string GetHeader()
        {
            return string.Empty;
        }
        public override string GetEntry(ErrorRecord record)
        {
            return $"Line {record.LineNumber} : {record.Value}";
        }
    }
    public class IncAmexHeaderError : IncAmexError
    {
        public override string GetHeader()
        {
            return "Header Line Length Error: Expected Line Length for the header : 40";
        }
    }
    public class IncAmexHeaderFieldsError : IncAmexError
    {
        public override string GetHeader()
        {
            return "Header Field Count Error: Expected Field Count for the header : 9  ";
        }
    }
    public class IncAmexHeaderLineTypeError : IncAmexError
    {
        public override string GetHeader()
        {
            return "Header Line Type Error: Expected Line Type for the header : 0001  ";
        }
    }
    public class IncAmexGlobalCounterLengthError : IncAmexError
    {
        public override string GetHeader()
        {
            return "Global Counter Length Error: Expected Length for the Global Counter : 7  ";
        }
    }
    public class IncAmexGlobalCounterValueError : IncAmexError
    {
        public override string GetHeader()
        {
            return $"Global Counter Value Error: Expected Value for the Global Counter : {Param}";
        }
    }
    public class IncAmexBodyFieldCountError : IncAmexError
    {
        public override string GetHeader()
        {
            return "Body Field Count Error: Expected Field Count Length :  31";
        }
    }
    public class IncAmexFirstLocalCounterError : IncAmexError
    {
        public override string GetHeader()
        {
            return "Local Counter Value Error: Expected First Local Counter :  000000001";
        }
    }
    public class IncAmexFirstDetailLineTypeError : IncAmexError
    {
        public override string GetHeader()
        {
            return "Line Type Value Error: Expected Body Line Type :  0002";
        }
    }
    public class IncAmexBodyLineLengthError : IncAmexError
    {
        public override string GetHeader()
        {
            return $"Body Line Length Error: Expected Body Line Length : {Param}";
        }
    }
    public class IncAmexLocalCounterLengthError : IncAmexError
    {
        public override string GetHeader()
        {
            return $"Local Counter Length Error: Expected Length :  9";
        }
    }
    public class IncAmexFooterLineTypeError : IncAmexError
    {
        public override string GetHeader()
        {
            return $"Footer Line Type error: Expected Value :  2000";
        }
    }
    public class IncAmexFooterLineTypeLengthError : IncAmexError
    {
        public override string GetHeader()
        {
            return $"Footer Line Type Length Error: Expected Value :  4";
        }
    }
    public class IncAmexFooterLineLengthError : IncAmexError
    {
        public override string GetHeader()
        {
            return $"Footer Line Length Error: Expected Value :  34";
        }
    }
    public class IncAmexFooterLocalCounterValueError : IncAmexError
    {
        public override string GetHeader()
        {
            return $"Footer Local Counter Value Error: Expected value : {Param}";
        }
    }
    public class IncAmexFooterFieldCountError : IncAmexError
    {
        public override string GetHeader()
        {
            return $"Footer Field Count Error: Expected Field Count : 15";
        }
    }
    public class NSReceiptNSNumberError : ErrorWriter
    {
        public override string GetHeader()
        {
            return $"NS Number Error: Expected NS Number {Param}";
        }
    }
    public class NSReceiptQuantityError : ErrorWriter
    {
        public override string GetHeader()
        {
            return $"NS Bundle Quantity Error: Expected NS Bundle Quantity {Param}";
        }
    }
    public class NSControlNumberError : ErrorWriter
    {
        public override string GetHeader()
        {
            return $"NS Control Number Sequence error: Error or gap in Sequence number";
        }
    }
    public class NSDateGenError : ErrorWriter
    {
        public override string GetHeader()
        {
            return $"NS Date Generation error: Error in Date generated, expected date was {Param}";
        }
    }
    public class NSGeneratorError : ErrorWriter
    {
        public override string GetHeader()
        {
            return $"NS Generator error: Error in generator Field, expected value was {Param}";
        }
    }
    public class NSExpiryError : ErrorWriter
    {
        public override string GetHeader()
        {
            return $"NS expiry error: Error in expiry Field, expected date was {Param}";
        }
    }
    public class ParentSerialError : ErrorWriter
    {
        public override string GetHeader()
        {
            return $"Parent serial number errors: {Param}";
        }
    }
    public class CardSerialError : ErrorWriter
    {
        public override string GetHeader()
        {
            return $"Card serial error";
        }
    }
    public class MissingFieldError : ErrorWriter
    {
        public override string GetHeader()
        {
            return "Missing Fields";
        }
    }
    #endregion // ErrorWriter
}
