﻿using PrepaidCommon;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace CNGenUtil
{
    public class RFileNameViewModel : FileNameViewModel
    {
        public List<string> ProcessorsList { get; set; }

        public string Processor { get; set; }

        public RFileNameViewModel()
		{
			var processorsList = new List<string> //list of processors DMT selects for file name.
			{
				"NETSPEND",
				"I2C",
				"PPT",
				"INCOMM",
				"TOKA",
				"GALILEO",
				"STOREFINANCIAL"
			};

			ProcessorsList = processorsList;
            Processor = processorsList[0];
		}

        public RFileNameViewModel(RFileNameViewModel rfvm) : this()
        {
            Processor = rfvm.Processor;
            IsProd = rfvm.IsProd;
        }
    }
}
