﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace CNGenUtil
{
    /// <summary>
    /// Interaction logic for RFileNameView.xaml
    /// </summary>
    public partial class RFileNameView : Window
	{
		
		public RFileNameView(RFileNameViewModel x)
		{
			InitializeComponent();
			DataContext = x;
		}

		private void OkButton_Click(object sender, RoutedEventArgs e)
		{
            DialogResult = true;
		}

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }


        private void ComboBox_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
		{
			
		}
		
		public string Production { get; set; }     
    }

    public class BoolRadioConverter : IValueConverter
    {
        public bool Inverse { get; set; }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool boolValue = (bool)value;

            return this.Inverse ? !boolValue : boolValue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool boolValue = (bool)value;

            if (!boolValue)
            {
                // We only care when the user clicks a radio button to select it.
                return null;
            }

            return !this.Inverse;
        }
    }
}
