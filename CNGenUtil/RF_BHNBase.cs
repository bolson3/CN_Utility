﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using PrepaidCommon;

namespace CNGenUtil
{
    public class RF_BHNBase : RF_Base
    {
        private string _bhnCounter = string.Empty;
        private string _bhnIID = string.Empty;

        public RF_BHNBase() : base()
        {

        }

        public override bool Initialize()
        {
            bool ret = base.Initialize();
            if (ret)
            {
                string errMsg = string.Empty;
                try
                {
                    CDMSDataController cdc = new CDMSDataController();

                    _bhnCounter = cdc.GetCounterValue("ArchwayEASN").ToString();
                    if (string.IsNullOrWhiteSpace(_bhnCounter))
                    {
                        errMsg = $"BHN counter not found for {SelectedJobCode} {SelectedSubjob}";
                    }
                    else
                    {
                        _bhnIID = cdc.GetDatafieldValue(SelectedJobCode, SelectedSubjob, "BHN IID");
                        if (string.IsNullOrWhiteSpace(_bhnIID))
                        {
                            errMsg = $"BHN IID not found for {SelectedJobCode} {SelectedSubjob}";
                        }
                    }
                }
                catch (Exception ex)
                {
                    errMsg = $"Exception while getting BHN counter or IID: {ex.Message}";
                }

                if (!string.IsNullOrEmpty(errMsg))
                {
                    MessageBox.Show($"{errMsg}", "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                    ret = false;
                }
            }
            return ret;
        }

        protected override string CreateFileName()
        {
            DateParts dp = new DateParts();

            return $"EASN_BHN_CPI{_bhnIID}_{dp.Year}{dp.Month}{dp.Day}_{_bhnCounter.PadLeft(6, '0')}.dat";
        }
    }
}
