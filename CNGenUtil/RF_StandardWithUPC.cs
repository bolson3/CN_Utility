﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PrepaidCommon;

namespace CNGenUtil
{
    public class RF_StandardWithUPC : RF_Standard
    {
        public override string DisplayName { get; set; } = "Standard With UPC";
        protected override string SprocName { get => "util_FullCNGenUPC3"; }
        protected override bool HasUPC { get => true; }

        public RF_StandardWithUPC(): base()
        {

        }

    }
}
