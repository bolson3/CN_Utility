﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CNGenUtil
{
	/// <summary>
	/// Interaction logic for MPFileNameView.xaml
	/// </summary>
	public partial class MPFileNameView : Window
	{
		public MPFileNameView(MPFileNameViewModel x)
		{
			InitializeComponent();
			DataContext = x;
		}

		private void OkButton_Click(object sender, RoutedEventArgs e)
		{
			DialogResult = true;
		}

		private void ComboBox_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
		{

		}
	}
}
