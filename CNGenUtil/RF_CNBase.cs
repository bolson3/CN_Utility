﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using PrepaidCommon;
using System.Data.SqlClient;
using System.Data;
using System.Numerics;
using System.IO;
using CsvHelper;
using System.Windows.Markup;

namespace CNGenUtil
{
    /// <summary>
    /// Base class for all Incomm CN files
    /// </summary>
    public class RF_CNBase : RF_Base
    {
        protected string _prodID = string.Empty;
        protected string _prodUPC = string.Empty;
        protected string _orderID = string.Empty;

        protected virtual bool HasUPC { get => false; }

        protected bool IsProduction { get => _mainModel.IsProduction; }
        protected bool IsTest { get => _mainModel.IsTest; }

        public RF_CNBase(): base()
        {
        }

        protected override string CreateFileName()
        {
            DateParts dp = new DateParts();
            return $"CPIToIncomm_StoreShipping_{dp.Month}{dp.Day}{dp.Year}{dp.Hour}{dp.Minute}{dp.Second}.csv";
        }

        public override string Key(bool isTest)
        {
            return (HasTestCardArgument && isTest) ? DisplayName + " (TestCards)" : DisplayName;
        }

        public override bool Initialize()
        {
            bool ret = base.Initialize();
            if (ret)
            {
                string errMsg = string.Empty;
                // check radio buttons prod/test
                if (HasTestCardArgument && !IsProduction && !IsTest)
                {
                    errMsg = "Please select either Production or Test.\n\n" +
                        "NOTE: Please be especially careful about your selection if this file will be automatically uploaded, as the upload may occur within seconds.";
                }
                else
                {
                    try
                    {
                        CDMSDataController cdc = new CDMSDataController();
                        string orderID = cdc.GetDatafieldValue(SelectedJobCode, SelectedSubjob, "OrderID"); // don't need to save this; it's used in a sproc so that's why we check here
                        if (string.IsNullOrWhiteSpace(orderID))
                        {
                            errMsg = $"Data field 'OrderID' was not found for: {SelectedJobCode} {SelectedSubjob}";
                        }
                    }
                    catch (Exception ex)
                    {
                        errMsg = $"Exception while getting data field value: {ex.Message}";
                    }
                }

                if (!string.IsNullOrEmpty(errMsg))
                {
                    MessageBox.Show($"{errMsg}", "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                    ret = false;
                }
            }
            return ret;
        }

        public override bool ValidateInitialize(out string errMsg)
        {
            bool ret = base.ValidateInitialize(out errMsg);
            if (ret)
            {
                ret = false;
                if (HasTestCardArgument && !IsProduction && !IsTest)
                {
                    errMsg = "Please select either Production or Test.";
                }
                else
                {
                    try
                    {
                        CDMSDataController cdc = new CDMSDataController();

                        _prodID = cdc.GetCustomerProductID(SelectedJobCode, SelectedSubjob);
                        _prodUPC = cdc.GetProductDataValue(SelectedJobCode, SelectedSubjob, "Package UPC");
                        _orderID = cdc.GetDatafieldValue(SelectedJobCode, SelectedSubjob, "OrderID");

                        if (string.IsNullOrEmpty(_prodID))
                        {
                            errMsg = $"Customer Product ID is not set for {SelectedJobCode} {SelectedSubjob} and will cause a validation error.";
                        }
                        else if (string.IsNullOrEmpty(_prodUPC) && HasUPC)
                        {
                            errMsg = $"Product UPC is not set for {SelectedJobCode} {SelectedSubjob} and will cause a validation error.";
                        }
                        else if (string.IsNullOrWhiteSpace(_orderID))
                        {
                            errMsg = $"Order ID is not set for {SelectedJobCode} {SelectedSubjob} and will cause a validation error.";
                        }
                        else
                        {
                            ret = true;
                        }
                    }
                    catch (Exception ex)
                    {
                        DBHelper.LogMessage($"Exception in ValidateInitialize. {DisplayName}, {SelectedJobCode} {SelectedSubjob}: {ex.Message}");
                        errMsg = $"Exception occurred: {ex.Message}";
                    }
                }
            }
            return ret;
        }

        public override int ValidateFile()
        {
            ErrorLogC errorLog = new ErrorLogC();
            int totalErrors = 0;
            int lineNum = 0;

            // check line count
            int actualRecords = File.ReadAllLines(ReturnFilePath).Count() - 1; // exclude header
            if (actualRecords != _expectedFileCountFromJob)
                totalErrors++;

            // process file using CsvReader to extract fields.
            using (TextReader fileReader = File.OpenText(ReturnFilePath))
            {
                if (fileReader.Peek() > 0)
                {
                    fileReader.ReadLine(); // header
                    lineNum = 1;
                }
                var csv = new CsvReader(fileReader);
                BigInteger lastControlVal = 0;
                BigInteger currentControlVal = 0;

                while (csv.Read())
                {
                    lineNum++;

                    string fileOrderID = GetField(csv, 20, "<missing>");
                    if (fileOrderID != _orderID)
                    {
                        errorLog.Add(typeof(OrderIDError), lineNum, fileOrderID);
                        totalErrors++;
                    }

                    string fileProdID = GetField(csv, 19, "<missing>");
                    if (fileProdID != _prodID)
                    {
                        errorLog.Add(typeof(ProdIDError), lineNum, fileProdID);
                        totalErrors++;
                    }

                    if (HasUPC)
                    {
                        string fileUPC = GetField(csv, 22, "<missing>");
                        if (fileUPC != _prodUPC)
                        {
                            errorLog.Add(typeof(ProductUPCError), lineNum, fileUPC);
                            totalErrors++;
                        }

                        string expireDate = GetField(csv, 23, string.Empty);
                        if (string.IsNullOrWhiteSpace(expireDate))
                        {
                            errorLog.Add(typeof(ExpireDateError), lineNum, "is missing");
                            totalErrors++;
                        }
                    }

                    if (IsMultipack)
                    {
                        string parentSN = GetField(csv, 21, "<missing>");
                        if (!BigInteger.TryParse(parentSN, out BigInteger value))
                        {
                            errorLog.Add(typeof(ParentSerialError), lineNum, parentSN);
                            totalErrors++;
                        }
                    }

                    string serialNum = GetField(csv, 11, "<missing>");
                    BigInteger.TryParse(serialNum, out currentControlVal);
                    if (lastControlVal != 0)
                    {
                        if (currentControlVal != (lastControlVal + 1))
                        {
                            errorLog.Add(typeof(ControlNumberError), lineNum, currentControlVal.ToString());
                            totalErrors++;
                        }
                    }
                    lastControlVal = currentControlVal;
                }
            }

            // add error log headers as needed
            Dictionary<Type, string> errorHeaderParams = new Dictionary<Type, string>();
            errorHeaderParams.Add(typeof(OrderIDError), _orderID);
            errorHeaderParams.Add(typeof(ProdIDError), _prodID);
            errorHeaderParams.Add(typeof(ProductUPCError), _prodUPC);
            int seCount = errorLog.ContainsKey(typeof(ParentSerialError)) ? errorLog[typeof(ParentSerialError)].Count : 0;
            errorHeaderParams.Add(typeof(ParentSerialError), seCount.ToString());

            // write the file
            WriteValidationFile(errorHeaderParams, actualRecords, totalErrors, errorLog);

            return totalErrors;
        }

    }
}
