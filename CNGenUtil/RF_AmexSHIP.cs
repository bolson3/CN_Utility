﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows;
using Microsoft.Win32;
using System.IO;
using PrepaidCommon;
using System.ComponentModel;
using FileHelpers;
using PgpCore;


namespace CNGenUtil
{
    public class RF_AmexSHIP : RF_Base
    {
        private string _ids = string.Empty;
        private string _orderID = string.Empty;
        private string _packageID = string.Empty;
        private string[] _serialList = null;
        private string _contactName = string.Empty;
        private string _address1 = string.Empty;
        private string _address2 = string.Empty;
        private string _cityStateZip = string.Empty;

        private string _amexCounter = string.Empty;

        public override string DisplayName { get; set; } = "Amex SHIP";
        public override string MessagePrefix { get => $"A SHIP file for {SourceFileName}"; }
        public override bool PerformsValidation { get => false; }
        protected string SourceFilePath { get; set; } = string.Empty;
        protected string SourceFileName { get => Path.GetFileName(SourceFilePath); }

        public RF_AmexSHIP() : base()
        {
        }

        protected override string CreateFileName()
        {
            return $"CPIMN_VMS_{DateTime.Now.ToString("MMddyy")}_{_amexCounter}_SHIP.csv";
        }

        public override string Key(bool isTest)
        {
            return SourceFileName; // it's a strange key, yes.
        }

        public override bool Initialize()
        {
            bool ret = base.Initialize();
            if (ret)
            {
                // user must select a source file
                SourceFilePath = GetGenFile();
                if (string.IsNullOrWhiteSpace(SourceFilePath))
                {
                    ret = false; // user basically cancelled
                }
                else if (!File.Exists(SourceFilePath))
                {
                    ret = false;
                    MessageBox.Show($"File not found: {SourceFilePath}", "File not found", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }

            if (ret)
            {
                string errMsg = string.Empty;

                // get counter
                try
                {
                    CDMSDataController cdc = new CDMSDataController();
                    int counter = cdc.GetCounterValue("IncommShipTrackCounter");
                    if (counter <= 0)
                    {
                        errMsg = "IncommShipTrackCounter is less <= 0";
                    }
                    else
                    {
                        _amexCounter = counter.ToString();
                        cdc.UpdateCounter("IncommShipTrackCounter", counter + 1);
                    }
                }
                catch (Exception ex)
                {
                    errMsg = $"Exception getting/setting IncommShipTrackCounter: {ex.Message}";
                }

                if (!string.IsNullOrEmpty(errMsg))
                {
                    MessageBox.Show(errMsg, "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                    ret = false;
                }

            }
            return ret;
        }

        public override bool CustomReturnFileRecordCheck()
        {
            UtilitySupportController usc = new UtilitySupportController();

            // see if source file has existing entries
            List<ReturnFileRecord> existingRecords = usc.GetReturnFileRecords(SourceFileName);
            if (existingRecords.Count > 0)
            {
                string jsjlist = string.Empty;
                foreach (ReturnFileRecord r in existingRecords)
                {
                    jsjlist += $"  {r.Jobcode}-{r.Subjob} {r.DateLastUpdated.ToString("MM/dd/yyyy h:mm tt")} Post state: {r.PostState.ToString()}\n";
                }
                if (MessageBoxResult.No == MessageBox.Show(
                    $"The source file '{SourceFileName}' already has at least one SHIP file associated with a different job/subjob:\n\n{jsjlist}\nAre you sure you want to continue?",
                    "Potential duplication", MessageBoxButton.YesNo, MessageBoxImage.Warning))
                {
                    return false;
                }
            }

            // see if job/subjob have existing SHIP entries (note: ReturnFileType in that case is the name of the source file)
            existingRecords = usc.GetReturnFileRecords(SelectedJobCode, SelectedSubjob, ReturnFileCategory.SHIP);
            if (existingRecords.Count > 0)
            {
                string flist = string.Empty;
                foreach (ReturnFileRecord r in existingRecords)
                {
                    flist += $"  {r.ReturnFileTypeName} {r.DateLastUpdated.ToString("MM/dd/yyyy h:mm tt")} Post state: {r.PostState.ToString()}\n";
                }
                if (MessageBoxResult.No == MessageBox.Show(
                    $"{SelectedJobCode}-{SelectedSubjob} already has at least one SHIP file associated with a different source file:\n\n{flist}\nAre you sure you want to continue?",
                    "Potential duplication", MessageBoxButton.YesNo, MessageBoxImage.Warning))
                {
                    return false;
                }
            }
            return true;
        }

        protected override void CreateFile()
        {
            AutoResetEvent join = new AutoResetEvent(false);
            string errMsg = string.Empty;
            FileResult result = new FileResult();

            BackgroundWorker worker = new BackgroundWorker();
            worker.DoWork += (s, e) => 
            {
                try
                {
                    if (SourceFileName.EndsWith(".pgp")) // decrypt it first
                    {
                        Task<FileResult> decrypter = Task.Run(async () => await Decrypt(SourceFilePath));
                        result = decrypter.Result;
                    }
                    else // simple
                    {
                        result.Filename = SourceFilePath;
                    }

                    if (result.IsSuccess)
                    {
                        try
                        {
                            if (ReadRawReceipt(result.Filename))
                            {
                                GenShipmentFile();
                            }
                        }
                        catch (Exception ex)
                        {
                            errMsg = $"Exception during SHIP file generation: {ex.Message}";
                        }
                    }
                    else
                    {
                        errMsg = $"Decryption failed: {result.ErrorMsg}. No file was generated.";
                    }
                }
                catch
                {
                    throw;
                }
                finally
                {
                    join.Set(); // unblock waiting thread
                }
            };

            worker.RunWorkerCompleted += (s, e) => 
            {
                if (e.Error != null)
                {
                    errMsg = $"Exception while decrypting: {e.Error.Message}";
                }

                if (!string.IsNullOrWhiteSpace(errMsg))
                {
                    DBHelper.LogMessage(errMsg);
                    throw new Exception(errMsg);
                }
            };

            worker.RunWorkerAsync(); //  launch the thread
            join.WaitOne(); // wait for completion - don't want to return to main view until the file is written     
        }

        public override bool ValidateInitialize(out string errMsg)
        {
            errMsg = string.Empty;
            return true;
        }

        private void GenShipmentFile()
        {
            try
            {
                using (StreamWriter wr = new StreamWriter(ReturnFilePath))
                {
                    wr.WriteLine(
                        "CUSTOMER,SOURCE_ONE_BATCH,PARENT_ORDER_ID,CHILD_ORDER_ID,FILE_DATE,SERIALNUMBER,CARDS,PACKAGE_ID,CARD_TYPE,CONTACT_NAME," +
                        "SHIP_TO,ADDRESS_1,ADDRESS_2,CITY,STATE,ZIP,TRACKING_NUMBER,SHIP_DATE,SHIPMENT_ID,SHIP_METHOD");
                    for (int i = 0; i < _serialList.Length; i++)
                    {
                        wr.WriteLine("INCOMM,," + _orderID + "," + _ids + "," + DateTime.Now.ToString("MM/dd/yyyy")
                                    + "," + _serialList[i] + ",," + _packageID + ",3,," + _contactName + "," + _address1 + "," + _address2 + "," + _cityStateZip + ",,"
                                    + DateTime.Now.ToString("MM/dd/yyyy") + ",N/A,");
                    }
                }
            }
            catch (Exception e)
            {
                throw new ArgumentException($"Unexpected failure during file generation: {e.Message}");
            }
        }

        private async Task<FileResult> Decrypt(string sourceFile)
        {
            UtilitySupportController usc = new UtilitySupportController();
            FileResult res = new FileResult();
            string decryptedFile = string.Empty;
            string privateKeyPath = usc.GetAppSetting("Common", "DMTPrivateKeyPath", string.Empty);
            string passcode = usc.GetAppSetting("Common", "DMTPwd", string.Empty);
            try
            {
                decryptedFile = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                                             Path.GetFileNameWithoutExtension(sourceFile));
                if (Path.GetExtension(decryptedFile).Length == 0)
                {
                    decryptedFile = Path.Combine(decryptedFile, ".csv");
                }
                if (File.Exists(decryptedFile))
                {
                    File.Delete(decryptedFile);
                }
                using (PGP pgp = new PGP())
                {
                    await pgp.DecryptFileAsync(sourceFile, decryptedFile, privateKeyPath, passcode);
                }
                res.Filename = decryptedFile;
            }
            catch (Exception e)
            {
                res.ErrorMsg = $"Failure while decrypting source file: {e.Message}";
                DBHelper.LogMessage(res.ErrorMsg);
            }
            return res;
        }

        private string GetGenFile()
        {
            string fileName = string.Empty;
            string srcFolder = AppHelper.GetAppSetting("SourceFolder", @"\\10.41.5.11\r_shared\Data Management Team");
            using (WaitCursor wc = new WaitCursor())
            {
                string tmpFolder = AppHelper.GetJobFolder(SelectedJobCode, AppHelper.TargetDB.Equals("PRODUCTION"));
                if (Directory.Exists(tmpFolder))
                {
                    srcFolder = tmpFolder;
                    tmpFolder = Path.Combine(srcFolder, "Received", "Data");
                    if (Directory.Exists(tmpFolder))
                    {
                        srcFolder = tmpFolder;
                        tmpFolder = Path.Combine(srcFolder, SelectedSubjob);
                        if (Directory.Exists(tmpFolder))
                        {
                            srcFolder = tmpFolder;
                        }
                    }
                }
            }

            OpenFileDialog dialog = new OpenFileDialog()
            {
                Title = "Select data source file to use for generating the SHIP file",
                Multiselect = false,
                Filter = "PGP files|*.pgp|CSV files|*.csv|All files (*.*)|*.*",
                InitialDirectory = srcFolder
            };

            if (dialog.ShowDialog() == true)
            {
                fileName = dialog.FileName;
            }
            return fileName;
        }

        private bool ReadRawReceipt(string inputFile)
        {
            string[] headerStrings;
            try
            {
                // TODO: Refactor to use strongly typed parts (FileHeader, FileBody, FileFooter)
                var engine = new MultiRecordEngine(typeof(FileHeader), typeof(FileBody), typeof(FileFooter))
                {
                    RecordSelector = new RecordTypeSelector(CustomSelector)
                };
                var res = engine.ReadFile(inputFile);

                headerStrings = res[0].ToString().Split(',');
                _orderID = headerStrings[0];
                _ids = headerStrings[1];
                _packageID = GetPackageID(_ids);
                _contactName = headerStrings[2];
                _address1 = headerStrings[3];
                _address2 = headerStrings[4];
                if (_address2 == "")
                    _address2 = " ";
                _cityStateZip = headerStrings[5] + "," + headerStrings[6] + "," + headerStrings[7];
                //grab serial number from body
                int offset = 334;
                int length = 16;
                int counter = 0;
                _serialList = new string[res.Length - 2];
                using (StreamReader sr = new StreamReader(inputFile))
                {
                    string line = sr.ReadLine();

                    while (counter < res.Length - 2)
                    {
                        line = sr.ReadLine();
                        _serialList[counter] = line.Substring(offset, length).Trim();
                        ++counter;
                    }
                }
            }
            catch (Exception e)
            {
                throw new ArgumentException($"Failure reading source file. Please verify that it's a valid CCF 4.8 file: {e.Message}");
            }
            return true;
        }

        private string GetPackageID(string childOrderID)
        {
            string packageID = string.Empty;
            string[] parts = childOrderID.Split(new char[] { '-', '_' });
            if (parts.Length > 2)
                packageID = parts[2]; // expected form: InComm-MR-07222019-0010 => packageID = 07222019
            return packageID;
        }

        private Type CustomSelector(MultiRecordEngine engine, string recordLine)
        {
            switch (recordLine.Length)
            {
                case 0:
                    return null;
                case 333:
                    return typeof(FileHeader);
                case 13:
                    return typeof(FileFooter);
                default:
                    return typeof(FileBody);
            }
        }

        private class FileResult
        {
            public string Filename { get; set; } = string.Empty;
            public bool IsSuccess { get { return Filename.Length > 0 && string.IsNullOrEmpty(ErrorMsg); } }
            public string ErrorMsg { get; set; } = string.Empty;
        }
    }
}
