﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data; 
using PrepaidCommon;

namespace CNGenUtil
{
    public class CDMSDataController
    {
        private readonly Regex MultipackSJID_RE = new Regex(@"^(T|S[BIE])(\d+)[A-K]$", RegexOptions.IgnoreCase);

        private readonly string _connStr;

        public CDMSDataController()
        {
            _connStr = AppHelper.ConnectionString;
        }

        public CDMSJob GetJob(string jobcode)
        {
            CDMSJob res = null;
            try
            {
                using (CDMSDataContext cdmsdb = new CDMSDataContext(_connStr))
                {
                    Job job = cdmsdb.Jobs.Where(j => j.vch_JobCode.Equals(jobcode)).FirstOrDefault();
                    if (job != null)
                    {
                        res = new CDMSJob()
                        {
                            JobCode = jobcode,
                            JobID = job.iPK_JobID,
                            JobDescription = AppHelper.TrimCRLF(job.vch_Description),
                            ProductionDB = job.vch_ProductionDataDatabase ?? string.Empty
                        };
                        res.IsMultipack = cdmsdb.SubJobs.Where(s => s.iFK_JobID == job.iPK_JobID && s.vch_SubJobIdentifier.ToUpper().Equals("SB01A"))
                            .FirstOrDefault() != null;
                    }
                }
            }
            catch
            {
                throw new ApplicationException("Unable to access CDMS database.");
            }
            return res;
        }

        public List<string> GetSubjobIdentifiers(int jobID)
        {
            List<string> subjobs = new List<string>();
            try
            {
                using (CDMSDataContext cdmsdb = new CDMSDataContext(_connStr))
                {
                    subjobs = cdmsdb.SubJobs.Where(s => s.iFK_JobID == jobID)
                        .Select(s => s.vch_SubJobIdentifier.ToUpper())
                        .Distinct()
                        .ToList();
                }
                // remove SJs with trailing letters
                for (int i = subjobs.Count - 1; i >= 0; i--)
                {
                    if (MultipackSJID_RE.IsMatch(subjobs[i]))
                        subjobs.Remove(subjobs[i]);
                }
                subjobs.Sort(new SubjobIDComparer());
            }
            catch
            {
                throw new ApplicationException("Unable to access CDMS database.");
            }
            return subjobs;
        }

        public int GetCounterValue(string counterName)
        {
            int counter = 0;
            try
            {
                using (CDMSDataContext cdmsdb = new CDMSDataContext(_connStr))
                {
                    counter = cdmsdb.Counters
                        .Where(c => c.vch_Name.Equals(counterName))
                        .Select(c => c.i_NextValue)
                        .FirstOrDefault() ?? 0;
                }
            }
            catch
            {
                throw new ApplicationException("Unable to access CDMS database.");
            }
            return counter;
        }

        public void UpdateCounter(string counterName, int newValue)
        {
            try
            {
                using (CDMSDataContext cdmsdb = new CDMSDataContext(_connStr))
                {
                    Counter counter = cdmsdb.Counters
                        .Where(c => c.vch_Name.Equals(counterName)).FirstOrDefault();
                    if (counter != null)
                    {
                        counter.i_NextValue = newValue;
                        cdmsdb.SubmitChanges();
                    }
                }
            }
            catch
            {
                throw new ApplicationException("Unable to access CDMS database.");
            }
        }

        /// <summary>
        /// Returns empty string if not found. Throws exception if data field values differ between subjobs
        /// </summary>
        public string GetDatafieldValue(string jobcode, string subjobID, string datafieldName)
        {
            List<CDMSSubjobData> sjData = new List<CDMSSubjobData>();
            string value = string.Empty;
            using (CDMSDataContext cdmsdb = new CDMSDataContext(_connStr))

            try
            {
                string identifierBase = GetBaseIdentifier(subjobID);
                var query = cdmsdb.Jobs
                    .Join(cdmsdb.SubJobs, j => j.iPK_JobID, s => s.iFK_JobID, (j, s) => new { j, s })
                    .Join(cdmsdb.SubJobDatas, js => js.s.iPK_SubJobID, d => d.iFK_SubJobID, (js, d) => new { js, d })
                    .Join(cdmsdb.lu_StaticDataFieldNames, jsd => jsd.d.iFK_DataFieldID, l => l.iPK_DataFieldID, (jsd, l) => new { jsd, l })
                    .Where(x => x.jsd.js.j.vch_JobCode == jobcode && x.jsd.js.s.vch_SubJobIdentifier == subjobID && x.l.vch_Name == datafieldName);
                sjData = query.Select(sjd => new CDMSSubjobData()
                {
                    SubjobDataID = sjd.jsd.d.iPK_SubJobDataID,
                    DataFieldID = sjd.jsd.d.iFK_DataFieldID,
                    DataFieldValue = sjd.jsd.d.vch_DataFieldValue
                }).ToList();

               if (sjData.Count > 0)
                {
                    for (int i = 1; i < sjData.Count; i ++)
                    {
                        if (sjData[0].DataFieldValue != sjData[i].DataFieldValue)
                        {
                            throw new ArgumentException($"Subjobs have different values for {datafieldName}. All subjobs must have same data field value.");
                        }
                    }

                    value = sjData[0].DataFieldValue.Trim();
                }
            }
            catch (ArgumentException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new Exception($"CDMS database access error: {ex.Message}");
            }

            return value;
        }

	    public int GetMPFileCount(string jobcode, string subjobID)
	    {
		    int value = 0;
		    try
		    {
			    CDMSJob job = GetJob(jobcode);
			    using (CDMSDataContext cdmsdb = new CDMSDataContext(_connStr))
			    {
				    value = cdmsdb.SubJobs
					    .Where(s => s.iFK_JobID == job.JobID && s.vch_SubJobIdentifier.StartsWith(subjobID) && s.vch_SubJobIdentifier.Length > 4).Count();
			    }
		    }
		    catch
		    {
			    throw new ApplicationException("Unable to access CDMS database.");
			}

		    return value;
	    }
        public string GetProductDataValue(string jobcode, string subjobID, string dataFieldName)
        {
            string value = string.Empty;
            try
            {
                using (CDMSDataContext cdmsdb = new CDMSDataContext(_connStr))
                {
                    value = cdmsdb.Jobs
                        .Join(cdmsdb.SubJobs, j => j.iPK_JobID, s => s.iFK_JobID, (j, s) => new { j.vch_JobCode, s })
                        .Join(cdmsdb.Products, js => js.s.iFK_ProductID, p => p.iPK_ProductID, (js, p) => new { js, p.iPK_ProductID })
                        .Join(cdmsdb.ProductDatas, jsp => jsp.iPK_ProductID, d => d.iFK_ProductID, (jsp, d) => new { jsp, d })
                        .Join(cdmsdb.lu_StaticDataFieldNames, jspd => jspd.d.iFK_DataFieldID, l => l.iPK_DataFieldID, (jspd, l) => new { jspd, l.vch_Name })
                        .Where(x => x.jspd.jsp.js.vch_JobCode == jobcode && x.jspd.jsp.js.s.vch_SubJobIdentifier == subjobID && x.vch_Name == dataFieldName)
                        .Select(q => q.jspd.d.vch_DataFieldValue)
                        .FirstOrDefault() ?? string.Empty;
                }
            }
            catch
            {
                throw new ApplicationException("Unable to access CDMS database.");
            }
            return value;
        }
        /// <summary>
        /// Gets CustomerProductID field from subjob's product
        /// </summary>
        /// <param name="jobcode"></param>
        /// <param name="subjobID"></param>
        /// <returns></returns>
        public string GetCustomerProductID(string jobcode, string subjobID)
        {
            string cid = string.Empty;
            try
            {
                using (CDMSDataContext cdmsdb = new CDMSDataContext(_connStr))
                {
                    cid = cdmsdb.Jobs
                        .Join(cdmsdb.SubJobs, j => j.iPK_JobID, s => s.iFK_JobID, (j, s) => new { j.vch_JobCode, s })
                        .Join(cdmsdb.Products, js => js.s.iFK_ProductID, p => p.iPK_ProductID, (js, p) => new { js, p })
                        .Where(x => x.js.vch_JobCode == jobcode && x.js.s.vch_SubJobIdentifier == subjobID)
                        .Select(x => x.p.vch_CustomerProductID)
                        .FirstOrDefault() ?? string.Empty;
                }
            }
            catch
            {
                throw new ApplicationException("Unable to access CDMS database.");
            }
            return cid;
        }

        public List<CDMSSubjob> GetSubjobsWithTables(string jobcode, string subjobIdentifier)
        {
            List<CDMSSubjob> subjobs = new List<CDMSSubjob>();
            try
            {
                using (CDMSDataContext cdmsdb = new CDMSDataContext(_connStr))
                {
                    string identifierBase = GetBaseIdentifier(subjobIdentifier);
                    var query = cdmsdb.Jobs
                        .Join(cdmsdb.SubJobs, j => j.iPK_JobID, s => s.iFK_JobID, (j, s) => new { j, s })
                        .Where(x => x.j.vch_JobCode == jobcode
                                    && x.s.vch_SubJobIdentifier.ToLower().StartsWith(identifierBase)
                                    && x.s.vch_CardTable != null)
                        .Select(x => x.s);
                    int carrierID = query.Join(cdmsdb.PackagingInfos, s => s.iPK_SubJobID, p => p.iFK_SubJobID, (s, p) => new { s, p })
                        .Where(x => x.p.i_PackingLevel == 2).Select(x => x.s.iPK_SubJobID).FirstOrDefault();
                    subjobs = query.Select(s => new CDMSSubjob()
                        {
                            SubjobID = s.iPK_SubJobID,
                            Identifier = s.vch_SubJobIdentifier,
                            Name = AppHelper.TrimCRLF(s.vch_Name),
                            CardTable = s.vch_CardTable,
                            IsCarrier = s.iPK_SubJobID == carrierID
                        }).ToList();
                }
            }
            catch
            {
                throw new ApplicationException("Unable to access CDMS database.");
            }
            return subjobs;
        }

        public int GetCount(string jobcode, string subjobIdentifier,  bool all)
        {
            int count = 0;
            using (SqlConnection con = new SqlConnection(AppHelper.ConnectionString))
            {
                con.Open();

                SqlCommand cmd = new SqlCommand(all ? "Util_GetProductionCount_all" : "Util_GetProductionCount", con)
                {
                    CommandTimeout = 600,
                    CommandType = CommandType.StoredProcedure
                };
                cmd.Parameters.AddWithValue("@JobCode", SqlDbType.VarChar).Value = jobcode;
                cmd.Parameters.AddWithValue("@SubjobIdentifier", SqlDbType.VarChar).Value = subjobIdentifier;

                count = DBHelper.ExecuteScalarSafe(cmd, 0);

                con.Close();
            }

            return count;
        }

        /// <summary>
        /// Returns subjob identifier w/o any trailing letters
        /// </summary>
        public string GetBaseIdentifier(string identifier)
        {
            Match match = MultipackSJID_RE.Match(identifier);
            return match.Success ? match.Groups[1].Value.ToUpper() + match.Groups[2].Value : identifier;
        }
    }
    public class CDMSJob
    {
        public int JobID { get; set; } = 0;
        public string JobCode { get; set; } = string.Empty;
        public string JobDescription { get; set; } = string.Empty;
        public string ProductionDB { get; set; } = string.Empty;
        public bool IsMultipack { get; set; } = false;

        public CDMSJob() { }
    }

    public class CDMSSubjob
    {
        private Regex _prefix = new Regex(@"SB\d\d *\(C.*?\) *", RegexOptions.IgnoreCase);

        public int SubjobID { get; set; } = 0;
        public string Identifier { get; set; } = string.Empty;
        public string Name { get; set; } = string.Empty;
        public string CardTable { get; set; } = string.Empty;
        public bool IsCarrier { get; set; } = false;
        public ulong MinControlNumber { get; set; } = 0;
        public string NameShort
        {
            get
            {
                string shortName = Name;
                Match sbMatch = _prefix.Match(Name);
                if (sbMatch.Success)
                    shortName = Name.Substring(sbMatch.Length);
                return string.IsNullOrWhiteSpace(shortName) ? Name : shortName;
            }
        }
    }

    public class CDMSSubjobData
    {
        public int SubjobDataID = 0;
        public int DataFieldID = 0;
        public string DataFieldValue = string.Empty;
    }

    public class SubjobIDComparer : IComparer<string>
    {
        private readonly Regex SJID_RE = new Regex(@"^(T|S[BIE])(\d+)$", RegexOptions.IgnoreCase);

        public int Compare(string x, string y)
        {
            if (x == null && y == null)
                return 0;
            if (x == null)
                return -1;
            if (y == null)
                return 1;

            x = x.ToUpper();
            y = y.ToUpper();

            Match xmatch = SJID_RE.Match(x);
            Match ymatch = SJID_RE.Match(y);
            if (!xmatch.Success && !ymatch.Success)
                return 0;
            if (!xmatch.Success)
                return -1;
            if (!ymatch.Success)
                return 1;

            int prefixCompare = xmatch.Groups[1].Value.CompareTo(ymatch.Groups[1].Value);
            if (prefixCompare != 0)
                return prefixCompare;

            return Int32.Parse(xmatch.Groups[2].Value).CompareTo(Int32.Parse(ymatch.Groups[2].Value));
        }
    }
}
