﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using PrepaidCommon;
using PrepaidSecurity;

namespace CNGenUtil
{
    /// <summary>
    /// Interaction logic for CNGenUtilView.xaml
    /// </summary>
    public partial class CNGenUtilView : Window
    {
        private CNGenUtilViewModel _viewModel;
		public CNGenUtilView()
        {
            InitializeComponent();
            DBHelper.Login();
            if (!SecurityManager.Get.Authenticate())
            {
                MessageBox.Show("Authentication failed. Please contact Support.", "Error", 
                    MessageBoxButton.OK, MessageBoxImage.Error);
                Close();
            }
            if (!SecurityManager.Get.Authorize())
            {
                MessageBox.Show("You are not authorized to use this application. Please contact Support.", "Error", 
                    MessageBoxButton.OK, MessageBoxImage.Error);
                Close();
            }
            _viewModel = new CNGenUtilViewModel();
            DataContext = _viewModel;
            _viewModel.Initialize();
            ReturnFileType_SelectionChanged(null, null);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            _viewModel.ClearSubjobs();
        }

        private void ReturnFileType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (_viewModel.SelectedReturnFile.HasTestCardArgument)
            {
                _rdoProduction.Visibility = Visibility.Visible;
                _rdoTest.Visibility = Visibility.Visible;
                _spFileNamePreview.Visibility = Visibility.Hidden;
            }
            else
            {
                _rdoProduction.Visibility = Visibility.Hidden;
                _rdoTest.Visibility = Visibility.Hidden;
                _spFileNamePreview.Visibility = _viewModel.SelectedReturnFile.ShowFileNamePreview ? Visibility.Visible : Visibility.Hidden;
                _rfileText.Visibility = (_viewModel.SelectedReturnFile is RF_RFile) ? Visibility.Visible : Visibility.Hidden;
                _mpfileText.Visibility = (_viewModel.SelectedReturnFile is RF_MPFile) ? Visibility.Visible : Visibility.Hidden;
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            DBHelper.Logoff();
        }
    }
}
