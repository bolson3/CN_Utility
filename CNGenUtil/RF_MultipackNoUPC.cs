﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using PrepaidCommon;

namespace CNGenUtil
{
    public class RF_MultipackNoUPC : RF_Multipack
    {
        public override string DisplayName { get => "Multipack No UPC"; }
        protected override string SprocName { get => "util_FullCnMPGen"; }

        public RF_MultipackNoUPC(): base()
        {

        }
    }
}
