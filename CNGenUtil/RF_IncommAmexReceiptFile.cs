﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PrepaidCommon;
using System.Windows;
using System.IO;
using CsvHelper;
using System.Numerics;

namespace CNGenUtil
{
    public class RF_IncommAmexReceiptFile : RF_AmexBase
    {
        protected string _amexProductCode = string.Empty;

        public override string DisplayName { get; set; } = "Incomm Amex Receipt File(Validate only)";
        protected override string SprocName { get => "Gen_IncAmexReceiptTracking"; }


        public RF_IncommAmexReceiptFile(): base()
        {
        }

        protected override string CreateFileName()
        {
            DateParts dp = new DateParts();

            return $"CPI_VMS_{_amexProductCode}_{dp.Day}{dp.Month}{dp.YearShort}_{_amexCounter.PadLeft(4, '0')}_RSP.csv";
        }

        public override bool Initialize()
        {
            bool ret = base.Initialize();
            if (ret)
            {
                string errMsg = string.Empty;
                try
                {
                    CDMSDataController cdc = new CDMSDataController();
                    _amexProductCode = cdc.GetCustomerProductID(SelectedJobCode, SelectedSubjob);
                }
                catch (Exception ex)
                {
                    errMsg = $"Exception getting customer product ID for selected subjob: {ex.Message}";
                    ret = false;
                }

                if (!string.IsNullOrEmpty(errMsg))
                {
                    MessageBox.Show(errMsg, "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                    ret = false;
                }
            }
            return ret;
        }

        public override int ValidateFile()
        {
            ErrorLogC errorLog = new ErrorLogC();
            int totalErrors = 0;
            int lineNum = 0;
            int expectedLineLength = 0;
            int actualRecords = 0;

            // check line length and line count
            using (StreamReader rdr = new StreamReader(ReturnFilePath))
            {
                while (rdr.Peek() > 0)
                {
                    int lineLen = rdr.ReadLine().Length;
                    lineNum++;
                    if (lineNum == 2)
                    {
                        expectedLineLength = lineLen; // use length of first detail row
                    }
                    else if (lineNum > 2 && lineLen != expectedLineLength)
                    {
                        errorLog.Add(typeof(LineLengthError), lineNum, lineLen.ToString());
                        totalErrors++;
                    }
                }

                actualRecords = lineNum - 1; // exlude header
                if (actualRecords != _expectedFileCountFromJob)
                {
                    totalErrors++;
                }
            }

            // declare fields we'll track to make sure they're the same in all lines
            string expectedCustomer = string.Empty;
            string expectedParentOrderID = string.Empty;
            string expectedChildOrderID = string.Empty;
            string expectedFileDate = string.Empty;
            string expectedAmexCardType = string.Empty;
            string expectedAmexClientIdReceipt = string.Empty;

            using (TextReader fileReader = File.OpenText(ReturnFilePath))
            {
                if (fileReader.Peek() > 0)
                {
                    fileReader.ReadLine(); // consume header
                    lineNum = 1;
                }
                var csv = new CsvReader(fileReader);
                BigInteger lastControlVal = 0;
                BigInteger currentControlVal = 0;

                while (csv.Read())
                {
                    lineNum++;
                    if (lineNum == 2)
                    {
                        expectedCustomer = csv.GetField<string>(0); // CustomerError
                        expectedParentOrderID = csv.GetField<string>(2); // ParentOrderIDError
                        expectedChildOrderID = csv.GetField<string>(3); // ChildOrdeIDError
                        expectedFileDate = csv.GetField<string>(7);
                        expectedAmexCardType = csv.GetField<string>(8);
                        expectedAmexClientIdReceipt = csv.GetField<string>(9);
                    }

                    string customer = csv.GetField<string>(0);
                    if (customer != expectedCustomer)
                    {
                        errorLog.Add(typeof(CustomerError), lineNum, customer);
                        totalErrors++;
                    }

                    string parentOrderID = csv.GetField<string>(2);
                    if (parentOrderID != expectedParentOrderID)
                    {
                        errorLog.Add(typeof(ParentOrderIDError), lineNum, parentOrderID);
                        totalErrors++;
                    }

                    string childOrderID = csv.GetField<string>(3);
                    if (childOrderID != expectedChildOrderID)
                    {
                        errorLog.Add(typeof(ChildOrderIDError), lineNum, childOrderID);
                        totalErrors++;
                    }

                    string fileDate = csv.GetField<string>(7);
                    if (fileDate != expectedFileDate)
                    {
                        errorLog.Add(typeof(FileDateError), lineNum, fileDate);
                        totalErrors++;
                    }

                    string amexCardType = csv.GetField<string>(8);
                    if (amexCardType != expectedAmexCardType)
                    {
                        errorLog.Add(typeof(AmexCardTypeError), lineNum, amexCardType);
                        totalErrors++;
                    }

                    string amexClientId = csv.GetField<string>(9);
                    if (amexClientId != expectedAmexClientIdReceipt)
                    {
                        errorLog.Add(typeof(ClientOrderIDError), lineNum, amexClientId);
                        totalErrors++;
                    }

                    string serialNum = csv.GetField<string>(4);
                    BigInteger.TryParse(serialNum, out currentControlVal);
                    if (lastControlVal != 0)
                    {
                        if (currentControlVal != (lastControlVal + 1))
                        {
                            errorLog.Add(typeof(ControlNumberError), lineNum, currentControlVal.ToString());
                            totalErrors++;
                        }
                    }
                    lastControlVal = currentControlVal;
                }
            }

            // add error log headers as needed
            Dictionary<Type, string> errorHeaderParams = new Dictionary<Type, string>();
            errorHeaderParams.Add(typeof(LineLengthError), expectedLineLength.ToString());
            errorHeaderParams.Add(typeof(CustomerError), expectedCustomer);
            errorHeaderParams.Add(typeof(ParentOrderIDError), expectedParentOrderID);
            errorHeaderParams.Add(typeof(ChildOrderIDError), expectedChildOrderID);
            errorHeaderParams.Add(typeof(FileDateError), expectedFileDate);
            errorHeaderParams.Add(typeof(AmexCardTypeError), expectedAmexCardType);
            errorHeaderParams.Add(typeof(ClientOrderIDError), expectedAmexClientIdReceipt);

            // write the file
            WriteValidationFile(errorHeaderParams, actualRecords, totalErrors, errorLog);

            return totalErrors;
        }
    }
}
