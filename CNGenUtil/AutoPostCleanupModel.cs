﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.ComponentModel;
using PrepaidCommon;

namespace CNGenUtil
{
    public class AutoPostCleanupModel : ViewModelBase
    {
        private List<UnresolvedError> _errors;
        private UnresolvedError _selectedError = null;
        private string _selRecDetails = string.Empty;

        public RelayCommand ClearErrorsCommand { get; set; }

        public List<UnresolvedError> Errors
        {
            get => _errors;
            set
            {
                _errors = value;
                OnPropertyChanged(nameof(Errors));
            }
        }

        public UnresolvedError SelectedError
        {
            get => _selectedError;
            set
            {
                _selectedError = value;
                OnPropertyChanged(nameof(SelectedError));
                OnPropertyChanged(nameof(SelectedRecordDetails));
                OnPropertyChanged(nameof(ClearIsEnabled));
            }
        }

        public bool ClearIsEnabled
        {
            get { return SelectedError != null; }
        }

        public string SelectedRecordDetails
        {
            get { return SelectedError == null ? "Details" : $"Details for return file: {SelectedError.RFRec.ToString()}"; }
        }

        public AutoPostCleanupModel()
        {
            ClearErrorsCommand = new RelayCommand(ClearErrors);
            FillGrid();
        }

        private void FillGrid()
        {
            UtilitySupportController usc = new UtilitySupportController();
            Errors = usc.GetUnresolvedErrors();
        }

        private void ClearErrors(object obj)
        {
            if (SelectedError != null)
                ClearErrors();
        }

        public void ClearErrors()
        {
            PostState state = SelectedError.RFRec.PostState;
            if (state == PostState.Error)
                state = PostState.NoAutoPost;
            UtilitySupportController usc = new UtilitySupportController();
            usc.ClearError(SelectedError, state);
            FillGrid();
        }
    }

    public class UnresolvedError
    {
        public int ID { get; set; }

        [DisplayName("Job code")]
        public string JobCode { get; set; }

        [DisplayName("Subjob")]
        public string SubjobIdentifier { get; set; }

        [DisplayName("Return file type")]
        public string ReturnFileType { get; set; }

        [DisplayName("Date")]
        [TextAlignment(TextAlignment.Right)]
        public DateTime DateOccurred { get; set; }

        [DisplayName("Description")]
        public string Description { get; set; }

        public ReturnFileRecord RFRec { get; set; }
    }
}
