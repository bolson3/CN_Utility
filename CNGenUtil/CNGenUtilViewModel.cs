using PrepaidCommon;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using PrepaidSecurity;
using MessageBox = System.Windows.MessageBox;
using OpenFileDialog = Microsoft.Win32.OpenFileDialog;
using FolderBrowserDialog = System.Windows.Forms.FolderBrowserDialog;
using System.Linq;

namespace CNGenUtil
{
    public class CNGenUtilViewModel : ViewModelBase
	{
        private string _selectedJobCode;
		private string _selectedSubjob;
        private List<string> _availableSubjobs;
        private List<string> _subjobdetails;
        private CDMSJob _selectedJob = null;
        private List<UploadOption> _uploadOptions;
        private UploadOption _selectedUploadOption;
        private List<UploadOption> _uploadSiteNA;
        private List<UploadOption> _uploadSiteYesNo;
        private bool _canAutoPost = false;
		private string _siteName = string.Empty;
		private RF_Base _selectedReturnFile;
        private RF_RFile _rFileType;
		private RF_MPFile _mpFileType;
        private RF_Base _default = null;
        private RF_Base _defaultMP = null;
        private int _retFileLineCount = 0;
        private int _validationErrors = 0;
        private PostState _currentPostStatus = PostState.NoAutoPost;
        private string _postTime = string.Empty;
        private string _subjobDescription = string.Empty;
        private bool _generating = false;

        public RFileNameViewModel RFile { get => _rFileType.RFileNameArgs; }

        public MPFileNameViewModel MPFile { get => _mpFileType.MPFileNameArgs; }
		
		public string SelectedSubjob
		{
			get => _selectedSubjob;
			set
			{
				_selectedSubjob = value;
				OnPropertyChanged(nameof(SelectedSubjob));
                if (SelectedReturnFile != null)
                {
                    SetInitialUploadSelection(SelectedReturnFile.CanAutoUpload);
                }
                SubjobDescription = GetSubjobDescription();
			}
		}

        public string SubjobDescription
        {
            get => _subjobDescription;
            set
            {
                _subjobDescription = value;
                OnPropertyChanged(nameof(SubjobDescription));
            }
        }

		public string SelectedJobCode
		{
			get => _selectedJobCode;
			set
			{
				_selectedJobCode = value;
				OnPropertyChanged(nameof(SelectedJobCode));
                LoadSubjobs();
                UploadSite = string.Empty;
            }
		}

        public CDMSJob SelectedJob 
        {
            get => _selectedJob;
            set
            {
                _selectedJob = value;
                OnPropertyChanged(nameof(SelectedJob));
            }
        }

        public string DBLabel { get; }

        public bool CanAutoPost
        {
            get => _canAutoPost;
            set
            {
                _canAutoPost = value;
                OnPropertyChanged(nameof(CanAutoPost));
            }
        }

        public List<RF_Base> AvailReturnFileTypes { get; set; }

		public RF_Base SelectedReturnFile
		{
			get => _selectedReturnFile;
			set
			{
				_selectedReturnFile = value;
                OnPropertyChanged(nameof(SelectedReturnFile));
                OnPropertyChanged(nameof(ReturnFileName));
                OnPropertyChanged(nameof(ValidationFileName));
                OnPropertyChanged(nameof(RetFileLineCountStr));
                OnPropertyChanged(nameof(ValidationErrorCountStr));
                OnPropertyChanged(nameof(PostStatusStr));
                SetUploadOptions(_selectedReturnFile.CanAutoUpload);
                IsProduction = false;
                IsTest = false;
			}
		}

        public List<UploadOption> AvailableUploadOptions
        {
            get => _uploadOptions;
            set
            {
                _uploadOptions = value;
                OnPropertyChanged(nameof(AvailableUploadOptions));
            }
        }

        public UploadOption SelectedUploadOption
        {
            get => _selectedUploadOption;
            set
            {
                _selectedUploadOption = value;
                FillUploadSite();
                OnPropertyChanged(nameof(SelectedUploadOption));
            }
        }

        public UploadInfo UploadRecord { get; set; }

        private bool _isProduction;
        public bool IsProduction
        {
            get => _isProduction;
            set
            {
                _isProduction = value;
                OnPropertyChanged(nameof(IsProduction)); 
            }
        }

        private bool _isTest;
        public bool IsTest
        {
            get => _isTest;
            set
            {
                _isTest = value;
                OnPropertyChanged(nameof(IsTest));
            }
        }

        private double _uploadOpacity = 1.0;
        public double UploadOpacity
        {
            get
            {
                return _uploadOpacity;
            }
            set
            {
                _uploadOpacity = value;
                OnPropertyChanged(nameof(UploadOpacity));
            }
        }

        private string _uploadSite;
        public string UploadSite
        {
            get => _uploadSite;
            set
            {
                _uploadSite = value;
                OnPropertyChanged(nameof(UploadSite));
            }
        }

        private Brush _uploadTextColor = Brushes.Black;
        public Brush UploadTextColor
        {
            get => _uploadTextColor;
            set
            {
                _uploadTextColor = value;
                OnPropertyChanged(nameof(UploadTextColor));
            }
        }

        public List<string> AvailableSubjobs
        {
            get => _availableSubjobs;
            set
            {
                _availableSubjobs = value;
                OnPropertyChanged(nameof(AvailableSubjobs));
            }
        }

        public List<string> SubJobDetails
        {
            get => _subjobdetails;
            set
            {
                _subjobdetails = value;
                OnPropertyChanged(nameof(SubJobDetails));
            }
        }

        public string SiteName
        {
            get => _siteName;
            set
            {
                _siteName = value;
                OnPropertyChanged(nameof(SiteName));
            }
        }

        public string RetFileLineCountStr
        {
            get
            {
                return string.IsNullOrEmpty(SelectedReturnFile.ReturnFilePath) || !File.Exists(SelectedReturnFile.ReturnFilePath) ? "" : RetFileLineCount.ToString("N0");
            }
        }

        public int RetFileLineCount
        {
            get => _retFileLineCount;
            set
            {
                _retFileLineCount = value;
                OnPropertyChanged(nameof(RetFileLineCountStr));
            }
        }

        public string ValidationErrorCountStr
        {
            get
            {
                string file = SelectedReturnFile == null ? string.Empty : SelectedReturnFile.ValidationFileName;
                return string.IsNullOrEmpty(file) ? "" : ValidationErrorCount.ToString("N0");
            }
        }

        public int ValidationErrorCount
        {
            get => _validationErrors;
            set
            {
                _validationErrors = value;
                OnPropertyChanged(nameof(ValidationErrorCountStr));
            }
        }

        public string PostStatusStr
        {
            get
            {
                string statusStr = string.Empty;
                if (!string.IsNullOrEmpty(SelectedReturnFile.ReturnFileName))
                {
                    switch (CurrentPostStatus)
                    {
                        case PostState.Pending:
                            statusStr = "Auto-upload pending...";
                            break;
                        case PostState.Posted:
                            statusStr = $"Uploaded at {_postTime}";
                            break;
                        case PostState.Deferred:
                            statusStr = "User declined auto-upload option, so must upload manually";
                            break;
                        case PostState.Error:
                            statusStr = "Error (press 'View files status' for details)";
                            break;
                        case PostState.NoAutoPost:
                            statusStr = "This return file type is not configured for auto-upload";
                            break;
                        case PostState.Unknown:
                        default:
                            statusStr = "Unknown";
                            break;
                    }
                }
                return statusStr;
            }
        }

        public PostState CurrentPostStatus
        {
            get => _currentPostStatus;
            set
            {
                _currentPostStatus = value;
                OnPropertyChanged(nameof(PostStatusStr));
            }
        }

        public string ReturnFileName
        {
            get => SelectedReturnFile != null && File.Exists(SelectedReturnFile.ReturnFilePath) ? SelectedReturnFile.ReturnFileName : string.Empty;
        }

        public string ValidationFileName
        {
            get => SelectedReturnFile != null && File.Exists(SelectedReturnFile.ValidationFilePath) ? SelectedReturnFile.ValidationFileName : string.Empty;
        }

        public RelayCommand ClearCommand { get; set; }
        public RelayCommand GenerateCommand { get; set; }
        public RelayCommand ValidateCommand { get; set; }
        public RelayCommand ChangeFileNameArgsCommand { get; set; }
        public RelayCommand ConfigureCommand { get; set; }
        public RelayCommand ViewStatusCommand { get; set; }
        public RelayCommand RefreshPostStatusCommand { get; set; }
        public RelayCommand CancelPostCommand { get; set; }
        public RelayCommand OpenReturnFileCommand { get; set; }
        public RelayCommand OpenValidationFileCommand { get; set; }
        public RelayCommand OpenReturnFolderCommand { get; set; }

        public static CNGenUtilViewModel Instance { get; internal set; }

        public CNGenUtilViewModel()
		{
            Instance = this;

            ChangeFileNameArgsCommand = new RelayCommand(action => ChangeFileNameArgs());
            GenerateCommand = new RelayCommand(action => Generate(), canExecute =>
                SelectedReturnFile != null
                && SelectedJobCode != string.Empty
                && SelectedSubjob != string.Empty
                && (SelectedUploadOption.PostType != PostState.Pending || UploadRecord.ErrorState == UploadSiteStatus.OK));
            ValidateCommand = new RelayCommand(action => Validate(), canExecute =>
                SelectedReturnFile != null
                && SelectedJobCode != string.Empty
                && SelectedSubjob != string.Empty);
            ClearCommand = new RelayCommand(action => ClearUi());
            ConfigureCommand = new RelayCommand(action => ConfigureUploadSite(), canExecute =>
                UploadRecord != null
                && SelectedUploadOption.PostType == PostState.Pending 
                && UploadRecord.ErrorState == UploadSiteStatus.NoSiteforUPC);
            ViewStatusCommand = new RelayCommand(action => ViewStatus());
            RefreshPostStatusCommand = new RelayCommand(action => RefreshPostStatus(), canExecute => CurrentPostStatus == PostState.Pending);
            CancelPostCommand = new RelayCommand(action => CancelPost(), canExecute => CurrentPostStatus == PostState.Pending);
            OpenReturnFileCommand = new RelayCommand(action => OpenReturnFile(), canExecute => !string.IsNullOrEmpty(ReturnFileName));
            OpenReturnFolderCommand = new RelayCommand(action => OpenReturnFolder(), canExecute => !string.IsNullOrEmpty(ReturnFileName));
            OpenValidationFileCommand = new RelayCommand(action => OpenValidationFile(), canExecute => !string.IsNullOrEmpty(ValidationFileName));

            // initialize return file types list
            List<RF_Base> typesOfCNFiles = new List<RF_Base>();

            if (AppHelper.TargetDB.Equals("TEST"))
            {
                _default = new RF_FakeCN();
                typesOfCNFiles.Add(_default);
            }

            _rFileType = new RF_RFile();
            _mpFileType = new RF_MPFile();
            _defaultMP = new RF_MultipackNoUPC();

            if (AppHelper.TargetDB.Equals("PRODUCTION") || SecurityManager.Get.UserIsSystemAdmin())
            {
                string trigger = AppHelper.GetAppSetting("Trigger", "any");
                if (trigger.Equals("import") || trigger.Equals("any"))
                {
                    _default = new RF_StandardNoUPC();
                    typesOfCNFiles.Add(_default);
                    typesOfCNFiles.Add(new RF_StandardWithUPC());
                    typesOfCNFiles.Add(new RF_AmexSHIP());
                    typesOfCNFiles.Add(new RF_IncommAmexShipmentFile());
                    typesOfCNFiles.Add(new RF_BHNStandardEASN());
                    typesOfCNFiles.Add(new RF_BHNSimonNoBundleEASN());
                    typesOfCNFiles.Add(new RF_IncommAmexVRFFile());
                    typesOfCNFiles.Add(_rFileType);
                    typesOfCNFiles.Add(_mpFileType);
                    typesOfCNFiles.Add(_defaultMP);
                    typesOfCNFiles.Add(new RF_MultipackWithUPC());
                    //typesOfCNFiles.Add(new RF_IncommAmexReceiptFile()); 
                }
                if (trigger.Equals("warehouse") || trigger.Equals("any"))
                {
                    typesOfCNFiles.Add(new RF_NetspendReceiptFile());
                }
            }

            AvailReturnFileTypes = typesOfCNFiles;

            _uploadSiteYesNo = new List<UploadOption>();
            _uploadSiteYesNo.Add(new UploadOption("Yes", PostState.Pending, "File will be automatically uploaded to SFTP"));
            _uploadSiteYesNo.Add(new UploadOption("No", PostState.Deferred, "File will not be automatically uploaded to SFTP"));
            _uploadSiteNA = new List<UploadOption>();
            _uploadSiteNA.Add(new UploadOption("Not applicable", PostState.NoAutoPost, "Selected return file type is not set for automatic posting to SFTP"));

            DBLabel = "DB: " + AppHelper.TargetDB;
        }

        public void Initialize()
        {
            try
            {
                UtilitySupportController usc = new UtilitySupportController();
                Dictionary<string, ReturnFileURLInfo> rfURLs = usc.GetReturnFileURLs();

                // update return file types w/ upload URLs
                foreach (RF_Base rft in AvailReturnFileTypes)
                {
                    if (rfURLs.ContainsKey(rft.DisplayName)) // update return file type with its configured upload URL
                    {
                        ReturnFileURLInfo rfui = rfURLs[rft.DisplayName];
                        rft.CanAutoUpload = true;
                        rft.UploadURL = rfui.UploadURL;
                        rft.UploadFolder = rfui.UploadFolder;
                        rft.UploadServerName = rfui.UploadServerName;
                    }
                }
            }
            catch (Exception e)
            {
                DBHelper.LogMessage($"Failed to initialize return files: {e.Message}");
            }
            ClearUi();
        }

        private void LoadSubjobs()
        {
            if (string.IsNullOrWhiteSpace(SelectedJobCode))
            {
                SelectedJob = null;
                return;
            }
            try
            {
                string errMsg = string.Empty;
                using (WaitCursor wc = new WaitCursor())
                {
                    CDMSDataController cdc = new CDMSDataController();
                    SelectedJob = cdc.GetJob(SelectedJobCode);
                    if (SelectedJob == null)
                    {
                        errMsg = $"Job not found: {SelectedJobCode}";
                    }
                    else
                    {
                        List<string> subjobs = new List<string>();
                        subjobs = cdc.GetSubjobIdentifiers(SelectedJob.JobID);
                        if (subjobs.Count <= 0)
                        {
                            errMsg = $"No subjobs found for job {SelectedJob.JobCode}";
                        }
                        else
                        {
                            AvailableSubjobs = subjobs;
                        }
                        SelectedReturnFile = SelectedJob.IsMultipack ? _defaultMP : _default;
                    }
                }
                if (!string.IsNullOrEmpty(errMsg))
                {
                    MessageBox.Show(errMsg, "Not found", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(string.Format("Failure accessing CDMS database ({0})", e.Message), "Database error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        public void ClearSubjobs()
        {
            if (AvailableSubjobs != null && AvailableSubjobs.Count > 0)
            {
                AvailableSubjobs = new List<string>();
                SelectedSubjob = string.Empty;
            }
            ClearResults();
        }

        private string GetSubjobDescription()
        {
            string res = string.Empty;
            if (!(string.IsNullOrEmpty(SelectedJobCode) || string.IsNullOrEmpty(SelectedSubjob)))
            {
                CDMSDataController cdc = new CDMSDataController();
                CDMSSubjob sj = cdc.GetSubjobsWithTables(SelectedJobCode, SelectedSubjob).FirstOrDefault();
                if (sj != null)
                {
                    res = sj.NameShort;
                }
            }
            return res;
        }

        private bool GetFileToValidate()
		{
            bool res = false;
            OpenFileDialog dialog = new OpenFileDialog()
            {
                Title = "Select file to validate",
                Multiselect = false,
                Filter = "Return files|*.csv; *.txt|All files (*.*)|*.*",
                InitialDirectory = GetDefaultFolder()
            };

            if (dialog.ShowDialog() == true)
            {
                SelectedReturnFile.SetReturnFile(dialog.FileName);
                res = true;
            }
            return res;
		}

        private void ConfigureUploadSite()
        {
            ConfigureUploadSite uploadWindow = new ConfigureUploadSite(this);
            if (uploadWindow.ShowDialog() == true)
            {
                FillUploadSite();
            }
        }

        public bool UserConfirmation(ReturnFileRecord rfr, bool autoUploadSelected)
        {
            bool res = true;
            string rfMessage = rfr == null ? string.Empty :
                $@"{SelectedReturnFile.MessagePrefix} has previously been generated: {rfr.FolderName}\{rfr.FileName}. ";

            if (SelectedReturnFile.CanAutoUpload)
            {
                if (rfr == null)
                {
                    if (autoUploadSelected)
                    {
                        string msg = $@"Are you sure you want this return file to be automatically uploaded to SFTP?";
                        if (SelectedReturnFile.HasTestCardArgument)
                        {
                            string pOrT = IsProduction ? "PRODUCTION" : "TEST";
                            msg = $"You have selected '{pOrT}' as the type for this return file.\n\n" + 
                                $"Before proceeding, PLEASE make sure this is the correct selection for the return file you are generating.\n\n{msg}";
                        }
                        res = MessageBoxResult.Yes == MessageBox.Show(msg, "Automatic upload warning", MessageBoxButton.YesNo, MessageBoxImage.Warning);
                    }
                }
                else // record exists; message depends on state of previous file
                {
                    if (rfr.PostState == PostState.Pending) // not safe to update it
                    {
                        MessageBox.Show($"{rfMessage}\n\nThis file is currently in the 'pending' list waiting for automatic upload to SFTP, "
                            + "so it cannot be updated at the moment. \n\n"
                            + "PLEASE NOTE: If you deleted or moved that file right after generating it, or accidentally saved it to a non-reachable location, "
                            + "just wait about 5 minutes. This allows the uploading service to remove the file "
                            + "from the 'pending' list. Then you can re-generate the file without getting this message (and you can still choose to auto-upload if you wish).",
                            "Return file already pending upload to SFTP", MessageBoxButton.OK, MessageBoxImage.Information);
                        res = false;
                    }
                    else if (rfr.PostState == PostState.Posted) // warn user of potential second upload to SFTP
                    {
                        string title = "Return file has previously been uploaded to SFTP";
                        rfMessage += "\n\nThis file has already been uploaded to SFTP.";
                        if (autoUploadSelected)
                        {
                            MessageBox.Show($"{rfMessage} It cannot be set to automatically upload.",
                                title, MessageBoxButton.OK, MessageBoxImage.Error);
                            res = false;
                        }
                        else
                            res = MessageBoxResult.Yes == MessageBox.Show($"{rfMessage}\n\nDo you really want to update this record and set it for deferred upload?",
                                title, MessageBoxButton.YesNo, MessageBoxImage.Warning);
                    }
                    else if (autoUploadSelected)
                        res = MessageBoxResult.Yes == MessageBox.Show(
                            $"{rfMessage}\n\nAre you sure you want to update this file and have it automatically uploaded to SFTP?",
                            "Automatic upload warning", MessageBoxButton.YesNo, MessageBoxImage.Warning);
                    else
                        res = MessageBoxResult.Yes == MessageBox.Show($"{rfMessage}\n\nDo you really want to update this record? ",
                             "Return file already exists", MessageBoxButton.YesNo, MessageBoxImage.Question);
                }
            }
            else if (rfr != null) // not an auto-postable return file & record exists, so just display the message we used to display
            {
                res = MessageBoxResult.Yes == MessageBox.Show($"{rfMessage}\n\nDo you really want to update this record? ",
                    "Return file already exists", MessageBoxButton.YesNo, MessageBoxImage.Question);
            }
            return res;
        }

        public void ChangeFileNameArgs()
        {
	        if (SelectedReturnFile is RF_RFile)
	        {
                RFileNameViewModel tmpVM = new RFileNameViewModel(_rFileType.RFileNameArgs);
		        RFileNameView view = new RFileNameView(tmpVM);

		        if (view.ShowDialog() == true)
		        {
                    _rFileType.RFileNameArgs = tmpVM;
                    OnPropertyChanged(nameof(RFile));
		        }

			}

	        if (SelectedReturnFile is RF_MPFile)
	        {
				MPFileNameViewModel tmpVM = new MPFileNameViewModel(_mpFileType.MPFileNameArgs);
				MPFileNameView view = new MPFileNameView(tmpVM);

		        if (view.ShowDialog() == true)
		        {
                    _mpFileType.MPFileNameArgs = tmpVM;
                    OnPropertyChanged(nameof(MPFile));
		        }
	        }
		}

        private string GetDefaultFolder()
        {
            string saveToLoc = AppHelper.GetAppSetting("DestinationFolder", @"\\mnmainfs00\r_shared\Data Management Team");
            if (!string.IsNullOrEmpty(SelectedJobCode))
            {
                string tmpFolder = AppHelper.GetJobFolder(SelectedJobCode);
                if (Directory.Exists(tmpFolder))
                {
                    saveToLoc = tmpFolder;
                    tmpFolder = Path.Combine(saveToLoc, "Return File");
                    if (Directory.Exists(tmpFolder))
                    {
                        saveToLoc = tmpFolder;
                    }
                }
            }
            return saveToLoc;
        }

        private bool GetDestinationFolder(out string uncPath)
        {
            uncPath = string.Empty;

            FolderBrowserDialog dialog = new FolderBrowserDialog()
            {
                Description = "Select the folder where you want to save the return file.",
                SelectedPath = GetDefaultFolder()
            };
            if (dialog.ShowDialog() != System.Windows.Forms.DialogResult.OK)
            {
                return false;
            }
            
            if (!Directory.Exists(dialog.SelectedPath))
            {
                MessageBox.Show($@"The specified location '{dialog.SelectedPath}' could not be found.", @"Folder not found",
                    MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }

            if (Directory.GetFiles(dialog.SelectedPath).Length > 0 && AppHelper.GetAppSetting("ShowFolderWarning", false))
            {
                MessageBox.Show($@"The specified location '{dialog.SelectedPath}' already contains files. Please ensure you are not duplicating return files",
                    @"Possible Duplicate Files", MessageBoxButton.OK, MessageBoxImage.Warning);
            }

            // Check for valid UNC path
            bool hasUNCpath = AppHelper.GetUNCPath(dialog.SelectedPath, out uncPath);
            if (SelectedUploadOption.PostType == PostState.Pending)
            {
                if (!hasUNCpath) // auto upload files must be in valid share drive
                {
                    MessageBox.Show(@"The specified destination folder cannot be converted to a valid UNC path (e.g., \\mnmainfs00\r_shared\...). "
                        + "Files set to automatic upload must be saved to a file server so they can be accessed by the uploading utility.",
                        "Invalid destination folder", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return false;
                }
                else
                {
                    UtilitySupportController usc = new UtilitySupportController();
                    if (uncPath.Contains(usc.GetAppSetting("SecureFileShareIP", "10.42.35.69"))) // file saved to secure-side file server - can't be reached from AutoPoster
                    {
                        MessageBox.Show($"Destination folder '{dialog.SelectedPath}' appears to be the secure-side file server, which the auto-posting service cannot access."
                            + "\nPlease select a corporate-visible destination folder.",
                            "Inaccessible destination folder", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return false;
                    }
                }
            }

            SelectedReturnFile.ReturnFileFolder = dialog.SelectedPath;
            return true;
        }

        /// <summary>
        /// Generates and validates a return file.
        /// </summary>
        public void Generate()
		{
            if (_generating)
            {
                return;
            }

            ClearResults();

            if (SelectedJob == null || string.IsNullOrWhiteSpace(SelectedSubjob))
            {
                MessageBox.Show("Please select a job and subjob", "Subjob missing", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }

            if (!SelectedReturnFile.Initialize()) 
            {
                return;
            }            
            
            if (!GetDestinationFolder(out string uncPath))
            {
                return;
            }

            // check for a previous upload
            UtilitySupportController usc = new UtilitySupportController();
            ReturnFileRecord rfr = usc.GetReturnFileRecord(SelectedJobCode, SelectedSubjob, SelectedReturnFile.Key(IsTest));
            if (rfr == null && !SelectedReturnFile.CustomReturnFileRecordCheck())
            {
                return;
            }

            // confirm selections with user - e.g., existing entry, and user does not want to overwrite it
            if (!UserConfirmation(rfr, SelectedUploadOption.PostType == PostState.Pending))
            {
                return;
            }

            BackgroundWorker worker = new BackgroundWorker()
            {
                WorkerReportsProgress = false,
                WorkerSupportsCancellation = false
            };
            PleaseWaitViewModel pwvm = new PleaseWaitViewModel(worker)
            {
                Title = "Please wait",
                LoadProgress = 50,
                LoadStatus = "Generating return file, please wait..."
            };
            PleaseWaitView pwv = new PleaseWaitView(pwvm)
            {
                WindowStartupLocation = WindowStartupLocation.CenterScreen
            };

            worker.DoWork += (s, e) =>
            {
                // generate the file
                _generating = true;
                SelectedReturnFile.GenerateFile();
                RetFileLineCount = File.Exists(SelectedReturnFile.ReturnFilePath) ? File.ReadAllLines(SelectedReturnFile.ReturnFilePath).Length : 0;
            };

            worker.RunWorkerCompleted += (s, e) =>
            {
                pwv.Close();
                if (e.Error != null)
                {
                    string context = $"Issue during file generation: {SelectedReturnFile.DisplayName} for {SelectedJobCode} {SelectedSubjob}";
                    DBHelper.LogMessage($"{context}: {e.Error.Message}");
                    MessageBox.Show($"{context}\n\n{e.Error.Message}", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else
                {
                    // check validation prerequisites
                    if (!SelectedReturnFile.ValidateInitialize(out string errMsg))
                    {
                        MessageBox.Show($"'{SelectedReturnFile.Key(IsTest)}' was generated, " +
                            $"but validation did not occur due to the following issue:\n\n{errMsg}",
                            "File generated but unable to validate", MessageBoxButton.OK, MessageBoxImage.Warning);
                    }
                    else // validate the generated file
                    {
                        try
                        {
                            using (WaitCursor wc = new WaitCursor())
                            {
                                ValidationErrorCount = SelectedReturnFile.ValidateFile();
                            }

                            if (ValidationErrorCount == 0) // success - generated and validated, so update the ReturnFileTable accordingly
                            {
                                usc.UpdateReturnFileTable(SelectedJobCode, SelectedSubjob, SelectedReturnFile.Key(IsTest), SelectedReturnFile.ReturnFileName, uncPath,
                                    SelectedUploadOption.PostType, UploadSite, SelectedReturnFile.UploadURL);
                                CurrentPostStatus = SelectedUploadOption.PostType;
                                string autoUploadMsg = SelectedReturnFile.CanAutoUpload ? (SelectedUploadOption.PostType == PostState.Pending ?
                                    $"\n\nThis return file will be automatically uploaded to SFTP within the next 5 minutes. " +
                                    "Please do not move the file during this time, or it will not be uploaded" :
                                    $"\n\nThis return file will *not* be automatically uploaded to SFTP.") :
                                    string.Empty;
                                string validationClause = SelectedReturnFile.PerformsValidation ? " and validated successfully," : " successfully";
                                MessageBox.Show($"'{SelectedReturnFile.Key(IsTest)}' return file was generated{validationClause}.{autoUploadMsg}",
                                    "Success", MessageBoxButton.OK, MessageBoxImage.Information);
                            }
                            else // validation errors
                            {
                                string msg = $"A '{SelectedReturnFile.Key(IsTest)}' return file was generated, but the validation failed." +
                                                $"\n\nErrors found: {ValidationErrorCount}";
                                MessageBox.Show(msg, "File generated but validation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                            }
                        }
                        catch (Exception ex)
                        {
                            string context = $"Exception during file validation: {SelectedReturnFile.DisplayName} for {SelectedJobCode} {SelectedSubjob}";
                            DBHelper.LogMessage($"{context}: {ex.Message}");
                            MessageBox.Show($"{context}\n\n{ex.Message}", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        }
                    }
                }
                OnPropertyChanged(nameof(ReturnFileName));
                OnPropertyChanged(nameof(ValidationFileName));
                _generating = false;
            };

            pwv.Show();
            worker.RunWorkerAsync();
		}

	    public void Validate()
	    {
            if (SelectedJob == null || string.IsNullOrWhiteSpace(SelectedSubjob))
            {
                MessageBox.Show("Please select a job and subjob", "Subjob missing", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }

            if (!SelectedReturnFile.PerformsValidation)
            {
                MessageBox.Show($"{SelectedReturnFile.DisplayName} return files do not support validation.", "Not applicable", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }

            if (!GetFileToValidate())
            {
                return;
            }
            RetFileLineCount = File.Exists(SelectedReturnFile.ReturnFilePath) ? File.ReadAllLines(SelectedReturnFile.ReturnFilePath).Length : 0;

            try
            {
                // check validation prerequisites
                if (SelectedReturnFile.ValidateInitialize(out string errMsg))
                {
                    using (WaitCursor wc = new WaitCursor())
                    {
                        ValidationErrorCount = SelectedReturnFile.ValidateFile();
                    }

                    if (ValidationErrorCount == 0) // success - generated and validated, so update the ReturnFileTable accordingly
                    {
                        MessageBox.Show($"Return file was validated successfully",
                            "File validated", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    else // validation errors
                    {
                        string msg = $"The validation failed.\n\nErrors found: {ValidationErrorCount}.";
                        MessageBox.Show(msg, "Validation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                    }
                    OnPropertyChanged(nameof(SelectedReturnFile));
                }
                else
                {
                    MessageBox.Show($"{errMsg}", "Unable to validate", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
            catch (Exception ex)
            {
                string context = $"Issue during file validation: {SelectedReturnFile.DisplayName} for {SelectedJobCode} {SelectedSubjob}";
                DBHelper.LogMessage($"{context}: {ex.Message}");
                MessageBox.Show($"{context}\n\n{ex.Message}", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            OnPropertyChanged(nameof(ReturnFileName));
            OnPropertyChanged(nameof(ValidationFileName));
        }

        public void FillUploadSite()
        {
            UploadSite = string.Empty;
            SiteName = string.Empty;
            UploadOpacity = 0.4;
            if (_selectedUploadOption != null && _selectedUploadOption.PostType == PostState.Pending)
            {
                SetUploadSite();
                UploadSite = UploadRecord.DisplayName;
                UploadOpacity = 1.0;
                if (UploadRecord.ErrorState == UploadSiteStatus.OK)
                {
                    UploadTextColor = Brushes.Black;
                    SiteName = SelectedReturnFile.UploadServerName;
                }
                else
                {
                    UploadTextColor = Brushes.Red;
                    SiteName = string.Empty;
                }
            }
        }

        /// <summary>
        /// Sets the upload site information configured for the selected subjob.
        /// </summary>
        private void SetUploadSite()
        {
            UploadRecord = new UploadInfo();
            if (!string.IsNullOrWhiteSpace(SelectedJobCode) && !string.IsNullOrWhiteSpace(SelectedSubjob) && SelectedReturnFile.CanAutoUpload)
            {
                if (!string.IsNullOrWhiteSpace(SelectedReturnFile.UploadFolder)) // use what's configured in the ReturnFileURLs table
                    UploadRecord.SetState(UploadSiteStatus.OK, SelectedReturnFile.UploadFolder);
                else // no UploadFolder, so must use package UPC to get upload site
                {
                    try
                    {
                        CDMSDataController cdc = new CDMSDataController();
                        string packingUPC = cdc.GetProductDataValue(SelectedJobCode, SelectedSubjob, "Package UPC");
                        if (!string.IsNullOrWhiteSpace(packingUPC))
                        {
                            UploadRecord.PackagingUPC = packingUPC;
                            UtilitySupportController usc = new UtilitySupportController();
                            string site = usc.GetUploadSite(packingUPC);
                            if (!string.IsNullOrWhiteSpace(site))
                                UploadRecord.SetState(UploadSiteStatus.OK, site);
                            else
                                UploadRecord.SetState(UploadSiteStatus.NoSiteforUPC, $"No upload site for subjob's packaging UPC");
                        }
                        else
                            UploadRecord.SetState(UploadSiteStatus.NoUPC, $"No UPC found for {SelectedSubjob} of {SelectedJobCode}");
                    }
                    catch (Exception e)
                    {
                        MessageBox.Show($"Failure accessing database ({e.Message})", "Database error", MessageBoxButton.OK, MessageBoxImage.Error);
                        UploadRecord.SetState(UploadSiteStatus.Error, "<Error>");
                    }
                }
            }
        }

        private void SetUploadOptions(bool canAutoUpload)
        {
            AvailableUploadOptions = canAutoUpload ? _uploadSiteYesNo : _uploadSiteNA;
            SetInitialUploadSelection(canAutoUpload);
            CanAutoPost = canAutoUpload;
        }

        private void SetInitialUploadSelection(bool canAutoUpload)
        {
            int selection = 0;
            if (canAutoUpload && AvailableUploadOptions.Count > 1)
            {
                SetUploadSite();
                if (UploadRecord.ErrorState != UploadSiteStatus.OK)
                    selection = 1;
            }
            SelectedUploadOption = AvailableUploadOptions[selection];
        }

        private void ClearUi()
		{
			SelectedJobCode = string.Empty;
			SelectedSubjob = string.Empty;
            ClearResults();
            SelectedReturnFile = AvailReturnFileTypes.Count < 1 ? new RF_Base() : AvailReturnFileTypes[0];
		}

        private void ClearResults()
        {
            if (SelectedReturnFile != null)
            {
                SelectedReturnFile.Reset();
                OnPropertyChanged(nameof(SelectedReturnFile));
            }
            RetFileLineCount = 0;
            ValidationErrorCount = 0;
            CurrentPostStatus = PostState.NoAutoPost;
        }

        private void ViewStatus()
        {
            ReturnFilesView retview = new ReturnFilesView();
            using (WaitCursor wc = new WaitCursor())
            {
                retview.Initialize();
            }
            retview.Show();
        }

        private void CancelPost()
        {
            if (MessageBox.Show("Are you sure you want to cancel the automatic upload?\n\nIf yes, it will be marked for Deferred upload.", 
                "Are you sure?", MessageBoxButton.YesNo, MessageBoxImage.Question)
                == MessageBoxResult.Yes)
            {
                UtilitySupportController usc = new UtilitySupportController();
                usc.CancelAutoUpload(SelectedJobCode, SelectedSubjob, SelectedReturnFile.Key(IsTest));
                RefreshPostStatus(usc);
            }
        }

        private void RefreshPostStatus()
        {
            RefreshPostStatus(new UtilitySupportController());
        }

        private void RefreshPostStatus(UtilitySupportController usc)
        { 
            ReturnFileRecord rfr = usc.GetReturnFileRecord(SelectedJobCode, SelectedSubjob, SelectedReturnFile.Key(IsTest));
            if (rfr != null)
            {
                if (rfr.PostState == PostState.Posted)
                {
                    _postTime = rfr.DatePosted == null ? "?" : ((DateTime)rfr.DatePosted).ToString("MM/dd/yy hh:mm");
                }
                CurrentPostStatus = rfr.PostState;
            }
            else
            {
                CurrentPostStatus = PostState.Unknown;
            }
        }

        private void OpenReturnFile()
        {
            AppHelper.OpenFile(SelectedReturnFile.ReturnFilePath);
        }

        private void OpenReturnFolder()
        {
            AppHelper.OpenFile(Path.GetDirectoryName(SelectedReturnFile.ReturnFilePath));
        }

        private void OpenValidationFile()
        {
            AppHelper.OpenFile(SelectedReturnFile.ValidationFilePath);
        }
    }

    public enum UploadSiteStatus
    {
        NoSubjob,
        NoUPC,
        NoSiteforUPC,
        OK,
        Error
    }

    public class UploadInfo
    {
        public string DisplayName { get; set; } = string.Empty;
        public string UploadSite { get; private set; } = string.Empty;
        public string PackagingUPC { get; set; } = string.Empty;
        public UploadSiteStatus ErrorState { get; set; } = UploadSiteStatus.NoSubjob;
        public void SetState(UploadSiteStatus state, string value)
        {
            DisplayName = value;
            ErrorState = state;
            if (state == UploadSiteStatus.OK)
                UploadSite = value;
        }
        public UploadInfo()
        {
        }
    }

    public class UploadOption
    {
        public string DisplayName { get; internal set; } = string.Empty;
        public string Tooltip { get; set; } = string.Empty;
        public PostState PostType { get; internal set; } = PostState.NoAutoPost;
        public UploadOption()
        {

        }
        public UploadOption(string name, PostState state, string tooltip)
        {
            DisplayName = name;
            PostType = state;
            Tooltip = tooltip;
        }
    }
}
