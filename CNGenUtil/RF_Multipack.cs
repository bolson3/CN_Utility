﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using PrepaidCommon;
using System.Windows;
using System.Data.SqlClient;
using System.Data;

namespace CNGenUtil
{
    public class RF_Multipack : RF_CNBase
    {
        public override bool HasTestCardArgument { get => true; }

        public override bool IsMultipack { get => true; }

        public virtual string Header 
        { 
            get => "Magic_Number,Status,Carrier,Date,Tracking_Number,Merchant_ID,Merchant_Name,StorelocationID,Batch_Number,Case_Number,"
                 + "Pallet_Number,Serial_Number,Ship_To,Street_Address1,Street_Address2,City,State,Zip,DC_ID,Prod_ID,Order_ID,Parent_Serial_Number";
        }

        public RF_Multipack() : base()
        {

        }

        /// <summary>
        /// Write file using a sproc to get data. This works for MultipackCN.
        /// </summary>
        protected override void CreateFile()
        {
            // get carrier subjob and a list of card subjobs
            CDMSDataController cdc = new CDMSDataController();
            List<CDMSSubjob> subjobs = cdc.GetSubjobsWithTables(SelectedJobCode, SelectedSubjob);
            CDMSSubjob carrier = subjobs.Where(s => s.IsCarrier).FirstOrDefault();
            if (carrier == null)
                throw new Exception($"Unable to find carrier subjob for {SelectedJobCode}{SelectedSubjob}");
            List<CDMSSubjob> cards = subjobs.Where(s => !s.IsCarrier).ToList();

            // get min control numbers for each card subjob
            CDMSJob job = cdc.GetJob(SelectedJobCode);
            ProductionDBController pdc = new ProductionDBController(job.ProductionDB);
            pdc.GetMinControlNumbers(cards);
            cards.Sort(new CardSubjobComparer());

            // create a temp filename for each card subjob
            Dictionary<int, string> filenames = new Dictionary<int, string>();
            foreach (CDMSSubjob sj in cards)
                filenames.Add(sj.SubjobID, Path.Combine(ReturnFileFolder, $"tmpfile_{job.JobCode}_{sj.SubjobID}"));

            using (StreamWriter writer = new StreamWriter(ReturnFilePath))
            {
                using (SqlConnection conn = new SqlConnection(AppHelper.ConnectionString))
                {
                    conn.Open();
                    // write into each card subjob's temp file by invoking the sproc
                    foreach (CDMSSubjob card in cards)
                    {
                        using (SqlCommand cmd2 = BuildSprocCommandMultipack(conn, carrier.SubjobID, card.SubjobID))
                        {
                            using (SqlDataReader rdr = cmd2.ExecuteReader())
                            {
                                if (!rdr.HasRows)
                                    throw new ArgumentException($"No results returned for the specified job and subjob {card.Identifier}");
                                using (StreamWriter tmpWriter = new StreamWriter(filenames[card.SubjobID]))
                                {
                                    WriteFileBody(tmpWriter, rdr);
                                }
                            }
                        }
                    }
                }

                // make a dictionary of file readers to read those temp files we just created
                Dictionary<int, StreamReader> readers = new Dictionary<int, StreamReader>();
                foreach (CDMSSubjob card in cards)
                    readers.Add(card.SubjobID, new StreamReader(filenames[card.SubjobID]));

                // write the output file, starting with the header
                writer.WriteLine(Header);

                // read from the temp files, interleaving results into the output file 
                while (!EndOfStream(readers.Values))
                {
                    foreach (CDMSSubjob card in cards)
                        writer.WriteLine(readers[card.SubjobID].ReadLine());
                }

                // cleanup 
                foreach (CDMSSubjob card in cards)
                {
                    readers[card.SubjobID].Close(); // close stream readers
                    readers[card.SubjobID].Dispose(); // dispose the readers (since we didn't do a "using")
                    File.Delete(filenames[card.SubjobID]); // delete temp files
                }
            }
        }

        private SqlCommand BuildSprocCommandMultipack(SqlConnection conn, int carrierID, int cardID)
        {
            SqlCommand cmd = new SqlCommand(SprocName, conn)
            {
                CommandTimeout = 600,
                CommandType = CommandType.StoredProcedure
            };
            cmd.Parameters.AddWithValue("@JobCode", SqlDbType.VarChar).Value = SelectedJobCode;
            cmd.Parameters.AddWithValue("@SubjobIdentifier", SqlDbType.VarChar).Value = SelectedSubjob;
            cmd.Parameters.AddWithValue("@CardID", SqlDbType.Int).Value = cardID;
            cmd.Parameters.AddWithValue("@CarrierID", SqlDbType.Int).Value = carrierID;
            cmd.Parameters.AddWithValue("@TestOnly", SqlDbType.Int).Value = IsTest ? 1 : 0;
            return cmd;
        }

        private bool EndOfStream(IEnumerable<StreamReader> readers)
        {
            foreach (StreamReader rdr in readers)
                if (rdr.EndOfStream)
                    return true;
            return false;
        }

        protected override int GetCardCount()
        {
            int count = GetCountMP(false);
            if (IsTest)
            {
                count = GetCountMP(true) - count;
            }
            return count;
        }

        protected int GetCountMP(bool all)
        {
            int count = 0;
            CDMSDataController cdc = new CDMSDataController();
            List<CDMSSubjob> cardSubjobs = cdc.GetSubjobsWithTables(SelectedJobCode, SelectedSubjob).Where(s => !s.IsCarrier).ToList();
            foreach (CDMSSubjob subjob in cardSubjobs)
                count += GetCountForCard(subjob.SubjobID, all);
            return count;
        }

        private int GetCountForCard(int cardID, bool all)
        {
            int count = 0;
            using (SqlConnection con = new SqlConnection(AppHelper.ConnectionString))
            {
                con.Open();

                SqlCommand cmd = new SqlCommand("Util_GetProductionCountByCardID", con)
                {
                    CommandTimeout = 600,
                    CommandType = CommandType.StoredProcedure
                };
                cmd.Parameters.AddWithValue("@JobCode", SqlDbType.VarChar).Value = SelectedJobCode;
                cmd.Parameters.AddWithValue("@CardID", SqlDbType.Int).Value = cardID;
                cmd.Parameters.AddWithValue("@All", SqlDbType.Int).Value = all ? 1 : 0;

                count = DBHelper.ExecuteScalarSafe(cmd, 0);

                con.Close();
            }

            return count;
        }
    }
}
