﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Reflection;
using System.ComponentModel;
using PrepaidCommon;

namespace CNGenUtil
{
    public class UtilitySupportController : UtilitySupportBase
    {
        public UtilitySupportController()
        {
        }

        /// <summary>
        /// Returns all records for the specified returnFileType. 
        /// Note that for SHIP files, returnFileType is the name of the source file.
        /// </summary>
        public List<ReturnFileRecord> GetReturnFileRecords(string returnFileType)
        {
            List<ReturnFileRecord> records = null;
            using (UtilitySupportDataContext usdc = new UtilitySupportDataContext(_connStr))
            {
                try
                {
                    records = usdc.NewReturnFiles
                        .Where(r => r.vch_ReturnFileType.Equals(returnFileType))
                        .Select(r => new ReturnFileRecord(r))
                        .ToList();
                }
                catch (Exception e)
                {
                    throw new ArgumentException($"Error getting return file records: {e.Message}");
                }
            }
            return records;
        }

        public List<ReturnFileRecord> GetReturnFileRecords(string jobcode, string subjob, ReturnFileCategory cat)
        {
            List<ReturnFileRecord> records = null;
            using (UtilitySupportDataContext usdc = new UtilitySupportDataContext(_connStr))
            {
                try
                {
                    var query = usdc.NewReturnFiles
                        .Where(r => r.vch_JobCode == jobcode && r.vch_SubjobId == subjob);
                    if (cat == ReturnFileCategory.Standard)
                        query = query.Where(r => r.vch_ReturnFileType.StartsWith("Standard"));
                    if (cat == ReturnFileCategory.Multipack)
                        query = query.Where(r => r.vch_ReturnFileType.StartsWith("Multipack"));
                    else if (cat == ReturnFileCategory.SHIP)
                        query = query.Where(r => r.vch_FileName.EndsWith("_SHIP.csv"));
                    else if (cat == ReturnFileCategory.VRF)
                        query = query.Where(r => r.vch_ReturnFileType.Equals(new RF_IncommAmexVRFFile().DisplayName));
                    records = query.Select(r => new ReturnFileRecord(r)).ToList();
                }
                catch (Exception e)
                {
                    throw new ArgumentException($"Error getting return file records: {e.Message}");
                }
            }
            return records;
        }

        public ReturnFileRecord GetReturnFileRecord(string jobcode, string subjob, string key)
        {
            ReturnFileRecord rfr = null;
            using (UtilitySupportDataContext usdc = new UtilitySupportDataContext(_connStr))
            {
                try
                {
                    rfr = usdc.NewReturnFiles
                        .Where(r => r.vch_JobCode == jobcode &&
                                    r.vch_SubjobId == subjob &&
                                    r.vch_ReturnFileType.Equals(key))
                        .Select(r => new ReturnFileRecord(r))
                        .FirstOrDefault();
                }
                catch (Exception e)
                {
                    throw new ArgumentException($"Error getting return file record: {e.Message}");
                }
            }

            return rfr;
        }

        protected IQueryable<ReturnFileRecord> QueryGetReturnFile()
        {
            IQueryable<ReturnFileRecord> returnFiles = null;
            UtilitySupportDataContext usdb = new UtilitySupportDataContext(_connStr);

            returnFiles = usdb.NewReturnFiles
                   .GroupJoin(usdb.AutoPostErrors, r => r.iPK_ReturnFileID, e => e.iFK_ReturnFileID, (r, e) => new { r, e })
                   .SelectMany(y => y.e.Where(q => y.r.dt_DateLastUpdated < q.dt_DateOccurred && q.i_Status == 0).DefaultIfEmpty(), (y, z) => new ReturnFileRecord()
                   {
                       ReturnFileID = y.r.iPK_ReturnFileID,
                       Jobcode = y.r.vch_JobCode,
                       Subjob = y.r.vch_SubjobId,
                       FileName = y.r.vch_FileName,
                       FolderName = y.r.vch_SaveToLocation,
                       UploadSite = y.r.vch_UploadSite ?? string.Empty,
                       UploadURL = y.r.vch_UploadURL ?? string.Empty,
                       DateGenerated = y.r.dt_DateGenerated,
                       DateLastUpdated = y.r.dt_DateLastUpdated,
                       LastUpdatedBy = y.r.vch_LastUpdatedBy,
                       TimesGenerated = y.r.i_TimesGenerated,
                       ReturnFileTypeName = y.r.vch_ReturnFileType,
                       State = y.r.i_PostState ?? 0,
                       DatePosted = y.r.dt_DatePosted,
                       PostError = z.vch_Description ?? string.Empty
                   }).AsQueryable();

            return returnFiles;
        }

        public List<ReturnFileRecord> GetReturnFiles(List<byte> stateSelections, string jobCode, DateTime dateTime, ReturnFileCategory cat)
        {
            List<ReturnFileRecord> retFiles = new List<ReturnFileRecord>();
            try
            {
                var returnFiles = QueryGetReturnFile();

                if (!string.IsNullOrEmpty(jobCode))
                    returnFiles = returnFiles.Where(x => x.Jobcode.Contains(jobCode));

                if (stateSelections.Count > 0)
                    returnFiles = returnFiles.Where(p => stateSelections.Contains(p.State));

                if (dateTime > DateTime.MinValue)
                    returnFiles = returnFiles.Where(p => p.DateLastUpdated > dateTime);

                if (cat == ReturnFileCategory.Standard)
                    returnFiles = returnFiles.Where(r => r.ReturnFileTypeName.StartsWith("Standard"));
                if (cat == ReturnFileCategory.Multipack)
                    returnFiles = returnFiles.Where(r => r.ReturnFileTypeName.StartsWith("Multipack"));
                else if (cat == ReturnFileCategory.SHIP)
                    returnFiles = returnFiles.Where(r => r.FileName.EndsWith("_SHIP.csv"));
                else if (cat == ReturnFileCategory.VRF)
                    returnFiles = returnFiles.Where(r => r.ReturnFileTypeName.Equals(new RF_IncommAmexVRFFile().DisplayName));

                retFiles = returnFiles.Select(s => new ReturnFileRecord()
                    {
                        ReturnFileID = s.ReturnFileID,
                        Jobcode = s.Jobcode,
                        Subjob = s.Subjob,
                        FileName = s.FileName,
                        FolderName = s.FolderName,
                        UploadSite = s.UploadSite ?? string.Empty,
                        UploadURL = s.UploadURL ?? string.Empty,
                        DateGenerated = s.DateGenerated,
                        DateLastUpdated = s.DateLastUpdated,
                        LastUpdatedBy = s.LastUpdatedBy,
                        TimesGenerated = s.TimesGenerated,
                        ReturnFileTypeName = s.ReturnFileTypeName,
                        State = s.State,
                        PostState = GetState(s.State),
                        DatePosted = s.DatePosted,
                        PostError = s.PostError
                    }).ToList();
            }
            catch(Exception ex)
            {
                LogMessage($"Exception in GetReturnFiles; {jobCode}: {ex.Message}");
            }
            return retFiles;
        }

        public void CancelAutoUpload(string jobcode, string subjob, string key)
        {
            using (UtilitySupportDataContext usdc = new UtilitySupportDataContext(_connStr))
            {
                try
                {
                    NewReturnFile nrf = usdc.NewReturnFiles
                        .Where(r => r.vch_JobCode == jobcode &&
                                    r.vch_SubjobId == subjob &&
                                    r.vch_ReturnFileType.Equals(key))
                        .FirstOrDefault();
                    if (nrf != null)
                    {
                        nrf.i_PostState = (byte)PostState.Deferred;
                        nrf.dt_DateLastUpdated = DateTime.Now;
                        nrf.vch_LastUpdatedBy = Environment.UserName;
                        usdc.SubmitChanges();
                        LogMessage($"Cancelled auto upload for {jobcode} {subjob} {key}");
                    }
                }
                catch (Exception ex)
                {
                    LogMessage($"Exception in CancelAutoUpload: {ex.Message}");
                }
            }
        }

        public void UpdateReturnFileTable(string jobcode, string subjob, string key, string fileName, string fileLocation, PostState state, string uploadSite, string uploadURL)
        {
            DateTime now = DateTime.Now;
            string msg = "updated";
            using (UtilitySupportDataContext usdb = new UtilitySupportDataContext(_connStr))
            {
                try
                {
                    NewReturnFile rf = usdb.NewReturnFiles
                        .Where(r => r.vch_JobCode == jobcode && r.vch_SubjobId == subjob && r.vch_ReturnFileType == key)
                        .FirstOrDefault();
                    if (rf == null) // create a new record
                    {
                        rf = new NewReturnFile()
                        {
                            vch_JobCode = jobcode,
                            vch_SubjobId = subjob,
                            vch_ReturnFileType = key,
                            dt_DateGenerated = now,
                            i_TimesGenerated = 0,
                        };
                        usdb.NewReturnFiles.InsertOnSubmit(rf);
                        msg = "created";
                    }
                    rf.vch_FileName = fileName;
                    rf.vch_SaveToLocation = fileLocation;
                    rf.dt_DateLastUpdated = now;
                    rf.vch_LastUpdatedBy = Environment.UserName;
                    rf.i_TimesGenerated = rf.i_TimesGenerated + 1;
                    rf.i_PostState = (byte)state;
                    rf.dt_DatePosted = null;
                    rf.vch_UploadSite = uploadSite;
                    rf.vch_UploadURL = uploadURL;
                    usdb.SubmitChanges();
                    LogMessage($"Return file record {msg} for {jobcode} {subjob} type {key}");
                }
                catch(Exception ex)
                {
                    LogMessage($"Exception in UpdateReturnFileTable, {jobcode} {subjob} type {key}: {ex.Message}");
                }
            }
        }

        public string GetUploadSite(string packingUPC)
        {
            string uploadSite = string.Empty;
            using (UtilitySupportDataContext usdb = new UtilitySupportDataContext(_connStr))
            {
                try
                {
                    uploadSite = usdb.UPCWorkflowUploads.Where(u => u.PackagingUPC == packingUPC).Select(u => u.UploadSite).FirstOrDefault() ?? string.Empty;
                }
                catch
                {
                    uploadSite = string.Empty;
                }
            }
            return uploadSite;
        }

        public List<string> GetWorkflowNames()
        {
            List<string> workflowNames = new List<string>();
            using (UtilitySupportDataContext usdb = new UtilitySupportDataContext(_connStr))
            {
                try
                {
                    workflowNames = usdb.UPCWorkflowUploads.Where(u => u.UploadSite.Length > 0).Select(u => u.WorkflowName).Distinct().OrderBy(s => s).ToList();
                }
                catch (Exception ex)
                {
                    LogMessage($"Exception in GetWorkflowNames: {ex.Message}");
                }
            }
            return workflowNames;
        }

        public string AddWorkflow(string packingUPC, string workflowName, string uploadSite)
        {
            string res = string.Empty;
            using (UtilitySupportDataContext usdb = new UtilitySupportDataContext(_connStr))
            {
                try
                {
                    if (usdb.UPCWorkflowUploads.Where(u => u.PackagingUPC == packingUPC).FirstOrDefault() != null)
                        res = $"Packaging UPC {packingUPC} already exists in the table";
                    else
                    {
                        UPCWorkflowUpload newWorkflow = new UPCWorkflowUpload()
                        {
                            PackagingUPC = packingUPC,
                            WorkflowName = workflowName,
                            UploadSite = uploadSite
                        };
                        usdb.UPCWorkflowUploads.InsertOnSubmit(newWorkflow);
                        usdb.SubmitChanges();
                    }
                }
                catch (Exception e)
                {
                    res = $"Exception while inserting new workflow: {e.Message}";
                }
            }
            return res;
        }

        public Dictionary<string, ReturnFileURLInfo> GetReturnFileURLs()
        {
            Dictionary<string, ReturnFileURLInfo> rfURLs = new Dictionary<string, ReturnFileURLInfo>();
            using (UtilitySupportDataContext usdb = new UtilitySupportDataContext(_connStr))
            {
                try
                {
                    rfURLs = usdb.ReturnFileURLs
                        .Join(usdb.FTPSites, r => r.vch_UploadURL, f => f.vch_URL, (r, f) => new { r, f.vch_SiteName })
                        .Select(x => new { x.r.vch_ReturnFileType, x })
                        .ToDictionary(y => y.vch_ReturnFileType, y => new ReturnFileURLInfo()
                        {
                            DisplayName = y.x.r.vch_ReturnFileType,
                            UploadURL = y.x.r.vch_UploadURL,
                            UploadFolder = y.x.r.vch_UploadFolder,
                            UploadServerName = y.x.vch_SiteName
                        });
                }
                catch (Exception ex)
                {
                    throw new Exception($"Exception in GetReturnFileURLs: {ex.Message}");
                }
            }
            return rfURLs;
        }

        public static PostState GetState(byte? state)
        {
            if (state == null)
                return PostState.Error;
            else if (state == 0)
                return PostState.Pending;
            else if (state == 1)
                return PostState.Posted;
            else if (state == 2)
                return PostState.Deferred;
            else if (state == 3)
                return PostState.Error;
            else if (state == 4)
                return PostState.NoAutoPost;
            else
                return PostState.Error;
        }

        public List<UnresolvedError> GetUnresolvedErrors()
        {
            List<UnresolvedError> unresolved = new List<UnresolvedError>();
            using (UtilitySupportDataContext usdb = new UtilitySupportDataContext(_connStr))
            {
                try
                {
                    unresolved = usdb.AutoPostErrors
                        .Join(usdb.NewReturnFiles, e => e.iFK_ReturnFileID, f => f.iPK_ReturnFileID, (e, f) => new { e, f })
                        .Where(x => x.e.i_Status == 0)
                        .Select(x => new UnresolvedError()
                        {
                            ID = x.e.iPK_AutoPosterErrors,
                            JobCode = x.e.vch_JobCode,
                            SubjobIdentifier = x.e.vch_SubjobID,
                            ReturnFileType = x.e.vch_ReturnFileType,
                            DateOccurred = x.e.dt_DateOccurred,
                            Description = x.e.vch_Description,
                            RFRec = new ReturnFileRecord(x.f)
                        })
                        .ToList();
                }
                catch (Exception ex)
                {
                    throw new Exception($"Exception in GetUnresolvedErrors: {ex.Message}");
                }
            }
            return unresolved;
        }

        public void ClearError(UnresolvedError urErr, PostState state)
        {
            DateTime now = DateTime.Now;
            using (UtilitySupportDataContext usdb = new UtilitySupportDataContext(_connStr))
            {
                try
                {
                    AutoPostError apErr = usdb.AutoPostErrors.Where(e => e.iPK_AutoPosterErrors == urErr.ID).FirstOrDefault();
                    if (apErr != null)
                        apErr.i_Status = 1;

                    NewReturnFile rf = null;
                    if (urErr.RFRec.PostState != state) // state change needed in the NewReturnFile record
                    {
                        rf = usdb.NewReturnFiles
                            .Where(r => r.vch_JobCode.Equals(urErr.RFRec.Jobcode) &&
                                        r.vch_SubjobId.Equals(urErr.RFRec.Subjob) &&
                                        r.vch_ReturnFileType.Equals(urErr.RFRec.ReturnFileTypeName))
                            .FirstOrDefault();
                        if (rf != null)
                        {
                            rf.dt_DateLastUpdated = now;
                            rf.vch_LastUpdatedBy = Environment.UserName;
                            rf.i_PostState = (byte)state;
                        }
                    }
                    usdb.SubmitChanges();
                    DBHelper.LogMessage($"Cleared error {urErr.ID} for return file record {urErr.RFRec.ToString()}");
                }
                catch (Exception ex)
                {
                    throw new Exception($"Exception in ClearError: {ex.Message}");
                }
            }
        }
    }

    public enum PostState
    {
        Pending = 0,
        Posted = 1,
        Deferred = 2,
        Error = 3,
        NoAutoPost = 4,
        Unknown = 5
    }

    public class ReturnFileRecord
    {
        public int ReturnFileID { get; set; } = 0;
        public string Jobcode { get; set; } = string.Empty;
        public string Subjob { get; set; } = string.Empty;
        public string FileName { get; set; } = string.Empty;
        public string FolderName { get; set; } = string.Empty;
        public string UploadSite { get; set; } = string.Empty;
        public string UploadURL { get; set; } = string.Empty;
        public DateTime DateGenerated { get; set; } = DateTime.MinValue;
        public DateTime DateLastUpdated { get; set; } = DateTime.MinValue;
        public string LastUpdatedBy { get; set; } = string.Empty;
        public int TimesGenerated { get; set; } = 0;
        public string ReturnFileTypeName { get; set; } = string.Empty;
        public PostState PostState { get; set; } = PostState.NoAutoPost;
        public string PostStateStr
        {
            get { return PostState == PostState.NoAutoPost ? "No autopost" : Enum.GetName(typeof(PostState), PostState); }
        }
        public byte State { get; set; } = (byte)PostState.NoAutoPost;
        public string PostError { get; set; } = string.Empty;
        public DateTime? DatePosted { get; set; } = DateTime.MinValue;
        public string SourceFile
        {
            get { return FileName.EndsWith("_SHIP.csv") ? ReturnFileTypeName : string.Empty; }
        }
        public ReturnFileRecord()
        {

        }

        public ReturnFileRecord(NewReturnFile r)
        {
            ReturnFileID = r.iPK_ReturnFileID;
            Jobcode = r.vch_JobCode;
            Subjob = r.vch_SubjobId;
            FileName = r.vch_FileName;
            FolderName = r.vch_SaveToLocation;
            UploadSite = r.vch_UploadSite ?? string.Empty;
            UploadURL = r.vch_UploadURL ?? string.Empty;
            DateGenerated = r.dt_DateGenerated;
            DateLastUpdated = r.dt_DateLastUpdated;
            LastUpdatedBy = r.vch_LastUpdatedBy;
            TimesGenerated = r.i_TimesGenerated;
            ReturnFileTypeName = r.vch_ReturnFileType;
            PostState = UtilitySupportController.GetState(r.i_PostState);
            DatePosted = r.dt_DatePosted;
        }

        public override string ToString()
        {
            return $"{Jobcode}_{Subjob} {ReturnFileTypeName}";
        }
    }

    public class ReturnFileURLInfo
    {
        public string DisplayName { get; set; } = string.Empty;
        public string UploadURL { get; set; } = string.Empty;
        public string UploadFolder { get; set; } = string.Empty;
        public string UploadServerName { get; set; } = string.Empty;
    }
}
