﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.IO;
using System.Numerics;
using CsvHelper;
using PrepaidCommon;
using System.Windows.Interop;
using System.Data.SqlClient;
using System.Data;

namespace CNGenUtil
{
    public class RF_MPFile : RF_FFBase
    {
        public override string DisplayName { get => "MP File"; }
        protected override string SprocName { get => "util_MPFile_General"; }
        public override bool IsMultipack { get => true; }

        private int Denomination { get; set; } = 0;
        private int MPCount { get; set; } = 0;
        private int TotalDenomination { get => Denomination * MPCount; }

        public MPFileNameViewModel MPFileNameArgs { get; set; } = new MPFileNameViewModel();

        /// <summary>
        /// ReturnFileKind.MPFile, "util_MPFile_General", false, false
        /// </summary>
        public RF_MPFile() : base()
        {
        }

        public override bool Initialize()
        {
            bool ret = base.Initialize();
            if (ret)
            {
                string errMsg = string.Empty;
                try
                {
                    CDMSDataController cdc = new CDMSDataController();
                    string val = cdc.GetProductDataValue(SelectedJobCode, SelectedSubjob, "Denomination");
                    if (Int32.TryParse(val, out int denom))
                    {
                        Denomination = denom;
                    }
                    else
                    {
                        errMsg = $"Invalid denomination: {val}";
                    }

                    MPCount = cdc.GetMPFileCount(SelectedJobCode, SelectedSubjob);
                    List<CDMSSubjob> subjobs = cdc.GetSubjobsWithTables(SelectedJobCode, SelectedSubjob);
                    CDMSSubjob carrier = subjobs.Where(s => s.IsCarrier).FirstOrDefault();
                    if (MPCount != 3 && MPCount != 5)
                    {
                        errMsg = $"MP count must be either 3 or 5";
                    }
                    else if (carrier == null)
                    {
                        errMsg = $"Unable to find carrier subjob for {SelectedJobCode}{SelectedSubjob}. MP File requires a carrier subjob.";
                    }
                }
                catch (Exception ex)
                {
                    errMsg = $"Exception occurred while getting initializing for MP file: {ex.Message}";
                }

                if (!string.IsNullOrWhiteSpace(errMsg))
                {
                    MessageBox.Show($"{errMsg}", "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                    ret = false;
                }
            }

            return ret;
        }

        protected override string CreateFileName()
        {
            DateParts dp = new DateParts();

            return $"MPFile_CPI_InComm_{MPFileNameArgs.Merchant}_{_ffid}_{dp.Year}{dp.Month}{dp.Day}{dp.Hour}{dp.Minute}{dp.Second}_{MPFileNameArgs.Suffix}.txt";
        }

        protected override void WriteFile(StreamWriter wtr, SqlDataReader rdr)
        {
            if (MPCount == 3)
            {
                wtr.WriteLine("Wrap Serial Number,Wrap Van 16,Wrap Denom,Number of Cards in Wrap,Reference #,Control Number,Account Number," +
                               "Track 1 Data,Track 2 Data / Card Activation Data,Denomination,Card Type,Reference #,Control Number,Account Number," +
                               "Track 1 Data,Track 2 Data / Card Activation Data,Denomination,Card Type,Reference #,Control Number,Account Number," +
                               "Track 1 Data,Track 2 Data / Card Activation Data,Denomination,Card Type");
            }
            else
            {
                wtr.WriteLine("Wrap Serial Number,Wrap Van 16,Wrap Denom,Number of Cards in Wrap,Reference #,Control Number,Account Number," +
                                 "Track 1 Data,Track 2 Data / Card Activation Data,Denomination,Card Type,Reference #,Control Number,Account Number," +
                                 "Track 1 Data,Track 2 Data / Card Activation Data,Denomination,Card Type,Reference #,Control Number,Account Number," +
                                 "Track 1 Data,Track 2 Data / Card Activation Data,Denomination,Card Type,Reference #,Control Number,Account Number," +
                                 "Track 1 Data,Track 2 Data / Card Activation Data,Denomination,Card Type,Reference #,Control Number,Account Number," +
                                 "Track 1 Data,Track 2 Data / Card Activation Data,Denomination,Card Type");
            }
            WriteFileBody(wtr, rdr);
        }

        protected override LineWriter GetLineWriter()
        {
            return (w, rdr) =>
            {
                string cardType = MPFileNameArgs.IsTypeOne ? "1" : "2";
                if (MPCount == 3)
                {
                    string format = MPFileNameArgs.IsTypeOne ? "{0},{1},{2},{3},{4},{5},{6},,{7},{8},{9},{10},{11},{12},,{13},{14},{15},{16},{17},{18},,{19},{20},{21}"
                                                                            : "{0},{1},{2},{3},{4},{5},{6},{7},,{8},{9},{10},{11},{12},{13},,{14},{15},{16},{17},{18},{19},,{20},{21}";
                    w.WriteLine(format,
                            rdr.GetString(0), rdr.GetString(1), (TotalDenomination.ToString("D3") + "00"), MPCount.ToString("00"),
                            _ffid, rdr.GetString(2), rdr.GetString(3), rdr.GetString(3), (Denomination.ToString("D3") + "00"), cardType,
                            _ffid, rdr.GetString(4), rdr.GetString(5), rdr.GetString(5), (Denomination.ToString("D3") + "00"), cardType,
                            _ffid, rdr.GetString(6), rdr.GetString(7), rdr.GetString(7), (Denomination.ToString("D3") + "00"), cardType);
                }
                else
                {
                    string format = MPFileNameArgs.IsTypeOne ? "{0},{1},{2},{3},{4},{5},{6},,{7},{8},{9},{10},{11},{12},,{13},{14},{15}," +
                                                                              "{16},{17},{18},,{19},{20},{21},{22},{23},{24},,{25},{26},{27},{28},{29},{30},,{31},{32},{33}"
                                                                            : "{0},{1},{2},{3},{4},{5},{6},{7},,{8},{9},{10},{11},{12},{13},,{14},{15}," +
                                                                              "{16},{17},{18},{19},,{20},{21},{22},{23},{24},{25},,{26},{27},{28},{29},{30},{31},,{32},{33}";
                    w.WriteLine(format,
                            rdr.GetString(0), rdr.GetString(1), (TotalDenomination.ToString("D3") + "00"), MPCount.ToString("00"),
                            _ffid, rdr.GetString(2), rdr.GetString(3), rdr.GetString(3), (Denomination.ToString("D3") + "00"), cardType,
                            _ffid, rdr.GetString(4), rdr.GetString(5), rdr.GetString(5), (Denomination.ToString("D3") + "00"), cardType,
                            _ffid, rdr.GetString(6), rdr.GetString(7), rdr.GetString(7), (Denomination.ToString("D3") + "00"), cardType,
                            _ffid, rdr.GetString(8), rdr.GetString(9), rdr.GetString(9), (Denomination.ToString("D3") + "00"), cardType,
                            _ffid, rdr.GetString(10), rdr.GetString(11), rdr.GetString(11), (Denomination.ToString("D3") + "00"), cardType);
                }
            };
        }

        public override int ValidateFile()
        {
            ErrorLogC errorLog = new ErrorLogC();
            int totalErrors = 0;
            int lineNum = 0;
            int expectedLineLength = 0;
            int actualRecords = 0;

            using (StreamReader rdr = new StreamReader(ReturnFilePath))
            {
                while (rdr.Peek() > 0)
                {
                    int lineLength = rdr.ReadLine().Length;
                    lineNum++;
                    if (lineNum == 2)
                    {
                        expectedLineLength = lineLength; // first data line is used to set expectedLineLength
                    }
                    else if (lineNum > 2 && lineLength != expectedLineLength)
                    {
                        errorLog.Add(typeof(LineLengthError), lineNum, lineLength.ToString());
                        totalErrors++;
                    }
                }

                actualRecords = lineNum - 1;
                if (actualRecords != _expectedFileCountFromJob)
                {
                    totalErrors++;
                }
            }

            using (TextReader fileReader = File.OpenText(ReturnFilePath))
            {
                if (fileReader.Peek() > 0)
                {
                    fileReader.ReadLine(); // header
                    lineNum = 1;
                }
                var csvFile = new CsvReader(fileReader);
                BigInteger lastControlVal = 0;
                BigInteger currentControlVal = 0;

                while (csvFile.Read())
                {
                    lineNum++;

                    string wrapserial = GetField(csvFile, 0, string.Empty);
                    if (wrapserial.Length < 1)
                    {
                        errorLog.Add(typeof(MissingFieldError), lineNum, "Wrap serial is missing");
                        totalErrors++;
                    }

                    string wrapVan = GetField(csvFile, 1, string.Empty);
                    if (wrapVan.Length < 1)
                    {
                        errorLog.Add(typeof(MissingFieldError), lineNum, "Wrap van16 is missing");
                        totalErrors++;
                    }

                    string referenceNumber = GetField(csvFile, 4, string.Empty);
                    if (referenceNumber.Length < 1)
                    {
                        errorLog.Add(typeof(MissingFieldError), lineNum, "FFID is missing");
                        totalErrors++;
                    }

                    string cardASerial = GetField(csvFile, 5, string.Empty);
                    if (cardASerial.Length < 1)
                    {
                        errorLog.Add(typeof(CardSerialError), lineNum, "Card A serial is missing");
                        totalErrors++;
                    }

                    string cardBSerial = GetField(csvFile, 12, string.Empty);
                    if (cardBSerial.Length < 1)
                    {
                        errorLog.Add(typeof(CardSerialError), lineNum, "Card B serial is missing");
                        totalErrors++;
                    }

                    string cardCSerial = GetField(csvFile, 19, string.Empty);
                    if (cardCSerial.Length < 1)
                    {
                        errorLog.Add(typeof(CardSerialError), lineNum, "Card C serial is missing");
                        totalErrors++;
                    }

                    string numberOfCards = GetField(csvFile, 3, string.Empty);
                    if (numberOfCards.Equals("05"))
                    {
                        string cardDSerial = GetField(csvFile, 26, string.Empty);
                        if (cardDSerial.Length < 1)
                        {
                            errorLog.Add(typeof(CardSerialError), lineNum, "Card D serial is missing");
                            totalErrors++;
                        }

                        string cardESerial = GetField(csvFile, 33, string.Empty);
                        if (cardESerial.Length < 1)
                        {
                            errorLog.Add(typeof(CardSerialError), lineNum, "Card E serial is missing");
                            totalErrors++;
                        }
                    }

                    //After the first line, it will grab the serial number and assign to tempSerial.
                    BigInteger.TryParse(wrapserial, out currentControlVal);
                    if (lastControlVal != 0)
                    {
                        if (currentControlVal != (lastControlVal + 1))
                        {
                            errorLog.Add(typeof(ControlNumberError), lineNum, currentControlVal.ToString());
                            totalErrors++;
                        }
                    }
                    lastControlVal = currentControlVal;
                }
            }

            Dictionary<Type, string> errorHeaderParams = new Dictionary<Type, string>();
            errorHeaderParams.Add(typeof(LineLengthError), expectedLineLength.ToString());

            WriteValidationFile(errorHeaderParams, actualRecords, totalErrors, errorLog);

            return totalErrors;
        }
    }
}
