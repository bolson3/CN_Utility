﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PrepaidCommon;
using System.Globalization;
using System.Numerics;
using System.IO;
using CsvHelper;
using System.Windows.Data;

namespace CNGenUtil
{
    public class RF_NetspendReceiptFile : RF_Base
    {
        public override string DisplayName { get; set; } = "Netspend Receipt File";
        protected override string SprocName { get => "util_NSReceiptFile"; }


        public RF_NetspendReceiptFile() : base()
        {
        }

        protected override string CreateFileName()
        {
            DateParts dp = new DateParts();

            DateTimeFormatInfo currentDate = new DateTimeFormatInfo();
            var longMonth = currentDate.GetAbbreviatedMonthName(DateTime.Now.Month);

            return $"CPIreceipts{dp.Month}-{longMonth}-{dp.Year}{dp.Hour}{dp.Minute}{dp.Second}.csv";
        }

        public override int ValidateFile()
        {
            ErrorLogC errorLog = new ErrorLogC();
            int totalErrors = 0;
            int lineNum = 0;

            // declare fields we'll track across lines
            string expectedNSNumber = string.Empty;
            string expectedQuantity = string.Empty;
            string expectedDateGen = string.Empty;
            string expectedGenerator = string.Empty;
            string expectedExpiry = string.Empty;            
            string lastControlNumber = string.Empty;
            
            using (TextReader fileReader = File.OpenText(ReturnFilePath))
            {
                var csv = new CsvReader(fileReader);
                while (csv.Read())
                {
                    lineNum++;
                    if (lineNum == 1) // first Row to set expected
                    {
                        expectedNSNumber = csv.GetField<string>(0);
                        expectedQuantity = csv.GetField<string>(1);
                        lastControlNumber = csv.GetField<string>(2);
                        expectedDateGen = csv.GetField<string>(3);
                        expectedGenerator = csv.GetField<string>(4);
                        expectedExpiry = csv.GetField<string>(7);
                    }
                    else if (lineNum > 1)
                    {
                        var currentNsNumber = csv.GetField<string>(0);
                        if (currentNsNumber != expectedNSNumber)
                        {
                            errorLog.Add(typeof(NSReceiptNSNumberError), lineNum, expectedNSNumber);
                            totalErrors++;
                        }

                        var currentQuantity = csv.GetField<string>(1);
                        if (currentQuantity != expectedQuantity)
                        {
                            errorLog.Add(typeof(NSReceiptQuantityError), lineNum, expectedQuantity);
                            totalErrors++;
                        }

                        var currentControlNumber = csv.GetField<string>(2);
                        if (currentControlNumber != (lastControlNumber + 1))
                        {
                            errorLog.Add(typeof(NSControlNumberError), lineNum, lastControlNumber);
                            totalErrors++;
                        }
                        lastControlNumber = currentControlNumber;

                        var currentDateGen = csv.GetField<string>(3);
                        if (currentDateGen != expectedDateGen)
                        {
                            errorLog.Add(typeof(NSDateGenError), lineNum, expectedDateGen);
                            totalErrors++;
                        }

                        var currentGenerator = csv.GetField<string>(5);
                        if (currentGenerator != expectedGenerator)
                        {
                            errorLog.Add(typeof(NSGeneratorError), lineNum, expectedGenerator);
                            totalErrors++;
                        }

                        var currentExpiry = csv.GetField<string>(7);
                        if (currentExpiry != expectedExpiry)
                        {
                            errorLog.Add(typeof(NSExpiryError), lineNum, expectedExpiry);
                            totalErrors++;
                        }
                    }
                }
            }

            Dictionary<Type, string> errorHeaderParams = new Dictionary<Type, string>();
            errorHeaderParams.Add(typeof(NSReceiptNSNumberError), expectedNSNumber);
            errorHeaderParams.Add(typeof(NSReceiptQuantityError), expectedQuantity);
            errorHeaderParams.Add(typeof(NSDateGenError), expectedDateGen);
            errorHeaderParams.Add(typeof(NSGeneratorError), expectedGenerator);
            errorHeaderParams.Add(typeof(NSExpiryError), expectedExpiry);

            WriteValidationFile(errorHeaderParams, lineNum, totalErrors, errorLog);

            return totalErrors;
        }
    }
}
