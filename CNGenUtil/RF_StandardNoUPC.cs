﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using PrepaidCommon;
using System.Windows;
using System.Data.SqlClient;
using System.Data;

namespace CNGenUtil
{
    public class RF_StandardNoUPC : RF_Standard
    {       
        public override string DisplayName { get; set; } = "Standard No UPC";
        protected override string SprocName { get => "util_FullCnGen2"; }

        public RF_StandardNoUPC(): base()
        {
        }
    }
}
