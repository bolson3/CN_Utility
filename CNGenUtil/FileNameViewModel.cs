﻿using PrepaidCommon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CNGenUtil
{
    public class FileNameViewModel : ViewModelBase
    {
		private bool _isProd;

		public bool IsProd
		{
			get => _isProd;
			set
			{
				_isProd = value;
				OnPropertyChanged(nameof(IsProd));
			}
		}

		public string Suffix { get => IsProd ? "PROD" : "TEST"; }

		public FileNameViewModel()
        {
			IsProd = true;
        }
	}
}
