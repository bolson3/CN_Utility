﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.IO;
using PrepaidCommon;
using System.Data.SqlClient;
using System.Data;
using System.Numerics;
using CsvHelper;

namespace CNGenUtil
{
    public class RF_RFile : RF_FFBase
    {
        public override string DisplayName { get; set; } = "R File";
        protected override string SprocName { get => "util_RFile"; }

        public RFileNameViewModel RFileNameArgs { get; set; } = new RFileNameViewModel();

        public RF_RFile(): base()
        {
        }

        protected override string CreateFileName()
        {
            DateParts dp = new DateParts();

            //this is naming convention if Netspend
            return $"RFILE_CPI_{RFileNameArgs.Processor}_INCOMM_{_ffid}_{dp.Year}{dp.Month}{dp.Day}{dp.Hour}{dp.Minute}{dp.Second}" + 
                   $"_{SelectedJobCode}{SelectedSubjob}_{RFileNameArgs.Suffix}.txt";
        }

        protected override void WriteFile(StreamWriter wtr, SqlDataReader rdr)
        {
            wtr.WriteLine("JOB ID,SERIAL,VAN,ACCT#,CONTROL #");
            WriteFileBody(wtr, rdr);
        }

        protected override LineWriter GetLineWriter()
        {
            return (w, rdr) =>
            {
                w.WriteLine("{0},{1},{2},{3},{4}", _ffid, rdr.GetString(0), rdr.GetString(1), rdr.GetString(2), rdr.GetString(3));
            };
        }

        public override int ValidateFile()
        {
            ErrorLogC errorLog = new ErrorLogC();
            int totalErrors = 0;
			int lineNum = 0;
			int expectedLineLength = 0;
			int actualRecords = 0;

			using (StreamReader rdr = new StreamReader(ReturnFilePath))
			{
				while (rdr.Peek() > 0)
				{
					int lineLength = rdr.ReadLine().Length;
					lineNum++;
					if (lineNum == 2)
                    {
						expectedLineLength = lineLength; // first data line is used to set expectedLineLength
                    }
					else if (lineNum > 2 && lineLength != expectedLineLength)
					{
						errorLog.Add(typeof(LineLengthError), lineNum, lineLength.ToString());
						totalErrors++;
					}
				}

				actualRecords = lineNum - 1;
				if (actualRecords != _expectedFileCountFromJob)
				{
					totalErrors++;
				}
			}

			using (TextReader fileReader = File.OpenText(ReturnFilePath))
			{
				if (fileReader.Peek() > 0)
				{
					fileReader.ReadLine(); // header
					lineNum = 1;
				}
				var csvFile = new CsvReader(fileReader);
				BigInteger lastControlVal = 0;
				BigInteger currentControlVal = 0;

				while (csvFile.Read())
				{
					lineNum++;

					string jobID = GetField(csvFile, 0, string.Empty);
					if (jobID.Length < 1)
					{
						errorLog.Add(typeof(MissingFieldError), lineNum, "Job ID is missing");
						totalErrors++;
					}

					string serialNum = GetField(csvFile, 1, string.Empty);
					if (serialNum.Length < 1)
					{
						errorLog.Add(typeof(MissingFieldError), lineNum, "Serial number is missing");
						totalErrors++;
					}

					string van16 = GetField(csvFile, 2, string.Empty);
					if (van16.Length < 1)
					{
						errorLog.Add(typeof(MissingFieldError), lineNum, "Van 16 is missing");
						totalErrors++;
					}

					string account = GetField(csvFile, 3, string.Empty);
					if (account.Length < 1)
					{
						errorLog.Add(typeof(MissingFieldError), lineNum, "Account number is missing");
						totalErrors++;
					}

					string control = GetField(csvFile, 4, string.Empty);
					if (control.Length < 1)
					{
						errorLog.Add(typeof(MissingFieldError), lineNum, "Control number is missing");
						totalErrors++;
					}

					BigInteger.TryParse(control, out currentControlVal);
					if (lastControlVal != 0)
					{
						if (currentControlVal != (lastControlVal + 1))
						{
							errorLog.Add(typeof(SeqNumError), lineNum, currentControlVal.ToString());
							totalErrors++;
						}
					}
					lastControlVal = currentControlVal;
				}
			}

			Dictionary<Type, string> errorHeaderParams = new Dictionary<Type, string>();
			errorHeaderParams.Add(typeof(LineLengthError), expectedLineLength.ToString());

			WriteValidationFile(errorHeaderParams, actualRecords, totalErrors, errorLog);

            return totalErrors;
        }
    }
}
