﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace CNGenUtil
{
    public class RF_FFBase : RF_Base
    {
        protected string _ffid = string.Empty;

        public override bool ShowFileNamePreview { get => true; }

        public RF_FFBase() : base()
        {
        }

        public override bool Initialize()
        {
            bool ret = base.Initialize();
            if (ret)
            {
                string errMsg = string.Empty;
                try
                {
                    CDMSDataController cdc = new CDMSDataController();
                    _ffid = cdc.GetDatafieldValue(SelectedJobCode, SelectedSubjob, "FFID");
                    if (string.IsNullOrWhiteSpace(_ffid))
                    {
                        errMsg = $"FFID not found";
                    }
                }
                catch (Exception ex)
                {
                    errMsg = $"Exception while getting data field value FFID: {ex.Message}";
                }

                if (!string.IsNullOrEmpty(errMsg))
                {
                    MessageBox.Show($"{errMsg}", "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                    ret = false;
                }
            }
            return ret;
        }

        protected override int GetCardCount()
        {
            return GetCount(true);
        }
    }
}
