﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PrepaidCommon;
using System.IO;
using CsvHelper;
using System.Numerics;

namespace CNGenUtil
{
    public class RF_BHNSimonNoBundleEASN : RF_BHNBase
    {
        public override string DisplayName { get; set; } = "BHN Simon No Bundle EASN";
        protected override string SprocName { get => "util_SimonNoBundleBHNEASN"; }

        public RF_BHNSimonNoBundleEASN(): base()
        {
        }

        public override int ValidateFile()
        {
            ErrorLogC errorLog = new ErrorLogC();
            int totalErrors = 0;
            int expectedLineLength = 0;
            int lineNum = 0;

            // check line length and line count
            using (StreamReader rdr = new StreamReader(ReturnFilePath))
            {
                while (rdr.Peek() > 0)
                {
                    var lineLength = rdr.ReadLine().Length;
                    lineNum++;
                    if (lineNum == 1)
                    {
                        expectedLineLength = lineLength;
                    }
                    else if (lineLength != expectedLineLength)
                    {
                        errorLog.Add(typeof(LineLengthError), lineNum, lineLength.ToString());
                        totalErrors++;
                    }
                }

                if (lineNum != _expectedFileCountFromJob)
                {
                    totalErrors++;
                }
            }

            // declare fields we'll track to make sure they're the same in all lines
            string expectedSender = string.Empty;
            string expectedGenDate = string.Empty;
            string expectedIID = string.Empty;
            string expectedExpiry = string.Empty;
            int expectedProxyLength = 0;
            string expectedCarton = string.Empty;

            // process file using CsvReader to extract fields.
            using (TextReader fileReader = File.OpenText(ReturnFilePath))
            {
                var csv = new CsvReader(fileReader);
                lineNum = 0;
                BigInteger lastControlVal = 0;
                BigInteger currentControlVal = 0;
 
                while (csv.Read())
                {
                    lineNum++;
                    if (lineNum == 1)
                    {
                        expectedSender = csv.GetField<string>(0);
                        expectedGenDate = csv.GetField<string>(1);
                        expectedIID = csv.GetField<string>(7);
                        expectedExpiry = csv.GetField<string>(9);
                        expectedProxyLength = csv.GetField<string>(5).Length;
                    }

                    var sender = csv.GetField<string>(0);
                    if (sender != expectedSender)
                    {
                        errorLog.Add(typeof(SenderIDError), lineNum, sender);
                        totalErrors++;
                    }

                    var genDate = csv.GetField<string>(1);
                    if (genDate != expectedGenDate)
                    {
                        errorLog.Add(typeof(FileDateError), lineNum, genDate);
                        totalErrors++;
                    }

                    var IID = csv.GetField<string>(7);
                    if (IID != expectedIID)
                    {
                        errorLog.Add(typeof(IIDError), lineNum, IID);
                        totalErrors++;
                    }

                    expectedCarton = csv.GetField<string>(3); // this is Carton barcode
                    var cartonCheck = csv.GetField<string>(4); // this is Bundle barcode... weird....
                    if (cartonCheck != expectedCarton)
                    {
                        errorLog.Add(typeof(CartonError), lineNum, cartonCheck);
                        totalErrors++;
                    }

                    var expiry = csv.GetField<string>(9);
                    if (expiry != expectedExpiry || expiry.Length != 6)
                    {
                        errorLog.Add(typeof(ExpiryError), lineNum, expiry);
                        totalErrors++;
                    }

                    var proxy = csv.GetField<string>(5);
                    var proxyLength = proxy.Length;
                    if (proxyLength != expectedProxyLength)
                    {
                        errorLog.Add(typeof(ProxyError), lineNum, proxy);
                        totalErrors++;
                    }

                    var control = csv.GetField<string>(6);
                    BigInteger.TryParse(control, out currentControlVal);
                    if (lastControlVal != 0)
                    {
                        if (currentControlVal != (lastControlVal + 1))
                        {
                            errorLog.Add(typeof(ControlNumberError), lineNum, currentControlVal.ToString());
                            totalErrors++;
                        }
                    }
                    lastControlVal = currentControlVal;
                }

            }

            // add error log headers as needed
            Dictionary<Type, string> errorHeaderParams = new Dictionary<Type, string>();
            errorHeaderParams.Add(typeof(LineLengthError), expectedLineLength.ToString());
            errorHeaderParams.Add(typeof(SenderIDError), expectedSender);
            errorHeaderParams.Add(typeof(FileDateError), expectedGenDate);
            errorHeaderParams.Add(typeof(IIDError), expectedIID);
            errorHeaderParams.Add(typeof(CartonError), expectedCarton);
            errorHeaderParams.Add(typeof(ExpiryError), expectedExpiry);
            errorHeaderParams.Add(typeof(ProxyError), expectedProxyLength.ToString());

            // write the file
            WriteValidationFile(errorHeaderParams, lineNum, totalErrors, errorLog);

            return totalErrors;
        }
    }
}
