﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using PrepaidCommon;

namespace CNGenUtil
{
    public class ProductionDBController
    {
        private readonly string _connStr;

        private string _prodDB = string.Empty;

        public ProductionDBController(string prodDBname)
        {
            _connStr = AppHelper.ConnectionString.Replace("Catalog=CDMS", $"Catalog={prodDBname}");
            _prodDB = prodDBname;
        }

        public void GetMinControlNumbers(List<CDMSSubjob> subjobs)
        {
            using (SqlConnection conn = new SqlConnection(_connStr))
            {
                conn.Open();
                foreach (CDMSSubjob sj in subjobs)
                {
                    string query = $"SELECT TOP (1) vch_CardControlNumber FROM {sj.CardTable}";
                    using (SqlCommand cmd = new SqlCommand(query, conn))
                    {
                        string controlNumber = DBHelper.ExecuteScalarSafe(cmd, "0");
                        if (ulong.TryParse(controlNumber, out ulong result))
                            sj.MinControlNumber = result;
                    }
                }
            }
        }

        public int GetCountForCard(string jobcode, int cardID, bool all)
        {
            int count = 0;
            using (SqlConnection con = new SqlConnection(AppHelper.ConnectionString))
            {
                con.Open();

                SqlCommand cmd = new SqlCommand("Util_GetProductionCountByCardID", con)
                {
                    CommandTimeout = 600,
                    CommandType = CommandType.StoredProcedure
                };
                cmd.Parameters.AddWithValue("@JobCode", SqlDbType.VarChar).Value = jobcode;
                cmd.Parameters.AddWithValue("@CardID", SqlDbType.Int).Value = cardID;
                cmd.Parameters.AddWithValue("@All", SqlDbType.Int).Value = all ? 1 : 0;

                count = DBHelper.ExecuteScalarSafe(cmd, 0);

                con.Close();
            }

            return count;
        }

    }
}
