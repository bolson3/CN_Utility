USE [CDMS]
GO

/****** Object:  StoredProcedure [dbo].[util_NSReceiptFile]    Script Date: 4/11/2019 8:37:05 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE PROCEDURE [dbo].[util_NSReceiptFile]
	
			@JobCode			NVarchar(10),
			@SubjobIdentifier	nVarchar(10)
			
AS
BEGIN

DECLARE @JobID int,@NSNumber nVarchar(50), @CardID int, @ProductID int, @SQL varchar (MAX), @CarrierID int,@Date varchar (25)
              , @db varchar (100), @Expiry varchar (25)
   


SET @Date = FORMAT(GetDate(),'dd-MMM-yy')



SELECT @JobID = (SELECT iPK_JobID FROM Jobs Where vch_JobCode = @JobCode) 

SELECT @CarrierID = (SELECT iPK_SUbjobID FROM SUbjobs WHERE iFK_JobID = @JobID 
AND vch_SubJobIdentifier = @SubjobIdentifier AND vch_Name LIKE '%(Carrier)%')
SELECT @CardID = (SELECT iPK_SUbjobID FROM SUbjobs WHERE iFK_JobID = @JobID AND 
vch_SubJobIdentifier = @SubjobIdentifier AND vch_Name NOT LIKE '%(Carrier)%' AND vch_CardTable IS NOT NULL)



SET @ProductID = (SELECT ifk_ProductID FROM SUbjobs WHERE iPK_SubJobID = @CarrierID)


SET @NSNumber = (SELECT vch_CustomerPRoductID FROM Products WHERE iPK_ProductID = @ProductID)
SET @Expiry = (SELECT vch_DataFieldValue FROM SubjobData WHERE iFK_SubJobID = @CarrierID AND iFK_DataFieldID = 6)

IF (@Expiry IS NULL)
BEGIN
SET @Expiry = (SELECT vch_DataFieldValue FROM SubjobData WHERE iFK_SubJobID = @CardID AND iFK_DataFieldID = 6)
END


SELECT @DB = (SELECT vch_ProductionDataDatabase from Jobs Where ipk_JobID = @JobID) 




SET @SQL =
'
DECLARE @File TABLE(NSNumber nVarchar(10),bundleQuantity nVarchar(10), bundleNum nVarchar(20)
,expiry nVarchar(20),Sender nVarchar(20),Blank1 nVarchar(5), Blank2 nVarchar(5),CurrentDate nVarchar(20))


INSERT INTO @File 
SELECT  
  ''"'+@NSNumber+'"'',d.i_quantity,d.vch_Barcode,''"'+@Expiry+'"'',''"CPI"'','''','''',''"'+@Date+'"''
FROM '+@DB+'.dbo.PackingLevel1_Subjob_'+CAST( @CarrierID AS nVarchar)+' d 
WHERE d.iFK_ContainingPackageID IS NOT NULL
order by d.vch_Barcode

SELECT * FROM @File
ORDER BY BundleNum
'
EXEC (@SQL)
END






GO


