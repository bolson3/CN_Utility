USE [UtilitySupport]
GO

/****** Object:  Table [dbo].[MessageLog]    Script Date: 4/8/2019 2:34:10 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[MessageLog](
	[MessageLogID] [int] IDENTITY(1,1) NOT NULL,
	[DateLogged] [datetime] NOT NULL,
	[SeverityLevel] [int] NOT NULL,
	[UserLoggedBy] [nvarchar](100) NOT NULL,
	[Message] [nvarchar](MAX) NOT NULL,
	[ApplicationLoggedBy] [nvarchar](100) NULL,
 CONSTRAINT [PK_MessageLog] PRIMARY KEY CLUSTERED 
(
	[MessageLogID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


