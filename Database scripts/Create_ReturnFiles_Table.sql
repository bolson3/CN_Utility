USE [UtilitySupport]
GO

/****** Object:  Table [dbo].[ReturnFiles]    Script Date: 4/10/2019 12:06:01 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ReturnFiles](
	[ReturnFileID] [int] IDENTITY(1,1) NOT NULL,
	[JobCode] [nvarchar](10) NOT NULL,
	[SubjobId] [nvarchar](8) NOT NULL,
	[FileName] [nvarchar](50) NOT NULL,
	[SaveToLocation] [nvarchar](200) NOT NULL,
	[DateGenerated] [datetime] NOT NULL,
	[DateLastUpdated] [datetime] NOT NULL,
	[LastUpdatedBy] [nvarchar](50) NOT NULL,
	[TimesGenerated] [int] NOT NULL,
	[ReturnFileType] [nVarchar](100) NOT NULL,
 CONSTRAINT [PK_ReturnFiles] PRIMARY KEY CLUSTERED 
(
	[ReturnFileID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


