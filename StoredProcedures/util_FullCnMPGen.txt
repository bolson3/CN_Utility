USE [CDMS]
GO
/****** Object:  StoredProcedure [dbo].[util_FullCnMPGen]    Script Date: 4/30/2018 10:16:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



ALTER PROCEDURE [dbo].[util_FullCnMPGen]
	
			@JobCode			NVarchar(10),
			@SubjobIdentifier	nVarchar(10)
			
AS
BEGIN

DECLARE @JobID int,@TrackingNumber nVarchar(50), @CardID int, @ProductID int, @SQL varchar (MAX), @CarrierID int,@Date varchar (25)
              , @db varchar (100), @ProdID varchar (25), @OrderID varchar (25)
              


SET @Date = CONVERT(Varchar (20),GETDATE(),101)



SELECT @JobID = (SELECT iPK_JobID FROM Jobs Where vch_JobCode = @JobCode) 

SELECT @CarrierID = (SELECT iPK_SUbjobID FROM SUbjobs WHERE iFK_JobID = @JobID 
AND vch_SubJobIdentifier = @SubjobIdentifier AND vch_Name LIKE '%(Carrier)%')
SELECT @CardID = (SELECT iPK_SUbjobID FROM SUbjobs WHERE iFK_JobID = @JobID AND 
vch_SubJobIdentifier = @SubjobIdentifier AND vch_Name NOT LIKE '%(Carrier)%' AND vch_CardTable IS NOT NULL)



SET @ProductID = (SELECT ifk_ProductID FROM SUbjobs WHERE iPK_SubJobID = @CarrierID)


SET @ProdID = (SELECT vch_CustomerPRoductID FROM Products WHERE iPK_ProductID = @ProductID)
SET @OrderID = (SELECT vch_DataFieldValue FROM SubjobData WHERE iFK_SubJobID = @CarrierID AND iFK_DataFieldID = 5)

IF (@OrderID IS NULL)
BEGIN
SET @OrderID = (SELECT vch_DataFieldValue FROM SubjobData WHERE iFK_SubJobID = @CardID AND iFK_DataFieldID = 5)
END


SELECT @DB = (SELECT vch_ProductionDataDatabase from Jobs Where ipk_JobID = @JobID) 


SET @TrackingNumber = @JobCode +' ' + @SubjobIdentifier + 'CN File ' + @Date


SET @SQL =
'
DECLARE @CNFile TABLE(Magic nVarchar(150), pStatus nVarchar(50),Carrier nVarchar(150), currentDate nVarchar(150),TrackingNum nVarchar(150)
				, MerchId nVarchar(150),MerchName nVarchar(150),StoreLocId nVarchar(150),Bundle nVarchar(150),CaseNum nVarchar(50)
				,Pallet nVarchar(50),controlNum nVarchar(50),ShipTo nVarchar(250), Street1 nVarchar(100), Street2 nVarchar(150)
				,City nVarchar(100),ShipState nVarchar(100),Zip nVarchar(50),DCID nVarchar(50),ProdID nVarchar(150),OrderID nVarchar(50),ParentSerNum nVarchar(50))

INSERT INTO @CNFile
VALUES(''Magic_Number'',''Status'',''Carrier'',''Date'',''Tracking_Number'',''Merchant_ID'',''Merchant_Name'',''StoreLocationID''
		,''Batch_Number'',''Case_Number'',''Pallet_Number'',''Serial Number'',''Ship_TO'',''Street_Address1'',
		''Street_Address2'',''CITY'',''State'',''Zip'',''DC_ID'',''Prod_ID'',''Order_ID'',''Parent_Serial_Number'')


INSERT INTO @CNFile 
SELECT  
  a.vch_Magic_Number AS ''Magic_Number'',''p'' AS ''Status'',''NA'' AS ''Carrier'','''+@Date+''' AS ''Date''
       ,''NA'' AS Tracking_Number, ''2188'' AS Merchant_ID
       ,''InComm'' AS ''Merchant_Name'',''113748'' AS ''StorelocationID'',d.vch_Barcode AS ''Batch_Number'' 
       ,e.vch_Barcode AS ''Case_Number'',f.vch_Barcode AS ''Pallet_Number'',a.vch_CardControlNumber AS ''Serial Number''
       , ''NA'' AS ''Ship_TO'', ''NA'' AS ''Street_Address1'', '''' AS ''Street_Address2'', ''NA'' AS ''CITY''
       , ''NA'' AS ''State'', ''NA'' AS ''Zip'','''' AS ''DC_ID'','+@ProdID+' AS ''Prod_ID'','''+@OrderID+''' AS ''Order_ID'',
          c.vch_Sequence_Number AS ''Parent_Serial_Number''
FROM '+@DB+'.dbo.Cards_Subjob_'+CAST(@CardID AS nVarchar)+' a
INNER JOIN '+@DB+'.dbo.lnk_Card_ChildCards_Subjob_'+CAST(@CarrierID AS nVarchar)+' b on a.iPK_CardID = b.iFK_ChildCardID
INNER JOIN '+@DB+'.dbo.Cards_Subjob_'+CAST (@CArrierID AS nVarchar)+' c on b.iFK_ParentCardID = c.iPK_CardID
INNER JOIN '+@DB+'.dbo.PackingLevel1_Subjob_'+CAST( @CarrierID AS nVarchar)+' d on c.iFK_ContainingPackageID = d.iPK_PackageID
INNER JOIN '+@DB+'.dbo.PackingLevel2_Subjob_'+CAST(@CarrierID AS Varchar)+' e on d.iFK_ContainingPackageID = e.iPK_PackageID
INNER JOIN '+@DB+'.dbo.PackingLevel3_Subjob_'+CAST(@CarrierID AS Varchar)+' f on e.iFK_ContainingPackageID = f.iPK_PackageID
WHERE a.i_CardType in (0, 2)
order by magic_number

SELECT * FROM @CNFile
ORDER BY Status DESC
'
PRINT (@SQL)
END




