﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework.Constraints;

namespace CNGenUtil.Tests
{
    public class CNGenUtilViewModelTests
    {
        [TestFixture]

        public class CNGenUtilViewModelTest
        {
            [Test]
            public void JobCode_is_Expected_When_Constructed()
            {
                var ViewModel = new CNGenUtilViewModel();
                var expected = string.Empty;
                var actual = ViewModel.SelectedJobCode;

                Assert.AreEqual(expected, actual);
            }

            [Test]
            public void Jobcode_is_Expected_When_Set()
            {
                var ViewModel = new CNGenUtilViewModel();
                var expected = "3180100";
                ViewModel.SelectedJobCode = "3180100";

                Assert.AreEqual(expected, ViewModel.SelectedJobCode);
            }

            [Test]
            public void Jobcode_Raise_Property_Changed()
            {
                var ViewModel = new CNGenUtilViewModel();
                var raised = false;

                ViewModel.PropertyChanged += (sender, e) =>
                {
                    if (e.PropertyName == "SelectedJobCode")
                        raised = true;
                };
                ViewModel.SelectedJobCode = "Test";
                
                Assert.IsTrue(raised);
            }

            [Test]
            public void Subjob_is_Expected_When_Constructed()
            {
                var ViewModel = new CNGenUtilViewModel();
                var expected = string.Empty;
                var actual = ViewModel.SelectedSubjob;

                Assert.AreEqual(expected, actual);
            }

            [Test]
            public void Subjob_is_Expected_When_Set()
            {
                var ViewModel = new CNGenUtilViewModel();
                var actual = ViewModel.SelectedSubjob = "Test";
                var expected = "Test";

                Assert.AreEqual(expected, actual);
            }

            [Test]
            public void Subjob_Raise_Property_Changed()
            {
                var ViewModel = new CNGenUtilViewModel();
                var raised = false;

                ViewModel.PropertyChanged += (sender, e) =>
                {
                    if (e.PropertyName == "SelectedSubjob")
                        raised = true;
                };
                ViewModel.SelectedSubjob = "Test";

                Assert.IsTrue(raised);
            }

			//[Test]
			//public void ViewModel_CNTypeCount_Is_As_Expected_When_Constructed()
			//{
			//	var viewModel = new CNGenUtilViewModel();
			//	var expected = 7;
			//	var actual = viewModel.AvailReturnFileTypes.Count;

			//	Assert.AreEqual(expected, actual);
			//}

			[Test]
			public void ViewModel_SelectedCN_Null_When_Constructed()
			{
				var viewModel = new CNGenUtilViewModel();
				var expected = string.Empty;
				var actual = viewModel.SelectedReturnFileType;

				Assert.AreEqual(expected, actual);
			}

			[Test]
			public void ViewModel_SelectedCN_Is_Set()
			{
				var viewmodel = new CNGenUtilViewModel();
				var expected = "Standard No UPC";
				viewmodel.SelectedReturnFileType = viewmodel.AvailReturnFileTypes[0];
				var actual = viewmodel.SelectedReturnFileType;

				Assert.AreEqual(expected, actual);
			}

	        [Test]
	        public void ViewModel_SaveToLocation_Is_Expected_When_Constructored()
	        {
				var viewmodel = new CNGenUtilViewModel();
		        var actual = viewmodel.SaveToLocation;
		        var expected = string.Empty;

				Assert.AreEqual(expected,actual);
	        }

	        [Test]
	        public void ViewModel_SaveToLocation_Is_Expected_When_set()
	        {
		        var viewmodel = new CNGenUtilViewModel();
		        viewmodel.SaveToLocation = "test";
		        var actual = viewmodel.SaveToLocation;
		        var expected = "test";

				Assert.AreEqual(expected,actual);
	        }

	        [Test]
	        public void ViewModel_SaveToLocation_Raised_Property_Change()
	        {
		        var viewmodel = new CNGenUtilViewModel();
		        var raised = false;
		        viewmodel.PropertyChanged += (sender, e) =>
		        {
			        if (e.PropertyName == "SaveToLocation")
				        raised = true;
		        };
		        viewmodel.SaveToLocation = "SaveToLocation";
				Assert.IsTrue(raised);
			}

	        [Test]
	        public void ViewModel_Status_Raises_Property_Change()
	        {
				var viewmodel = new CNGenUtilViewModel();
		        var raised = false;

		        viewmodel.PropertyChanged += (sender, e) =>
		        {
			        if (e.PropertyName == "Status")
				        raised = true;

		        };

		        viewmodel.Status = "test";
				
				Assert.IsTrue(raised);
	        }

            [Test]
            public void ViewModel_SelectedFile_Is_Expected_When_Constructed()
            {
                var viewmodel = new CNGenUtilViewModel();
                var expected = string.Empty;
                var actual = viewmodel.SelectedReturnFile;

                Assert.AreEqual(expected, actual);
            }

            [Test]
            public void ViewModel_SelectedFile_Is_Expected_When_Set()
            {
                var viewmodel = new CNGenUtilViewModel();
                var expected = "test";
                viewmodel.SelectedReturnFile = "test";
                var actual = viewmodel.SelectedReturnFile;

                Assert.AreEqual(expected, actual);
            }

            [Test]
            public void ViewModel_SelectedFile_Raises_Property_Change()
            {
                var viewmodel = new CNGenUtilViewModel();
                var raised = false;

                viewmodel.PropertyChanged += (sender, e) =>
                {
                    if (e.PropertyName == "SelectedFile")
                        raised = true;

                };

                viewmodel.SelectedReturnFile = "test";

                Assert.IsTrue(raised);
            }

            [Test]
            public void ViewModel_ClearCommand_ClearsUi()
            {
                var viewModel = new CNGenUtilViewModel();
                viewModel.SelectedReturnFile = "test1";
                viewModel.SelectedReturnFileType = viewModel.AvailReturnFileTypes[1];
                viewModel.SelectedJobCode = "TestJob";
                viewModel.SelectedSubjob = "TestSJob";
                viewModel.SaveToLocation = "testLocal";

                viewModel.ClearCommand.Execute(null);

                Assert.AreEqual(viewModel.SelectedReturnFile,"test1");
                Assert.AreEqual(viewModel.SelectedReturnFileType, string.Empty);
                Assert.AreEqual(viewModel.SelectedJobCode, string.Empty);
                Assert.AreEqual(viewModel.SelectedSubjob,string.Empty);
                Assert.AreEqual(viewModel.SaveToLocation, "testLocal");
            }
        }
    }
}
